﻿Shader "Unlit/Lava"
{
    Properties
    {
        [Header(Main)]
        _MainTex ("Texture", 2D) = "white" {}
        _Scale("MainTex Scale", Range(0,2)) = 0.5
        _SpeedMainX("Speed Main X", Range(-10, 10)) = 0
        _SpeedMainY("Speed Main Y", Range(-10, 10)) = 0
        _VertexDistortion("Vetex Distortion", Range(0, 1)) = 0
        _Offset("Color Offset for Lerp", Range(0, 10)) = 0
        _Strength("Intensity Lava", Range(0, 10)) = 0
        _Color("Main Color Begin", Color) = (0, 0, 0, 0)
        _Color2("Main Color End", Color) = (0, 0, 0, 0)
        _Color3("Glowing Top Color", Color) = (0, 0, 0)
        _Cutoff("Cutoff Top Cracks", Range(0,1)) = 0
        _TopBlur("Top Crack Glow", Range(0,1)) = 0
        
        [Header(Distortion)]
        _DistortTex("Distortion Texture", 2D) = "white" {}
        _DistortionStrength("Distortion Strength", Range(0,1)) = 0.5
        _ScaleDist("Scale Distance", Range(0,1)) = 0.5
        _SpeedDistortX("Speed Distortion X", Range(-10, 10)) = 0
        _SpeedDistortY("Speed Distortion Y", Range(-10, 10)) = 0
        
        [Header(Edge)]
        _Edge("Edge Thickness", Range(0, 20)) = 0
        _EdgeBlur("Edge Glow Intensity", Range(0,1)) = 0.5
        _EdgeC("Edge Color", Color) = (0, 0, 0, 0)
        _StrengthTop("Edge Brightness", Range(0, 10)) = 0
        
        [Header(Vertex Movement)]
		_Speed("Wave Speed", Range(0,1)) = 0
		_Amount("Wave Amount", Range(0,1)) = 0
		_Height("Wave Height", Range(0,1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color: COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float4 worldPos : TEXCOORD4; // Declare worldpos is being passed to fragment shader directly in the v2f struct
                float4 color: COLOR;
                float4 scrPos: TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Speed, _Amount, _Height;
            sampler2D _CameraDepthTexture;

            v2f vert (appdata v)
            {
                v2f o;
                
                // Add wavemovent to the vertecis height - needs to happen before further vertex acces in the vertex shader
                // Change to vertex z for up and down movement - currently disabled anyways
		        v.vertex.y += (sin(_Time.z * _Speed + (v.vertex.x * v.vertex.z * _Amount)) * _Height) * v.color.r;
		        
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
		        o.worldPos = mul(unity_ObjectToWorld, v.vertex); // Retrieve the world pos data
		        o.color = v.color;
		        o.scrPos = ComputeScreenPos(o.vertex); // screen position for depth
                return o;
            }
            
            sampler2D _DistortTex;
            float _Scale, _SpeedMainX, _SpeedMainY, _VertexDistortion, _Offset, _Strength;
            float _ScaleDist, _SpeedDistortX, _SpeedDistortY, _DistortionStrength;
            float4 _Color, _Color2, _EdgeC, _Color3;
            float _Edge, _EdgeBlur, _StrengthTop;
            float _Cutoff, _TopBlur;

            fixed4 frag (v2f i) : SV_Target
            {
                // UV Distortion
                float2 uvDistort = i.worldPos.xz * _ScaleDist;
                // Move over time
                float speedDistortX = _Time.x * _SpeedDistortX;
                float speedDistortY = _Time.x * _SpeedDistortY;
                float2 speedDistortCombined = float2(speedDistortX, speedDistortY);
                // Combine the calculated uv-Distortion with the Texture that is to distort
                float distortion1 = tex2D(_DistortTex, uvDistort + speedDistortCombined).r;
                
                // Create a secondary distortion with a different scale, adds a bit more depth
                float distortion2 = tex2D(_DistortTex, (i.worldPos.xz * (_ScaleDist * 0.5)) + speedDistortCombined).r;
                
                // Combine the two distortions
                float combinedDistortion = saturate((distortion1 + distortion2)*0.5); // Saturate clamps the values so they are not less than 0 or more than 1
                
                // Scale and Calculate UVs of MainTex
                float2 uvMain = i.worldPos.xz * _Scale;
                // Add calculated distortion multiplied by distortion strength variable to control distortion
                uvMain += combinedDistortion * _DistortionStrength;
                // Calcuate Movement over time
                float speedMainX = _Time.x * _SpeedMainX;
                float speedMainY = _Time.x * _SpeedMainY;
                float2 speedMainCombined = float2(speedMainX, speedMainY);
                // Combined Calculated movemnt over time with calculated uvs and distort extra based on vertex color
                uvMain += speedMainCombined + (i.color.r * _VertexDistortion);
                // Calculate color  
                half4 distortedColor = tex2D(_MainTex, uvMain) * i.color.r;
                // Add the calculated distortion to it
                distortedColor += combinedDistortion;
                
                // Colorize the Texture
                float4 color = lerp(_Color, _Color2, distortedColor * _Offset) * _Strength; // Lerp color at each fragment between to set colors depending on the calculated distortion at that point
                
                // Fade color with Red Vertex Color, so we can Vertex paint the mesh with black to fade the color at these points
                color *= i.color.r;
                
                // Detect edged of lava via depth
                // EDGE IS CURRENTLY NOT WORKING :(
                half depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos))); 
		        half4 edgeLine = 1 - saturate(_Edge* (depth - i.scrPos.w));
                //Cutoff edge
                float edge = smoothstep(1 - distortedColor, 1 - _EdgeBlur, edgeLine);
                // Take the edge out of the main color
                color *= (1 - edge);
                // Add the edge back in with the selected edgecolor and multiply it by brightnessvalue
                color += (edge * _EdgeC) * _StrengthTop;
                
                // Glowing Top parts
                float top = smoothstep(_Cutoff, _Cutoff + _TopBlur, distortedColor) * i.color.r; // Smoothstep over current main texture result to to get the brightest parts
                color *= (1-top); // Take top out of main color
                color += top * _Color3 * _StrengthTop; // add top back in with the selectedc color and brightness multiplier
                
                return color;
            }
            ENDCG
        }
    }
}
