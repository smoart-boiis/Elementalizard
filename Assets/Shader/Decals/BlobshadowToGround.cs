﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobshadowToGround : MonoBehaviour
{
    [SerializeField] Transform raycastStart;
    int layermask;

    // Start is called before the first frame update
    void Start()
    {
        // Bit mask for ground
        int layerMask = 1 << 8;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Raycast for ground under character at every frame, set blobshadow there
        RaycastHit hit;
        if (Physics.Raycast(raycastStart.position, Vector3.down, out hit, Mathf.Infinity, layermask))
        {
            transform.position = hit.point;
        }
    }
}
