﻿Shader "Toon/Water"
{
    // Tutorial thanks to: https://roystan.net/articles/toon-water.html
    // Skipped 3. Consisten Foam size - might add later

    Properties
    {
        _DepthGradientShallow("Depth Gradient Shallow", Color) = (0.325, 0.807, .971, 0.725)
        _DepthGradientDeep("Depth Gradient Deep", Color) = (0.086, 0.407, 1, 0.749)
        _DepthMaxDistance("Depth Maximum Distance", Float) = 1

        _SurfaceNoise("Surface Noise", 2D) = "white" {} // Open Texture Slot for Noise
        _SurfaceNoiseCutoff("Surface Noise Cutoff", Range(0, 1)) = 0.777 // Cutoff for the noise texture for toon-style
        _SurfaceNoiseScroll("Surface Noise Scroll Amoung", Vector) = (0.3, 0.3, 0, 0)
        _SurfaceDistortion("Surface Distortion", 2D) = "white" {}
        _SurfaceDistortionAmount("Surface Distortion Amount", Range(0,1)) = 0.2

        _FoamDistance("Foam Distance", Float) = 0.4
        _FoamColor("Foam Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent" // Tag this Shader as transparent so it supports transparency - gets renderered after opaque objets     ## Might Remove for opaqueness
        }
        
        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha // Tells us how to blend the transparency with the lower opaque objects                         ## Might Remove for opaqueness
            ZWrite Off // Prevent object from being written in depth buffer. If it was in depth buffer it would occlude other objects       ## Might Remove for opaqueness

			CGPROGRAM
            #define SMOOTHSTEP_AA 0.01 // Makes smoothstep function available
                                       // Smooth step is similar to lerp but non-linear, so ideal for smootly blending values
                                       // Also used for clamping values, we use it to antialias our foam
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata // Provides Vertex Data
            {
                float4 vertex : POSITION; // Vertexposition
                float4 uv : TEXCOORD0;  // UV Coordinates
            };

            struct v2f // structure being created in vertex shader to pass data to fragment shader
            {
                float4 vertex : SV_POSITION;
                float4 screenPosition : TEXCOORD2; 
                float2 noiseUV : TEXCOORD0; //  noise UV variable as the provided UV Coordinates
                float2 distortUV : TEXCOORD1; //  Distortion Map UVs
            };

            sampler2D _SurfaceNoise;
            float4 _SurfaceNoise_ST; // Unity automatically populate with tiling and offset from _SurfaceNoise due to the _ST ending

            sampler2D _SurfaceDistortion;
            float4 _SurfaceDistortion_ST; // Fill cutoff and tiling automatically for UCS

            v2f vert (appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.screenPosition = ComputeScreenPos(o.vertex); // Calculate Pixelscreenposition
                o.noiseUV = TRANSFORM_TEX(v.uv, _SurfaceNoise); // Modifies UVs with given Tiling and offset from Texture
                o.distortUV = TRANSFORM_TEX(v.uv, _SurfaceDistortion); // as above

                return o;
            }

            float4 _DepthGradientShallow;
            float4 _DepthGradientDeep;
            float _DepthMaxDistance;

            sampler2D _CameraDepthTexture; // Gives Acces to Cameras Depth Texture! White = close to camera; black = far from camera
                                           // Camera Depth texture mode must be activated via camera script!!!

            float _SurfaceNoiseCutoff; // Initialize property as variable
            float _FoamDistance; // Property as Variable
            float2 _SurfaceNoiseScroll; // Property as Variable Vector seems to be 4s by default but get cast as sth else as variabel
            float _SurfaceDistortionAmount;
            float4 _FoamColor;

            float4 frag(v2f i) : SV_Target
            {
                float existingDepth01 = tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPosition)).r; // Returns non-linear depth information for pixelposition
                float existingDepthLinear = LinearEyeDepth(existingDepth01); // Makes depth information linear!

                float depthDifference = existingDepthLinear - i.screenPosition.w; // Calculate depth difference relative to water surface

                float waterDepthDifference01 = saturate(depthDifference / _DepthMaxDistance); // Normalize depth value into 0-1 values for lerping
                float4 waterColor = lerp(_DepthGradientShallow, _DepthGradientDeep, waterDepthDifference01); // Lerp between the shallow and deep water color in correlance to depth

                // Distortion map is normal map with only R and G color channels so we acces x and y for r and g values for our distorition
                float2 distortSample = (tex2D(_SurfaceDistortion, i.distortUV).xy * 2 - 1) * _SurfaceDistortionAmount; // multiplying UV with (x*2) - 1 takes range from 0to1 to -1to1

                // Offset Noise Texure UV by Value times Time to create scrolling noise, also add extra distortion from distort map
                float2 noiseUV = float2((i.noiseUV.x + _Time.y * _SurfaceNoiseScroll.x) + distortSample.x, (i.noiseUV.y + _Time.y * _SurfaceNoiseScroll.y) + distortSample.y); 
                float surfaceNoiseSample = tex2D(_SurfaceNoise, noiseUV).r; // Samples the noise texture

                float foamDepthDifference01 = saturate(depthDifference / _FoamDistance); // Normalize Value between depth and foamdistance
                float surfaceNoiseCutoff = foamDepthDifference01 * _SurfaceNoiseCutoff; // Adjust the Noise Cutoff according to depth to another object for Foamshoreline

                float surfaceNoise = smoothstep(surfaceNoiseCutoff - SMOOTHSTEP_AA, surfaceNoiseCutoff + SMOOTHSTEP_AA, surfaceNoiseSample); // Clamp Noise at noise-cutoff via smoothstep for antialiased foam
                float4 surfaceNoiseColor = _FoamColor * surfaceNoise; // Multiply Noise - aka foam - with selected color for colored foam
                                                                      // This provides additive blending which does not stay color-true but instead combines colors, the results look abstract but fitting for our game
                                                                      // Tutorial does provide a solution for alpha blending instea, which would result in true color

				return waterColor + surfaceNoiseColor; // Combine the watercolor with the coloredNoise
            }
            ENDCG
        }
    }
}