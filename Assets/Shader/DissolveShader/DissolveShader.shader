﻿Shader "Unlit/DissolveShader"
{
    // Inspired by https://lindenreid.wordpress.com/2017/12/16/dissolve-shader-in-unity/
    // If you want to animate it, use time instead of cutoff values
    // But this is used as a stati cut off that rotates around a fireball :)
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _EdgeColor2("Outer Edge Color", Color) = (1, 1, 1, 1)  // Color for the outer edge
        _EdgeColor1("Inner Edge Color", Color) = (1, 1, 1, 1)  // Color for the edge
        _MainColor("Body Color", Color) = (1, 0.1, 0.1, 1)  // Color for the edge
        
        _NoiseCutoff("Outer Edge Cutoff", Range(0, 1)) = 0.777 // Must be smallest
        _EdgeCutoff2("Inner Cutoff2", Range(0, 1)) = 0.9 // Cutoffs determine value which part of noise texture to cut off
        _EdgeCutoff1("Body Cutoff1", Range(0, 1)) = 0.9 // Must be highest
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            float _NoiseCutoff;
            float4 _MainColor;
            float _EdgeCutoff1;
            float4 _EdgeColor1;            
            float _EdgeCutoff2;
            float4 _EdgeColor2;

            fixed4 frag(v2f i) : SV_Target
            {
                float4 color = _MainColor;

                float noiseSample = tex2D(_MainTex, i.uv); // Samples Texture with UV-Coords from vertex-shader

                float edgeDissolve = noiseSample - _EdgeCutoff1 < 0; // Imitate clip function and check if Noise - Edgecut off is smaller than  0
                    color = (1 - edgeDissolve) * color + edgeDissolve * _EdgeColor1; // if yes, instead of deleting, color it with Edge Color


                float edgeDissolve2 = noiseSample - _EdgeCutoff2 < 0; // Repeart for a second edge color
                    color = (1 - edgeDissolve2) * color + edgeDissolve2 * _EdgeColor2;
               
                clip(noiseSample - _NoiseCutoff); // Deletes current pixel if value is less than 0

                return color;
            }
            ENDCG
        }
    }
}
