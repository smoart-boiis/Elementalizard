﻿using System.Collections;
using UnityEngine;

public class StopLooping : MonoBehaviour
{
    [SerializeField] private float loopDelay;
    
    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(Delay());
    }

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(loopDelay);
        var particleSys = transform.GetComponent<ParticleSystem>().main;
        particleSys.loop = false;
    }
}
