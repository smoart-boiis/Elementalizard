﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcicleBehaviour : MonoBehaviour
{
    [SerializeField] private List<GameObject> vfxToDisable = new List<GameObject>();
    
    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(DelayedDestroy());
        foreach (GameObject go in vfxToDisable)
        {
            go.SetActive(false);
        }
    }

    private IEnumerator DelayedDestroy()
    {
        yield return new WaitForSeconds(5f);
        Destroy(transform.root.gameObject);
    }
}
