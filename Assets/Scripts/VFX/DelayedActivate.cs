﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedActivate : MonoBehaviour
{
    [SerializeField] private float delay = 2f;

    [SerializeField] private GameObject particleToDelay;
    
    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(DelayedParticle());
    }

    private IEnumerator DelayedParticle()
    {
        yield return new WaitForSeconds(delay);
        particleToDelay.SetActive(true);
    }
}
