﻿using UnityEngine;

public class DestroyOnWall : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("IceWall"))
            Destroy(gameObject);
    }
}
