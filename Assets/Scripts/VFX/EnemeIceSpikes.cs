﻿using UnityEngine;

public class EnemeIceSpikes : MonoBehaviour
{
    [SerializeField] private Vector3 targetPos;
    [SerializeField] private float tweenIn = 0.1f;
    [SerializeField] private float tweenOut = 0.5f;
    private Vector3 startPos;
    
    // Start is called before the first frame update
    private void Start()
    {
        startPos = transform.localPosition;
        LeanTween.moveLocal(gameObject, targetPos, tweenIn).setEaseSpring().setOnComplete(TweenBack);
    }

    private void TweenBack()
    {
        LeanTween.moveLocal(gameObject, startPos, tweenOut);
    }
}
