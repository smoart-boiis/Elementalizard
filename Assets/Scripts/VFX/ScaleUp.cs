﻿using UnityEngine;

public class ScaleUp : MonoBehaviour
{
    [SerializeField] private float tweenTime;
    [SerializeField] private Vector3 targetScale = Vector3.one;
    [SerializeField] private Vector3 startScale = Vector3.zero;
    public bool isPreview;
    
    // Start is called before the first frame update
    private void Start()
    {
        if (isPreview)
            return;
        
        transform.localScale = startScale;
        LeanTween.scale(gameObject, targetScale, tweenTime);
    }

    public void Scale()
    {
        transform.localScale = startScale;
        LeanTween.scale(gameObject, targetScale, tweenTime);
    }
}
