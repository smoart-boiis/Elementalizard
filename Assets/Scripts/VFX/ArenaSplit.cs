﻿using System.Collections;
using UnityEngine;

public class ArenaSplit : MonoBehaviour
{
    private GameObject leftVfx;
    private GameObject rightVfx;

    private void Start()
    {
        leftVfx = GameObject.Find("ProxyArmParentL").transform.GetChild(0).gameObject;
        rightVfx = GameObject.Find("ProxyArmParentR").transform.GetChild(0).gameObject;
        StartCoroutine(ActivateArmVfx());
    }

    private IEnumerator ActivateArmVfx()
    {
        leftVfx.SetActive(true);
        yield return new WaitForSeconds(1f);
        rightVfx.SetActive(true);
        yield return new WaitForSeconds(1f);
        leftVfx.SetActive(false);
        rightVfx.SetActive(false);
    }
}
