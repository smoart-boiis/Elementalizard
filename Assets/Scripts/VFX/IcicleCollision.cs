﻿using System;
using UnityEngine;

public class IcicleCollision : MonoBehaviour
{
    private int tweenId = -1;
    [SerializeField] private float tweentime;
    [SerializeField] private GameObject onHit;
    
    public void MoveIcicle(Vector3 targetPos)
    {
        tweenId = LeanTween.move(transform.parent.gameObject, targetPos, tweentime).setOnComplete(ResetId).id;
    }

    private void ResetId()
    {
        tweenId = -1;
        onHit.SetActive(true);
    }
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("MainCharacter"))
            return;
        
        if(tweenId != -1)
            LeanTween.cancel(tweenId);
        onHit.SetActive(true);
    }
}
