﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    [SerializeField] private float destructionTime;
    
    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(DestroyTime());
    }

    private IEnumerator DestroyTime()
    {
        yield return new WaitForSeconds(destructionTime);
        Destroy(gameObject);
    }
}
