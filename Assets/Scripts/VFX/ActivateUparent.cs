﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateUparent : MonoBehaviour
{
    [SerializeField] private float delay = 0.3f;
    [SerializeField] private GameObject childVfx;
    
    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(Unparent());
    }

    private IEnumerator Unparent()
    {
        yield return new WaitForSeconds(0.3f);
        childVfx.transform.parent = null;
        childVfx.SetActive(true);
    }
}
