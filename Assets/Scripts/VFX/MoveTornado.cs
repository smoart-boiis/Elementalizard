﻿using UnityEngine;

public class MoveTornado : MonoBehaviour
{
    [SerializeField] private GameObject toMove;
    [SerializeField] private float tweenTime = 0.3f;

    public void Move(Vector3 position)
    {
        LeanTween.move(toMove, position, tweenTime);
    }
}
