﻿using System;
using System.Net.Http;
using UnityEngine;

public class TorusGrowth : MonoBehaviour
{
    [SerializeField] private float growthSpeed;
    private Mesh mesh;
    private Vector3[] modelOnStart;
    
    private void Awake()
    {
        mesh = GetComponent<ParticleSystem>().GetComponent<ParticleSystemRenderer>().mesh;
        modelOnStart = mesh.vertices;
    }

    private void Update()
    {
        var vertices = mesh.vertices;
 
        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 normalised = vertices[i].normalized;
            normalised.y = 0;
            vertices[i] += normalised * Time.deltaTime * growthSpeed;
        }
 
        mesh.vertices = vertices;
    }

    private void OnDestroy()
    {
        mesh.vertices = modelOnStart;
    }
    
    private void OnDisable()
    {
        mesh.vertices = modelOnStart;
    }
}
