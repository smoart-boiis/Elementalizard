﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadScene : MonoBehaviour
{
    public void ReloadAndLoad()
    {
        MarkLoadOnReload.loadSave = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
