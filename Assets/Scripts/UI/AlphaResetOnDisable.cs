﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaResetOnDisable : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    
    private void OnDisable()
    {
        GetComponent<CanvasGroup>().alpha = 0;
    }
}
