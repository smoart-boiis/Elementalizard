﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YouDiedAnim : MonoBehaviour
{
    [SerializeField] private CanvasGroup deathText;

    private void OnEnable()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        StartCoroutine(DeathUi());
    }

    private IEnumerator DeathUi()
    {
        deathText.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
        LeanTween.alphaCanvas(deathText, 1, 1.5f);
        LeanTween.scale(deathText.gameObject, Vector3.one, 1.5f);
        yield return new WaitForSeconds(1.75f);
        for(int i = 1; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        deathText.alpha = 0;
        deathText.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
        for(int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(false);
    }
}
