﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button loadButton;
    [SerializeField] private GameObject safetyPopUp;
    public void StartGame(bool load)
    {
        MarkLoadOnReload.loadSave = load;
        
        if(load)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        else if(!loadButton.interactable)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        else
            safetyPopUp.SetActive(true);
    }

    public void Safety(bool isCertain)
    {
        safetyPopUp.SetActive(false);
        
        if(isCertain)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
    public void Quit()
    {
        Application.Quit();
    }
}
