﻿using System.Collections;
using UnityEngine;

public class SaveSuccessMessage : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void OnEnable()
    {
        StartCoroutine(FadeOutTurnOff());
    }

    private IEnumerator FadeOutTurnOff()
    {
        canvasGroup.alpha = 1;
        yield return null;

        LeanTween.alphaCanvas(canvasGroup, 0, 0.66f).setUseEstimatedTime(true);
        
        yield return new WaitForSecondsRealtime(0.66f);
        
        gameObject.SetActive(false);
    }
}
