﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles when saving is allowed - is forbidden, when:
/// in combat, in death animation, in flight, when skill has CD
/// </summary>
public class SaveConditions : MonoBehaviour
{
    public static SaveConditions instance;
    
    /// <summary>
    /// Add to stack if a condition is active that forbids saving
    /// </summary>
    private readonly Stack<bool> savingAllowed = new Stack<bool>();
    [HideInInspector] public bool saveAllowed = true;

    private void Awake()
    {
        instance = this;
    }

    public void AddToStack()
    {
        savingAllowed.Push(true);
        saveAllowed = false;
    }

    public void DelayedPop(float delay)
    {
        StartCoroutine(DelayPop(delay));
    }

    private IEnumerator DelayPop(float delay)
    {
        yield return new WaitForSeconds(delay);
        Remove();
    }

    public void Remove()
    {
        if(savingAllowed.Count > 0)
            savingAllowed.Pop();
        
        if (savingAllowed.Count < 1)
            saveAllowed = true;
    }

}
