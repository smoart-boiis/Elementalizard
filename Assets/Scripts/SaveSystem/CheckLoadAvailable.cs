﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


public class CheckLoadAvailable : MonoBehaviour
{
    private Button loadbutton;

    private void Awake()
    {
        loadbutton = GetComponent<Button>();
        CheckLoadFile();
    }

    private void OnEnable()
    {
        CheckLoadFile();
    }

    private void CheckLoadFile()
    {
        string path = Application.persistentDataPath + "/savedata.save";

        loadbutton.interactable = File.Exists(path);
    }
}
