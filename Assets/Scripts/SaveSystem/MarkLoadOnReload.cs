﻿using UnityEngine;

public class MarkLoadOnReload : MonoBehaviour
{
    public static bool loadSave = false;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
