﻿using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using SaveSystem;

/// <summary>
/// Static class that handles writing and loading the save data to and from file for us
/// </summary>
public static class SaveManager
{
    public static void SaveGame(ResourceManager resourceManager)
    {
        BinaryFormatter formatter = GetBinaryFormatter();
        SurrogateSelector selector = new SurrogateSelector();
        Vector3SerializationSurrogate vector3SerializationSurrogate = new Vector3SerializationSurrogate();
        selector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SerializationSurrogate);
        formatter.SurrogateSelector = selector;

        string path = Application.persistentDataPath + "/savedata.save"; // Set save data pathfile to a persistent one
        FileStream stream = new FileStream(path, FileMode.Create); // Create a new filestream to write/create a file
        
        // Save the current Platforms stati
        var allPlatforms = GameObject.FindGameObjectsWithTag("MovingPlatform");
        var savedPlatforms = allPlatforms.Select(platform => new SavePlatforms(platform)).ToList();
        
        // Save curren frozen surfaces
        var allSurfaces = GameObject.FindGameObjectsWithTag("FrozenSurface");
        var savedSurfaces = allSurfaces.Select(surface => new SaveFrozenSurface(surface)).ToList();

        GameObject fireBoss = GameObject.FindGameObjectWithTag("Fireboss");
        GameObject iceBoss = GameObject.FindGameObjectWithTag("Iceboss");
        SaveData save = new SaveData(resourceManager, savedPlatforms, savedSurfaces, iceBoss == null, fireBoss == null); // Create a Savedata class with the help of the written savedata constructor
        
        formatter.Serialize(stream, save); // Serialize the data is a binary file and save it to the path
        stream.Close(); // Close Filestream when done writing
    }

    public static SaveData LoadGame()
    {
        string path = Application.persistentDataPath + "/savedata.save";
        if (File.Exists(path)) // if save data is found reconvert to our data
        {
            BinaryFormatter formatter = GetBinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open); // Open up the existing Save data

            SaveData data = formatter.Deserialize(stream) as SaveData; // Deserialize it and cast File Data as SaveData type
            stream.Close(); // Close stream after done reading
            
            return data;
        }
        else
        {
            Debug.Log("No Sava Data found");
            return null;
        }
    }

    /// <summary>
    /// Creates a binary Formatter and tells it to use our Vector3 Surrogate to properly serialize Vector3s
    /// </summary>
    /// <returns></returns>
    private static BinaryFormatter GetBinaryFormatter()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        SurrogateSelector selector = new SurrogateSelector();
        Vector3SerializationSurrogate vector3SerializationSurrogate = new Vector3SerializationSurrogate();
        selector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SerializationSurrogate);
        formatter.SurrogateSelector = selector;
        return formatter;
    }
}
