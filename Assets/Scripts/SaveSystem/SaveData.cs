﻿using System.Collections.Generic;
using Enemies;
using UnityEngine;

namespace SaveSystem
{
    /// <summary>
    /// Enum that indicates the savesystem of what type the enemy is
    /// </summary>
    [System.Serializable]
    public enum EnemyType
    {
        IceGolem,
        FireDjinn,
        IceBoss,
        FireBoss
    };

    /// <summary>
    /// Save frozen surfaces
    /// </summary>
    [System.Serializable]
    public class SaveFrozenSurface
    {
        public Vector3 position;
        public Vector3 eulerAngles;
        public SlowMelt.SurfaceType surfaceType;
        public float timeActive;
        public bool isIce;

        public SaveFrozenSurface(GameObject surfaceObject)
        {
            position = surfaceObject.transform.position;
            eulerAngles = surfaceObject.transform.eulerAngles;
            surfaceType = surfaceObject.GetComponent<SlowMelt>().surfaceType;
            timeActive = surfaceObject.GetComponent<SlowMelt>().timePassed;
            isIce = surfaceObject.GetComponent<SlowMelt>().isIce;
        }
    }

    /// <summary>
    /// Saving Platform Position and state
    /// Only needs position not rotation as they only move up and down
    /// Also we only have the same amount of platforms in the game as they do not get destroyed or instantiated
    /// </summary>
    [System.Serializable]
    public class SavePlatforms
    {
        public string platformName;
        public bool moveToStart;
        public float currentLerpTime;
        public Vector3 position;

        public SavePlatforms(GameObject platform)
        {
            platformName = platform.name;
            moveToStart = platform.GetComponent<PlatformMover>().moveToStart;
            currentLerpTime = platform.GetComponent<PlatformMover>().currentTime;
            position = platform.transform.position;
        }
    }
    
    /// <summary>
    /// Class that saves enemy entry data for the save system
    /// </summary>
    [System.Serializable]
    public class EnemyEntry
    {
        public EnemyType enemyType;
        public string enemyId;
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 origin;
        public bool alive;

        public EnemyEntry(GameObject enemy)
        {
            enemyType = enemy.GetComponent<EnemyStats>().enemyType;
            enemyId = enemy.GetComponent<EnemyStats>().enemyId;
            position = enemy.transform.position;
            origin = enemy.GetComponent<EnemyDetection>().origin;
            eulerAngles = enemy.transform.eulerAngles;
            alive = true;
        }

        public void UpdateOnSave(GameObject enemy)
        {
            position = enemy.transform.position;
            eulerAngles = enemy.transform.eulerAngles;
        }
    }
    
    /// <summary>
    /// Class that decides what data to save and where to get it from
    /// </summary>
    [System.Serializable] // This Tag means we can save this class as a file
    public class SaveData
    {
        public int currentTemp;
        public Vector3 position;
        public Vector3 eulerAngles;
        public bool[] skillsUnlocked;
        public List<EnemyEntry> enemyStati;
        public List<SavePlatforms> platforms;
        public List<SaveFrozenSurface> frozenSurfaces;
        public bool iceBossDead;
        public bool fireBossDead;

        public SaveData(ResourceManager resourceManager, List<SavePlatforms> savedPlatforms,
            List<SaveFrozenSurface> saveFrozenSurfaces, bool iceBossDeadNew, bool fireBossDeadNew) // Constructor for SaveData
        {
            currentTemp = resourceManager.GetResource();
            position = resourceManager.transform.position;
            eulerAngles = resourceManager.transform.eulerAngles;
            skillsUnlocked = resourceManager.GetComponent<SaveDataAccessor>().skillsUnlocked;
            enemyStati = resourceManager.GetComponent<SaveDataAccessor>().allEnemies;
            platforms = savedPlatforms;
            frozenSurfaces = saveFrozenSurfaces;
            iceBossDead = iceBossDeadNew;
            fireBossDead = fireBossDeadNew;
        }
    }
}
