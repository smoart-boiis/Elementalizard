﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Enemies;
using SaveSystem;
using UnityEngine;
using UnityEngine.SceneManagement;
using CharacterController = Character.CharacterController;

public class SaveAndLoadFunctions : MonoBehaviour
{
    [SerializeField] private List<GameObject> enemyPrefabs = new List<GameObject>();
    [SerializeField] private List<GameObject> frozenSurfaces = new List<GameObject>();
    [SerializeField] private CanvasGroup fadeScreen;
    [SerializeField] private GameObject saveSucces;
    [SerializeField] private GameObject saveNotPossible;
    [SerializeField] private GameObject intro;

    private void Awake()
    {
        GameObject newSceneMark = null;
        newSceneMark = GameObject.Find("NewGame");
        if (MarkLoadOnReload.loadSave)
            StartCoroutine(LoadWithFade());
        else if (newSceneMark != null)
        {
            StartCoroutine(NewGame());
        }
        
        if(newSceneMark != null)
            Destroy(newSceneMark);

    }

    private IEnumerator NewGame()
    {
        yield return new WaitForEndOfFrame();
        yield return null;

        // Update enemy positions and rotations before saving
        SaveDataAccessor.instance.UpdateEnemies();
        
        // Save
        SaveManager.SaveGame(ResourceManager.instance);
        
        intro.SetActive(true);
    }

    /// <summary>
    /// Save Game Function that can be called by the player
    /// </summary>
    public void SaveGame()
    {
        if (!SaveConditions.instance.saveAllowed)
        {
            saveNotPossible.SetActive(true);
            return;
        }
        
        // Update enemy positions and rotations before saving
        SaveDataAccessor.instance.UpdateEnemies();
        
        // Save
        SaveManager.SaveGame(ResourceManager.instance);
        
        // Unpause the game (as we can only acces save/load menu via Pausemenu
        PauseMenu.instance.PauseUnpause();
        saveSucces.SetActive(true);
    }

    /// <summary>
    /// Function that is called when player loads game from pause menu
    /// </summary>
    public void LoadGameFromPause()
    {
        MarkLoadOnReload.loadSave = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private IEnumerator LoadWithFade()
    {
        fadeScreen.gameObject.SetActive(true);
        fadeScreen.GetComponent<CanvasGroup>().alpha = 1;
            
        yield return new WaitForSecondsRealtime(0.25f);
        Time.timeScale = 1;
        AbilityManager.noInput = false;
        SaveData saveData = SaveManager.LoadGame();
        if (saveData != null)
        {
            // Body Temp
            ResourceManager.instance.SetResource(saveData.currentTemp);
            yield return new WaitForSecondsRealtime(0.11f); // wait for Temp-Zeiger to lerp to right position
            // Set position
            ResourceManager.instance.transform.position = saveData.position;
            // Set rotation
            ResourceManager.instance.transform.eulerAngles = saveData.eulerAngles;
        
            AdjustSkillUnlocks(saveData);
            AdjustEnemies(saveData);
            AdjustPlatforms(saveData);
            AdjustSurfaces(saveData);
            AdjustBosses(saveData);
        }
        yield return null;
        ResourceManager.instance.isDead = false;
        CharacterController.lockMovement = false;
        AbilityManager.noInput = false;
        LeanTween.alphaCanvas(fadeScreen, 0, 0.25f).setIgnoreTimeScale(true);
        yield return new WaitForSecondsRealtime(0.25f);
        fadeScreen.gameObject.SetActive(false);
        yield return new WaitForSecondsRealtime(0.25f);
    }

    /// <summary>
    /// Handles resetting skill unlocks when loading the game
    /// </summary>
    /// <param name="saveData"></param>
    private static void AdjustSkillUnlocks(SaveData saveData)
    {
        // Skill Unlocks
        for (int i = 0; i < saveData.skillsUnlocked.Length; i++)
        {
            if (saveData.skillsUnlocked[i] == SaveDataAccessor.instance.skillsUnlocked[i])
                continue; // Skip rest of this iteration because everything is in order for this one
            if (saveData.skillsUnlocked[i]) // Skills need to be unlocked
                SaveDataAccessor.instance.possibleUnlocks[i].GetComponent<UnlockSkills>().UnlockSkillOnLoad();
            else // Skills need to be removed
            {
                // Tag in the save data accessor
                SaveDataAccessor.instance.skillsUnlocked[i] = false;
                
                // Adjust UI
                FindObjectOfType<AbilityRotator>().DecreaseAbility();
                
                // Remove from AbilityManagerList (Just remove the last list entry as it is always the most recent addition)
                AbilityManager abilityManager = GameObject.FindGameObjectWithTag("MainCharacter").GetComponent<AbilityManager>();
                abilityManager.fireAbilites.Remove(abilityManager.fireAbilites[abilityManager.fireAbilites.Count - 1]);
                abilityManager.iceAbilites.Remove(abilityManager.iceAbilites[abilityManager.iceAbilites.Count - 1]);
                
                // Respawn collectible item & make it interactable again
                SaveDataAccessor.instance.possibleUnlocks[i].SetActive(true);
                SaveDataAccessor.instance.possibleUnlocks[i].GetComponent<UnlockSkills>().onlyOnce = false;
            }
        }
    }

    /// <summary>
    /// Handles setting up all enemies at the right pos/rot when loading (or destroy/instantiate new ones when killed/not killed).
    /// </summary>
    /// <param name="saveData"></param>
    private void AdjustEnemies(SaveData saveData)
    {
        foreach (EnemyEntry oldEnemy in saveData.enemyStati)
        {
            bool exists = false;
            
            foreach (EnemyStats currentEnemy in FindObjectsOfType<EnemyStats>())
            {
                if (oldEnemy.enemyId != currentEnemy.enemyId) 
                    continue;
                
                exists = true;
                if(!oldEnemy.alive)    // Saved enemy is marked as dead so destroy it
                    Destroy(currentEnemy.gameObject);
                else      // Saved enemy still exists and is alive so set it to the previous position and rotation
                {
                    currentEnemy.transform.position = oldEnemy.position;
                    currentEnemy.transform.eulerAngles = oldEnemy.eulerAngles;
                }
            }
            
            if(!exists && oldEnemy.alive) // Saved enemy was marked alive but does not exist anymore so spawn it again
                SpawnEnemy(oldEnemy);
        }
        
        // Go the other way round and find all enemies that exists but are not on the save-List -> Therefore destory them
        foreach (EnemyStats newEnemy in FindObjectsOfType<EnemyStats>())
        {
            bool exists = false;
            
            foreach (EnemyEntry oldEnemy in saveData.enemyStati.Where(oldEnemy => newEnemy.enemyId == oldEnemy.enemyId))
            {
                exists = true;
            }
            
            if(!exists)
                Destroy(newEnemy.gameObject);
        }
        
        // Lastly replace the SaveDatasAccesors enemylist with the one from the savedata file, so it is up to date
        SaveDataAccessor.instance.allEnemies = saveData.enemyStati;
    }

    // Handles respawning a saved enemy
    private void SpawnEnemy(EnemyEntry enemyEntry)
    {
        GameObject newEnemy = null;
        // Spawn Enemy depending on what type it is
        if ((int) enemyEntry.enemyType < enemyPrefabs.Count)
            newEnemy = Instantiate(enemyPrefabs[(int) enemyEntry.enemyType], enemyEntry.position, Quaternion.Euler(enemyEntry.eulerAngles));

        if (newEnemy == null)
            return;

        // Set the enemies origin
        newEnemy.GetComponent<EnemyDetection>().origin = enemyEntry.origin;

        // Give the spawned enemy the saved ID
        newEnemy.GetComponent<EnemyStats>().enemyId = enemyEntry.enemyId;
    }
    
    /// <summary>
    /// Adjust platforms height when loading in, also prevents them from starting 2 lerp-corutines at the same time when loading in scene
    /// </summary>
    /// <param name="saveData"></param>
    private void AdjustPlatforms(SaveData saveData)
    {
        foreach (SavePlatforms platform in saveData.platforms)
        {
            GameObject platformToChange = GameObject.Find(platform.platformName);
            platformToChange.transform.position = platform.position;
            platformToChange.GetComponent<PlatformMover>().LoadIn(platform.moveToStart, platform.currentLerpTime);
            
        }
    }

    /// <summary>
    /// Makes sure all frozen surfaces are kept in the same state when loading
    /// </summary>
    /// <param name="saveData"></param>
    private void AdjustSurfaces(SaveData saveData)
    {
        var oldSurfaces = GameObject.FindGameObjectsWithTag("FrozenSurface");
        foreach (GameObject oldSurface in oldSurfaces)
        {
            Destroy(oldSurface);
        }

        foreach (SaveFrozenSurface newSurface in saveData.frozenSurfaces)
        {
            GameObject spawnedSurface;
            
            switch (newSurface.surfaceType)
            {
                case SlowMelt.SurfaceType.Model1:
                    spawnedSurface = Instantiate(frozenSurfaces[0], newSurface.position, Quaternion.Euler(newSurface.eulerAngles));
                    break;
                case SlowMelt.SurfaceType.Model2:
                    spawnedSurface = Instantiate(frozenSurfaces[1], newSurface.position, Quaternion.Euler(newSurface.eulerAngles));
                    break;
                case SlowMelt.SurfaceType.Model3:
                    spawnedSurface = Instantiate(frozenSurfaces[2], newSurface.position, Quaternion.Euler(newSurface.eulerAngles));
                    break;
                case SlowMelt.SurfaceType.Slowfield:
                    spawnedSurface = Instantiate(frozenSurfaces[3], newSurface.position, Quaternion.Euler(newSurface.eulerAngles));
                    break;
                case SlowMelt.SurfaceType.Icewall:
                    spawnedSurface = Instantiate(frozenSurfaces[4], newSurface.position, Quaternion.Euler(newSurface.eulerAngles));
                    break;
                default:
                    spawnedSurface = null;
                    break;
            }

            if (spawnedSurface == null)
                return;
            
            spawnedSurface.GetComponent<SlowMelt>().LoadOverwrite(newSurface.timeActive);
            spawnedSurface.GetComponent<SlowMelt>().timePassed = newSurface.timeActive;
            spawnedSurface.GetComponent<SlowMelt>().isIce = newSurface.isIce;

        }
    }

    private void AdjustBosses(SaveData saveData)
    {
        BossDeathControls.instance.BossDied(true, 0, saveData.fireBossDead);
        BossDeathControls.instance.BossDied(false, 0, saveData.iceBossDead);
    }
}
