﻿using System;
using System.Collections.Generic;
using System.Linq;
using Enemies;
using UnityEngine;

namespace SaveSystem
{
    /// <summary>
    ///  Stores data that needs to be saved but is otherwise difficult to access here, so it can be easily accessed later
    /// </summary>
    public class SaveDataAccessor : MonoBehaviour
    {
        public static SaveDataAccessor instance;
        [HideInInspector] public bool[] skillsUnlocked;
        public List<GameObject> possibleUnlocks;
        [HideInInspector] public List<EnemyEntry> allEnemies;

        private void Awake()
        {
            instance = this;
            skillsUnlocked = new bool[possibleUnlocks.Count];

            foreach (EnemyDetection enemy in FindObjectsOfType<EnemyDetection>())
            {
                // Give enemy unique ID
                EnemyStats enemyStats = enemy.gameObject.GetComponent<EnemyStats>();
                if (string.IsNullOrEmpty(enemyStats.enemyId))
                    enemyStats.enemyId = Guid.NewGuid().ToString(); 
                
                allEnemies.Add(new EnemyEntry(enemy.gameObject));
            }
        }

        /// <summary>
        ///  Marks skillset that got unlocked as true in the array
        /// </summary>
        /// <param name="unlockedSkillset"></param>
        public void NewSkillUnlocked(GameObject unlockedSkillset)
        {
            for (int i = 0; i < possibleUnlocks.Count; i++)
            {
                if (unlockedSkillset.name == possibleUnlocks[i].name)
                    skillsUnlocked[i] = true;
            }
        }

        /// <summary>
        /// When enemy dies it calls this function and marks itself as dead in the save data list
        /// </summary>
        /// <param name="enemyStats"></param>
        public void MarkDeadEnemy(EnemyStats enemyStats)
        {
            // LINQ simplification of if: we are checking for every entry where the entry ID equalds the given enemyStatId
            foreach (EnemyEntry enemy in allEnemies.Where(enemy => enemy.enemyId == enemyStats.enemyId))
            {
                enemy.alive = false;
            }
        }

        /// <summary>
        /// Updates enemy positions and rotations when save function is called
        /// Very ineffecient as we are looping over 2 lists... Should change later
        /// </summary>
        public void UpdateEnemies()
        {
            var currentEnemies = FindObjectsOfType<EnemyStats>();

            foreach (EnemyStats currentEnemy in currentEnemies)
            {
                foreach (EnemyEntry enemy in allEnemies.Where(enemy => enemy.enemyId == currentEnemy.enemyId))
                {
                    enemy.UpdateOnSave(currentEnemy.gameObject);
                }
            }
        }
    }
}
