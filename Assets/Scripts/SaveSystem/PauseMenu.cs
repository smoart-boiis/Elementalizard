﻿using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool cantPause;
    public static PauseMenu instance;
    [SerializeField] private GameObject pausemenu;
    private bool paused;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            PauseUnpause();
    }

    /// <summary>
    /// Pauses the game, by setting the games timescale to 0.
    /// Therefore to animate something over time one must use Time.UnscaledDeltaTime
    /// </summary>
    /// <param name="pausestatus"></param>
    /// <returns></returns>
    public void PauseUnpause()
    {
        if (cantPause)
            return;
        
        paused = !paused;
        
        Time.timeScale = paused ? 0 : 1;
        pausemenu.SetActive(paused);
        AbilityManager.noInput = paused;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
