﻿using System;
using UnityEngine;

public class OpenLink : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private float tweenTime = 30f;

    private void Start()
    {
        LeanTween.alphaCanvas(canvasGroup, 1, tweenTime);
    }

    public void OpenUrl()
    {
        Application.OpenURL("https://gitlab.com/smoart-boiis/Elementalizard/-/blob/master/README.md");
    }
}
