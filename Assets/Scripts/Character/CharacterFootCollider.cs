﻿using System;
using System.Collections;
using UnityEngine;
using Hellmade.Sound;

namespace Character
{
    /// <summary>
    /// Checks if characters feet are touching water/ground/etc and initates the respective logic
    /// </summary>
    public class CharacterFootCollider : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private CharacterController charcontroller;
        [SerializeField] private float internalJumpCooldown = 0.1f;
        [SerializeField] private int drownrate = -25;
        [SerializeField] private AudioClip landing;
        
        private Coroutine coroutine;
        private bool onCd;
        private bool isTicking;
        private bool isSubmerged;

        private static readonly int IsJumping = Animator.StringToHash("isJumping");
        private static readonly int IsSwimming = Animator.StringToHash("isSwimming");

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer != 13 && other.gameObject.layer != 14)
                return;
            
            // Character entered water 
            animator.SetBool(IsSwimming, true);
            isSubmerged = true;
            ResourceManager.isSwimming = true;
            charcontroller.grounded = false;
            
            if (other.gameObject.layer != 14)
            {
                coroutine = StartCoroutine(ReduceBodyHeat());
                return;
            }

            ResourceManager.isLavaing = true;             // death in lava animation
            ResourceManager.instance.TakeDamage(500, true); // Die when jumping lava
        }

        private void OnTriggerStay(Collider other)
        {
            switch (other.gameObject.layer)
            {
                // 8 is Ground layer
                case 8 when !onCd && !charcontroller.grounded && !isSubmerged:
                    // Exececute jump-end animation
                    charcontroller.animator.SetBool(IsJumping, false);
                    // After landing make jumping available again
                    charcontroller.grounded = true;
                    StartCoroutine(Cooldown());
                    EazySoundManager.PlaySound(landing, Mathf.Clamp01(EazySoundManager.GlobalSoundsVolume), false, transform);
                    break;
                // 10 is Tree-Layer, should be jumpable but not dashable, thus the separation
                case 10 when !onCd && !charcontroller.grounded && !isSubmerged:
                    // Exececute jump-end animation
                    charcontroller.animator.SetBool(IsJumping, false);
                    // After landing make jumping available again
                    charcontroller.grounded = true;
                    StartCoroutine(Cooldown());
                    break;
                // Feet touching water floor
                case 13:
                    charcontroller.grounded = false;
                    charcontroller.animator.SetBool(IsJumping, false);
                    break;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer != 13)
                return;

            // Character left water and as such is not swimming anymore
            isSubmerged = false;
            ResourceManager.isSwimming = false;
            charcontroller.grounded = true;
            animator.SetBool(IsSwimming, false);
            if(coroutine != null)
                StopCoroutine(coroutine);
        }

        /// <summary>
        /// Flags characters internal Jump Cooldown
        /// </summary>
        /// <returns></returns>
        private IEnumerator Cooldown()
        {
            onCd = true;
            yield return new WaitForSeconds(internalJumpCooldown);
            onCd = false;
        }

        /// <summary>
        /// Reduce body heat drastically while in Water
        /// </summary>
        /// <returns></returns>
        private IEnumerator ReduceBodyHeat()
        {
            yield return new WaitForSeconds(1f);
            ResourceManager.instance.UseResource(drownrate);
            coroutine = StartCoroutine(ReduceBodyHeat());
        }
    }
}
