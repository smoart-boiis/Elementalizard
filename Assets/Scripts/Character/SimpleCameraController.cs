﻿using System;
using UnityEngine;



/// <summary>
/// Quick and dirty version from URP Examples
/// Removed wasd/Shift control. We just want rotation :)
/// </summary>
public class SimpleCameraController : MonoBehaviour
{
    public static bool lockCamControl;
    
    public class CameraState
    {
        public float yaw;
        public float pitch;
        public float roll;

        public void SetFromTransform(Transform t)
        {
            pitch = t.eulerAngles.x;
            yaw = t.eulerAngles.y;
            roll = t.eulerAngles.z;
        }

        public void LerpTowards(CameraState target, float rotationLerpPct)
        {
            yaw = Mathf.Lerp(yaw, target.yaw, rotationLerpPct);
            if(target.pitch <= staticAngle && target.pitch >= -staticAngle)
                pitch = Mathf.Lerp(pitch, target.pitch, rotationLerpPct);
            roll = Mathf.Lerp(roll, target.roll, rotationLerpPct);
        }

        public void UpdateTransform(Transform t)
        {
            // Set maximum boundaries for camera x rotation toward top and bottom
            if (pitch > staticAngle)
                pitch = staticAngle;

            if (pitch < -staticAngle)
                pitch = -staticAngle;

            t.eulerAngles = new Vector3(pitch, yaw, roll);

        }
    }

    CameraState m_TargetCameraState = new CameraState();
    CameraState m_InterpolatingCameraState = new CameraState();

    [Header("Movement Settings")]
    [Tooltip("Exponential boost factor on translation, controllable by mouse wheel.")]
    public float boost = 3.5f;

    [Tooltip("Time it takes to interpolate camera position 99% of the way to the target."), Range(0.001f, 1f)]
    public float positionLerpTime = 0.2f;

    [Header("Rotation Settings")]
    [Tooltip("X = Change in mouse position.\nY = Multiplicative factor for camera rotation.")]
    public AnimationCurve mouseSensitivityCurve = new AnimationCurve(new Keyframe(0f, 0.5f, 0f, 5f), new Keyframe(1f, 2.5f, 0f, 0f));

    [Tooltip("Time it takes to interpolate camera rotation 99% of the way to the target."), Range(0.001f, 1f)]
    public float rotationLerpTime = 0.01f;

    [Tooltip("Whether or not to invert our Y axis for mouse input to rotation.")]
    public bool invertY = false;

    [SerializeField] private float maxTopBotAngle = 90f;
    private static float staticAngle;
    

    private void OnEnable()
    {
        m_TargetCameraState.SetFromTransform(transform);
        m_InterpolatingCameraState.SetFromTransform(transform);
    }

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Confined;
        staticAngle = maxTopBotAngle;
    } 

    private void Update()
    {
        // No rotatrion when cam locked 
        if (lockCamControl)
            return;

        // Rotation
        if (Input.GetMouseButton(1))
        {
            Vector2 mouseMovement = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y") * (invertY ? 1 : -1));
            float mouseSensitivityFactor = mouseSensitivityCurve.Evaluate(mouseMovement.magnitude);

            m_TargetCameraState.yaw += mouseMovement.x * mouseSensitivityFactor;
            if( m_TargetCameraState.pitch + mouseMovement.y * mouseSensitivityFactor <= staticAngle &&
                m_TargetCameraState.pitch + mouseMovement.y * mouseSensitivityFactor >= -staticAngle)
                m_TargetCameraState.pitch += mouseMovement.y * mouseSensitivityFactor;
        }

        // Framerate-independent interpolation
        // Calculate the lerp amount, such that we get 99% of the way to our target in the specified tim
        float rotationLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / rotationLerpTime) * Time.deltaTime);
        m_InterpolatingCameraState.LerpTowards(m_TargetCameraState, rotationLerpPct);

        m_InterpolatingCameraState.UpdateTransform(transform);

    }

}
