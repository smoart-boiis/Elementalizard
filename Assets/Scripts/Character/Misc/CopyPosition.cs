﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Copies the Position of the actual characterobject and sets it localposition to 0, so the total position does not change
/// This way we can rotate the character, without rotating the camera with him
/// </summary>
public class CopyPosition : MonoBehaviour
{
    [SerializeField] private Transform character;
    [SerializeField] private float smoothtime = 0.1f;
    private Vector3 velocity = Vector3.zero;

    private void Awake()
    {
        transform.position = character.position;
    }

    private void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, character.position, ref velocity, smoothtime);
    }
}
