﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using CharacterController = Character.CharacterController;

/// <summary>
/// Scales blobshadow under the character according to animation, as our character goes to quadruped from biped when walking
/// </summary>
public class BlobShadowRotationScale : MonoBehaviour
{
    [SerializeField] private float minScale = 2.6f;
    [SerializeField] private float maxScale = 4f;
    [FormerlySerializedAs("character_control")] [SerializeField] Transform characterControl;
    private CharacterController characterController;
    private Transform thisTransform;

    private void Start()
    {
        characterController = characterControl.GetComponent<CharacterController>();
        thisTransform = transform;
    }

    // Update is called once per frame
    private void Update()
    {
        float newZScale = Mathf.Lerp(minScale, maxScale, characterController.currentSpeed); // Scale shadow in x direction if in run animation
        Vector3 newScale = thisTransform.localScale;
        newScale.z = newZScale;
        thisTransform.localScale = newScale;
    }
}
