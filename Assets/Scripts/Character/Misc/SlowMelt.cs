﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Used on fields created by Freeze Surface ability to slowly melt and then disappear
/// </summary>
public class SlowMelt : MonoBehaviour
{
    [System.Serializable]
    public enum SurfaceType
    {
        Model1,
        Model2,
        Model3,
        Slowfield,
        Icewall
    };

    public SurfaceType surfaceType;
    [SerializeField] private Material iceMaterial;

    [HideInInspector]  public bool isIce;
    [HideInInspector] public float timePassed;

    [SerializeField] private AnimationCurve animationCurve;
    [SerializeField] private float delay;
    [SerializeField] private float meltTime;
    
    [SerializeField] private bool xAxis = true;
    [SerializeField] private float xTarget = 0;
    [SerializeField] private bool yAxis;
    [SerializeField] private float yTarget = 0;
    [SerializeField] private bool zAxis = true;
    [SerializeField] private float zTarget = 0;

    private float xStart;
    private float yStart;
    private float zStart;
    private Coroutine coroutine;
    private float currentLerpTime;

    private void Awake()
    {
        xStart = transform.localScale.x;
        yStart = transform.localScale.y;
        zStart = transform.localScale.z;
    }

    private void Start()
    {
        if (surfaceType != SurfaceType.Slowfield && isIce)
            GetComponent<MeshRenderer>().material = iceMaterial;

        if(coroutine == null)
            coroutine = StartCoroutine(DelayedStart());
    }

    private void Update()
    {
        timePassed += Time.deltaTime;
    }

    public void LoadOverwrite(float timeActive)
    {
        if(coroutine != null)
            StopCoroutine(coroutine);
        
        if (timeActive > delay)
        {
            currentLerpTime = timeActive - delay;
            coroutine = StartCoroutine(Melt());
        }
        else
        {
            delay -= timeActive;
            coroutine = StartCoroutine(DelayedStart());
        }
            
    }

    private IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(delay);
        coroutine = StartCoroutine(Melt());
    }

    private IEnumerator Melt()
    {
        float newX = xStart;
        float newY = yStart;
        float newZ = zStart;

        float percent = 0;
        while (percent < 1)
        {
            currentLerpTime += Time.deltaTime;
            percent = Mathf.Clamp01(currentLerpTime / meltTime);

            if (xAxis)
                newX = Mathf.Lerp(xStart, xTarget, animationCurve.Evaluate(percent));
            if (yAxis)
                newY = Mathf.Lerp(yStart, yTarget, animationCurve.Evaluate(percent));
            if (zAxis)
                newZ = Mathf.Lerp(zStart, zTarget, animationCurve.Evaluate(percent));
            
            transform.localScale = new Vector3(newX, newY, newZ);
                
            yield return new WaitForFixedUpdate();
        }
        
        
        Destroy(gameObject);
    }
}
