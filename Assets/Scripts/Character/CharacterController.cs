﻿using System;
using System.Collections;
using UnityEngine;
using Hellmade.Sound;

namespace Character
{
    public class CharacterController : MonoBehaviour
    {
        /// <summary>
        /// Flag to allow character movement (locked during casting for example)
        /// </summary>
        public static bool lockMovement = false;
        
        /// <summary>
        /// Status that checks if Character is airborne - is written to by FootCollider Object
        /// </summary>
        [HideInInspector] public bool grounded = true; 
        [HideInInspector] public Animator animator;

        /// <summary>
        /// Current character speed, accessed by blobshadow-scaler
        /// </summary>
        [HideInInspector] public float currentSpeed;
        
        [SerializeField] private Transform charactercam;
        [SerializeField] private float movespeed = 5f;
        [SerializeField] private float jumpVelocity = 5f;
        [SerializeField] private float fallMultiplier = 3f;
        [SerializeField] private float lowJumpMultiplier = 2.5f;
        [SerializeField] private AudioClip jumpSound;
        
        private Rigidbody rb;
        private bool jumpinput;
        private bool moveinput;
        private Vector3 movement;

        private static readonly int IsJumping = Animator.StringToHash("isJumping");
        private static readonly int Speedpercent = Animator.StringToHash("Speedpercent");


        private static GameObject blobshadow;

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            animator = GetComponent<Animator>();

            blobshadow = GetComponentInChildren<BlobShadowRotationScale>().gameObject;
        }

        // Update is called once per frame - check here for User input to not miss it
        private void Update()
        {
            if (lockMovement)
            {
                return;
            }

            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) // Calls move-function if there is any Input on our Movementkeys
            {
                // Calculate forward and right direction in dependance of the camera viewing direction
                Vector3 forward = charactercam.forward;
                Vector3 right = charactercam.right;
                forward.y = 0;
                right.y = 0;

                moveinput = true;
                // Checks for Movementbuttons and calculate total movement
                Vector3 sidewaysmovement = Input.GetAxis("Horizontal") * right.normalized;
                Vector3 forwardmovement = Input.GetAxis("Vertical") * forward.normalized;
                movement = forwardmovement + sidewaysmovement;
            }

            // Check for Jumps and if character has landed after the last jump
            if (Input.GetButtonDown("Jump"))
                jumpinput = true;  
        }

        // Called once per phyic update, apply physics to character if we had user input
        private void FixedUpdate()
        {
            if (lockMovement)
            {
                jumpinput = false;
                moveinput = false;
                return;
            }

            // Check if Character is falling, if that is the case make him fall faster (regular fall seems floaty)
            if (rb.velocity.y < 0)
            {
                rb.velocity += Vector3.up * (Physics2D.gravity.y * fallMultiplier * Time.fixedDeltaTime);
            }
            else if (rb.velocity.y > 0 && !Input.GetButton("Jump")) // Check if Character is jumping but Jump button isnt held for a smaller jump
            {
                rb.velocity += Vector3.up * (Physics2D.gravity.y * lowJumpMultiplier * Time.fixedDeltaTime);
            }

            // Jump
            if (jumpinput)
                Jump();
            
            // Move
            if (moveinput)
                Move(movement);
            else
            {
                // Reduce movmenetspeed if no move-input
                currentSpeed -= 3 * Time.fixedDeltaTime;
                if (currentSpeed < 0)
                    currentSpeed = 0;
            }
            
            // Set speedpercent for the character movement animation-tree
            animator.SetFloat(Speedpercent, currentSpeed);
        }

        /// <summary>
        /// Initate jumping physic
        /// </summary>
        private void Jump()
        {
            if(!grounded)
            {
                return;
            }
        
            EazySoundManager.PlaySound(jumpSound, EazySoundManager.GlobalSoundsVolume, false, transform);
            animator.SetBool(IsJumping, true);
            StartCoroutine(DelayedUngrounded());
            rb.velocity = Vector3.up * jumpVelocity;

            jumpinput = false;
        }

        /// <summary>
        /// Character movement if there was user input
        /// </summary>
        /// <param name="totalmovement"></param>
        private void Move(Vector3 totalmovement)
        {
            // Rotate our character towards the movement direction
            rb.MoveRotation(Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(totalmovement), 15f * Time.fixedDeltaTime));
            // Move our Character
            if(grounded)
                rb.MovePosition(rb.position + totalmovement * (movespeed * Time.fixedDeltaTime));
            else
                rb.MovePosition(rb.position + totalmovement * (movespeed * Time.fixedDeltaTime * 0.75f));

            // Interpolate currentspeed for smooth run-transition
            currentSpeed += 2 * Time.fixedDeltaTime;
            if (currentSpeed > 1)
                currentSpeed = 1;

            moveinput = false;
        }

        private IEnumerator DelayedUngrounded()
        {
            yield return new WaitForSeconds(0.15f);
            grounded = false;
        }

        public static void OnOffBlob()
        {
           blobshadow.SetActive(!blobshadow.activeInHierarchy);
        }
    }
}
