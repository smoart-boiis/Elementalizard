﻿using UnityEngine;
using Cinemachine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/// <summary>
/// Activates camera shake and red vignette when player takes damage
/// </summary>
public class TookDamageCamShake : MonoBehaviour
{
    public static TookDamageCamShake instance;
    private CinemachineBasicMultiChannelPerlin noiseChannel;
    private Vignette vignette;



    private void Awake()
    {
        instance = this;
        noiseChannel = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        VolumeProfile volumeProfile = FindObjectOfType<Volume>().profile;
        if (!volumeProfile.TryGet(out vignette)) throw new System.NullReferenceException(nameof(vignette));
    }
    
    public void TookDamage(float duration = 0.1f, float intensity = 2.5f)
    {
        vignette.color.Override(Color.red);
        LeanTween.value(gameObject, UpdateShakeValue , 0, intensity, duration).setLoopPingPong(1);
        LeanTween.value(gameObject, UpdateVignetteValue, 0.25f, intensity/5, duration).setLoopPingPong(1).setOnComplete(DisableColor); // Make vignette black again after completion
    }

    private void UpdateShakeValue(float val, float ratio)
    {
        noiseChannel.m_AmplitudeGain = val;
    }

    private void UpdateVignetteValue(float val, float ratio)
    {
        vignette.intensity.Override(val);
    }

    private void DisableColor()
    {
        vignette.color.Override(Color.black);
    }
}
