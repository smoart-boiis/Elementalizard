﻿using System.Collections;
using Character.Abilities;
using UnityEngine;
using UnityEngine.Serialization;
using CharacterController = Character.CharacterController;
using Hellmade.Sound;

/// <summary>
/// Takes care of the characters main system: Body Heat as Resource, and its consequences
/// </summary>
public class ResourceManager : MonoBehaviour
{
    public static ResourceManager instance;
    public static bool isSwimming;
    public static bool isLavaing;
    public static bool isJetpacking;

    [FormerlySerializedAs("death_pos")] [SerializeField] private float deathPos = 231;
    [SerializeField] private RectTransform zeiger;
    [SerializeField] private CanvasGroup blackscreen;
    [SerializeField] private int firedeath = 100;
    [SerializeField] private int icedeath = -100;
    [SerializeField] private AudioClip hitSound;
    [SerializeField] private AudioClip death;

    [SerializeField] private GameObject deathUi;
    
    private const float LerpZeigerTime = 0.11f;
    private int resource;
    private Animator animator;
    [HideInInspector] public bool isDead;
    private static readonly int IsJetpackDead = Animator.StringToHash("isJetpackDead");

    private void Awake()
    {
        instance = this;
        animator = GameObject.FindGameObjectWithTag("MainCharacter").GetComponent<Animator>();
    }

    public int GetResource()
    {
        return resource;
    }

    /// <summary>
    /// Only called from savamanager
    /// </summary>
    public void SetResource(int newResource)
    {
        resource = newResource;
        CheckResource();
    }

    /// <summary>
    /// Modify Resource, called when using ability
    /// </summary>
    /// <param name="value"></param>
    public void UseResource(int value)
    {
        resource += value;
        CheckResource();
    }

    /// <summary>
    /// Called when taking damage by enemy
    /// </summary>
    /// <param name="value"></param>
    /// <param name="is_fire"></param>
    public void TakeDamage(int value, bool is_fire)
    {
        TookDamageCamShake.instance.TookDamage();

        if (is_fire)
            resource += value;
        else
            resource -= value;
        
        EazySoundManager.PlaySound(hitSound, Mathf.Clamp01(EazySoundManager.GlobalSoundsVolume), false, transform);

        CheckResource();
    }


    /// <summary>
    /// Checks if bodyheat reached max/min and cause death if so
    /// </summary>
   private void CheckResource()
   {
       // Calculate Zeigerposition
        float zeigerPos = Mathf.Abs(resource) / 100f;
        if (zeigerPos > 1)
            zeigerPos = 1;
        zeigerPos *= deathPos;
        if (resource < 0)
            zeigerPos *= -1;
        StartCoroutine(LerpZeiger(zeigerPos));

        if (isDead)
            return;
        
        if (resource > firedeath)
            FireDeathAnim();
        else if (resource < icedeath)
            IceDeathAnim();
    }

    /// <summary>
    /// Checking if alive, used for more specific case in JetPack-Ability
    /// </summary>
    /// <returns></returns>
   public bool CheckAlive()
   {
       return resource <= 100 && resource >= -100;
   }
   
    /// <summary>
    /// Lerps UI component that displays body temperature
    /// </summary>
    /// <param name="zeigerposition"></param>
    /// <returns></returns>
    private IEnumerator LerpZeiger(float zeigerposition)
    {
        float currenttime = 0;
        float percent = 0;
        Vector2 oldpos = zeiger.transform.localPosition;

        while(percent < 1)
        {
            currenttime += Time.deltaTime;
            percent = currenttime / LerpZeigerTime;
            if (percent > 1)
                percent = 1;
            Vector2 newpos = new Vector2(Mathf.Lerp(oldpos.x, zeigerposition, percent), oldpos.y);
            zeiger.transform.localPosition = newpos;
            yield return null;
        }
    }

    private void FireDeathAnim()
    {
        if(isJetpacking)
            animator.SetBool(IsJetpackDead, true);
        else
            animator.SetBool(isLavaing ? "isMelting" : "isBurnt", true);
        
        CommonDeathLogic();
    }

    private void IceDeathAnim()
    {
        if(isJetpacking)
            animator.SetBool(IsJetpackDead, true);
        else
            animator.SetBool(isSwimming ? "isDrowning" : "isFrozen", true);
        
        CommonDeathLogic();
    }
    
    private void CommonDeathLogic()
    {
        isDead = true;
        EazySoundManager.PlayUISound(death, EazySoundManager.GlobalUISoundsVolume);
        CharacterController.lockMovement = true;
        AbilityManager.noInput = true;
        SaveConditions.instance.AddToStack();
        JetPack jetPack = FindObjectOfType<JetPack>();
        if(jetPack != null)
            Destroy(jetPack.gameObject);
        
        StartCoroutine(FadeBlack());
    }

    /// <summary>
    ///  Fades screen black and reloads scene
    /// </summary>
    /// <returns></returns>
    private IEnumerator FadeBlack()
    {
        yield return new WaitForSeconds(1f);

        blackscreen.gameObject.SetActive(true);
        LeanTween.alphaCanvas(blackscreen, 1, 1f);
        yield return new WaitForSeconds(1.1f);
        deathUi.SetActive(true);
        yield return null;
    }
}
