﻿using UnityEngine;

public class SkillUnlock : MonoBehaviour
{
    private void OnEnable()
    {
        Time.timeScale = 0;
        Character.CharacterController.lockMovement = true;
        AbilityManager.noInput = true;
    }

    public void Close()
    { 
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
        Character.CharacterController.lockMovement = false;
        AbilityManager.noInput = false;
    }
}
