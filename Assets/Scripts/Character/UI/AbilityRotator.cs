﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles visual UI Components that display Character ablities
/// </summary>
public class AbilityRotator : MonoBehaviour
{
    public static AbilityRotator instance;
    
    public float tweentime = 0.1f;
    
    [HideInInspector] public int knownAbilites;
    
    [SerializeField] private Transform fireRoot;
    [SerializeField] private Transform iceRoot;

    private int activeFireAbility;
    private int activeIceAbility;

    private void Awake()
    {
        instance = this;
        knownAbilites = 1;
    }

    /// <summary>
    /// Called when collecitble skill unlock item is picked up
    /// </summary>
    /// <param name="fireAbility"></param>
    /// <param name="iceAbility"></param>
    public void IncreaseAbilites(Texture2D fireAbility, Texture2D iceAbility)
    {
        knownAbilites++;

        iceRoot.GetChild(knownAbilites - 1).gameObject.SetActive(true);
        iceRoot.GetChild(knownAbilites - 1).GetChild(0).GetComponent<RawImage>().texture = iceAbility;
        fireRoot.GetChild(knownAbilites - 1).gameObject.SetActive(true);
        fireRoot.GetChild(knownAbilites - 1).GetChild(0).GetComponent<RawImage>().texture = fireAbility;

        if (knownAbilites < 3)
            return;
        
        // Adjust rotations of the abilities icons to allow for 3 skill set up - if we add 4th skill need to add another case
        if (activeFireAbility == 0)
        {
            // Default rotations for fireabilites
            fireRoot.GetChild(1).transform.localEulerAngles = new Vector3(0, 0, 120f);
            fireRoot.GetChild(1).GetChild(0).transform.localEulerAngles = new Vector3(0, 0, -120f);
        }
        else
        {
            fireRoot.GetChild(0).transform.localEulerAngles = new Vector3(0, 0, 60f);
            fireRoot.GetChild(0).GetChild(0).transform.localEulerAngles = new Vector3(0, 0, -240f);
            
            fireRoot.GetChild(2).transform.localEulerAngles = new Vector3(0, 0, -60f);
            fireRoot.GetChild(2).GetChild(0).transform.localEulerAngles = new Vector3(0, 0, 240f);
        }
        
        // Same for active ice ability
        if (activeIceAbility == 0)
        {
            // Default rotations for fireabilites
            iceRoot.GetChild(1).transform.localEulerAngles = new Vector3(0, 0, 120f);
            iceRoot.GetChild(1).GetChild(0).transform.localEulerAngles = new Vector3(0, 0, -120f);
        }
        else
        {
            iceRoot.GetChild(0).transform.localEulerAngles = new Vector3(0, 0, 60f);
            iceRoot.GetChild(0).GetChild(0).transform.localEulerAngles = new Vector3(0, 0, -240f);
            
            iceRoot.GetChild(2).transform.localEulerAngles = new Vector3(0, 0, -60f);
            iceRoot.GetChild(2).GetChild(0).transform.localEulerAngles = new Vector3(0, 0, 240f);
        }
    }
    
    /// <summary>
    /// Called when player loads game after he unlocked all skills but at the loaded moment only had 2 skills
    /// </summary>
    public void DecreaseAbility()
    {
        if (knownAbilites == 1)
            return;
        
        iceRoot.GetChild(knownAbilites - 1).gameObject.SetActive(false);
        fireRoot.GetChild(knownAbilites - 1).gameObject.SetActive(false);

        switch (knownAbilites)
        {
            case 2:
                if (activeIceAbility != 0)
                    RotateAbility(false);
                if (activeFireAbility != 0)
                    RotateAbility(true);
                break;
            case 3:
                // Hardreset to 1 as active skill
                activeIceAbility = 0;
                AbilityManager.instance.selectedIce = 0;
                iceRoot.localEulerAngles = Vector3.zero;
                iceRoot.GetChild(0).localEulerAngles = Vector3.zero;
                iceRoot.GetChild(0).GetChild(0).localEulerAngles = Vector3.zero;
                iceRoot.GetChild(0).GetChild(0).localScale = Vector3.one;
                iceRoot.GetChild(1).localEulerAngles = new Vector3(0, 0, 180);
                iceRoot.GetChild(1).GetChild(0).localEulerAngles = new Vector3(0, 0, -180);
                iceRoot.GetChild(1).GetChild(0).localScale = new Vector3(0.75f, 0.75f, 0.75f);
                iceRoot.GetChild(2).localEulerAngles = new Vector3(0, 0, -120);
                iceRoot.GetChild(2).GetChild(0).localEulerAngles = new Vector3(0, 0, 120);
                iceRoot.GetChild(2).GetChild(0).localScale = new Vector3(0.75f, 0.75f, 0.75f);
                
                activeFireAbility = 0;
                AbilityManager.instance.selectedFire = 0;
                fireRoot.localEulerAngles = Vector3.zero;
                fireRoot.GetChild(0).localEulerAngles = Vector3.zero;
                fireRoot.GetChild(0).GetChild(0).localEulerAngles = Vector3.zero;
                fireRoot.GetChild(0).GetChild(0).localScale = Vector3.one;
                fireRoot.GetChild(1).localEulerAngles = new Vector3(0, 0, 180);
                fireRoot.GetChild(1).GetChild(0).localEulerAngles = new Vector3(0, 0, -180);
                fireRoot.GetChild(1).GetChild(0).localScale = new Vector3(0.75f, 0.75f, 0.75f);
                fireRoot.GetChild(2).localEulerAngles = new Vector3(0, 0, -120);
                fireRoot.GetChild(2).GetChild(0).localEulerAngles = new Vector3(0, 0, 120);
                fireRoot.GetChild(2).GetChild(0).localScale = new Vector3(0.75f, 0.75f, 0.75f);
                break;
        }

        knownAbilites--;
        Debug.Log("known abilites: " + knownAbilites);
    }


    /// <summary>
    /// Visually rotate selected skill, called from AbilityManager when specific key is pressed
    /// </summary>
    /// <param name="isFire"></param>
    public void RotateAbility(bool isFire)
    {
        if (knownAbilites == 1) // no need to rotate if only one ability per elemnt
            return;

        float rotationvalue = - (360 / knownAbilites);

        int previousAbility;
        int newAbility;
        Transform rotateroot;

        if (isFire)
        {
            rotateroot = fireRoot;
            previousAbility = activeFireAbility;
            activeFireAbility++;
            if(activeFireAbility > knownAbilites-1)
            {
                activeFireAbility = 0;
            }
            newAbility = activeFireAbility;
        }      
        else
        {
            rotateroot = iceRoot;
            previousAbility = activeIceAbility;
            activeIceAbility++;
            if (activeIceAbility > knownAbilites - 1)
            {
                activeIceAbility = 0;
            }
            newAbility = activeIceAbility;
        }


        LeanTween.rotateAround(rotateroot.gameObject, Vector3.forward, rotationvalue, tweentime);

        for(int i = 0; i < knownAbilites; i++)
        {
            Transform icon = rotateroot.GetChild(i).GetChild(0);
            icon.Rotate(Vector3.forward, -rotationvalue);

            if (i == newAbility)
            {
                LeanTween.scale(icon.gameObject, Vector3.one, tweentime);
            }

            if(i == previousAbility)
            {
                LeanTween.scale(icon.gameObject, new Vector3(0.75f, 0.75f, 0.75f), tweentime);
            }
        }
    }

    /// <summary>
    /// Public function that will be called from ability manager that initate the visualisation of an abilites CD and selects the abilites image
    /// </summary>
    /// <param name="abilitycount"></param>
    /// <param name="isFire"></param>
    /// <param name="cooldown"></param>
    public void AbilityCooldownVisuals(int abilitycount, bool isFire, float cooldown)
    {
        Image target = isFire 
            ? fireRoot.GetChild(abilitycount).GetComponentInChildren<Image>()
            : iceRoot.GetChild(abilitycount).GetComponentInChildren<Image>();
        StartCoroutine(VisualizeCooldown(target, cooldown));
    }

    /// <summary>
    /// Lerp the abilites radial mask fill to grey it out according the time left until available
    /// </summary>
    /// <param name="target"></param>
    /// <param name="cooldown"></param>
    /// <returns></returns>
    private static IEnumerator VisualizeCooldown(Image target, float cooldown)
    {
        target.fillAmount = 1;

        float percent = 0;
        float time = 0;

        while(percent < 1)
        {
            time += Time.deltaTime;
            percent = time / cooldown;
            if (percent > 1)
                percent = 1;

            target.fillAmount = 1 - percent;

            yield return null;
        }
    }
}
