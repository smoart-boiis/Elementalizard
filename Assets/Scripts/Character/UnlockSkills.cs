﻿using System.Collections;
using SaveSystem;
using UnityEngine;

public class UnlockSkills : MonoBehaviour
{
    [SerializeField] private AbilityManager.AbilityEntry fireability;
    [SerializeField] private Texture2D fireabilityIcon;
    [SerializeField] private AbilityManager.AbilityEntry iceability;
    [SerializeField] private Texture2D iceabilityIcon;
    [SerializeField] private GameObject skillUnlockUi;
    [HideInInspector] public bool onlyOnce;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("MainCharacter"))
            return;

        if (onlyOnce)
            return;
        
        skillUnlockUi.SetActive(true);
        StartCoroutine(SkillUnlockAnimation());
    }

    private IEnumerator SkillUnlockAnimation()
    {
        onlyOnce = true;
        SkillUnlock();
        yield return new WaitForSeconds(0f);
        gameObject.SetActive(false);
    }

    private void SkillUnlock()
    {
        AbilityManager abilityManager = GameObject.FindGameObjectWithTag("MainCharacter").GetComponent<AbilityManager>();
        abilityManager.fireAbilites.Add(fireability);
        abilityManager.iceAbilites.Add(iceability);
        AbilityRotator.instance.IncreaseAbilites(fireabilityIcon, iceabilityIcon);
        SaveDataAccessor.instance.NewSkillUnlocked(gameObject);
    }

    public void UnlockSkillOnLoad()
    {
        SkillUnlock();
        gameObject.SetActive(false);
    }
}
