﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Cinemachine;

namespace Character.Abilities
{
    // Rotate character with slight modification of SimpleCameraController Class
    // Camera follows and looks at character instead
    
    public class JetPack : MonoBehaviour
    {
        public class CameraState
        {
            public float yaw;
            public float pitch;
            public float roll;

            public void SetFromTransform(Transform t)
            {
                pitch = t.eulerAngles.x;
                yaw = t.eulerAngles.y;
                roll = t.eulerAngles.z;
            }

            public void LerpTowards(CameraState target, float rotationLerpPct)
            {
                yaw = Mathf.Lerp(yaw, target.yaw, rotationLerpPct);
                pitch = Mathf.Lerp(pitch, target.pitch, rotationLerpPct);
                roll = Mathf.Lerp(roll, target.roll, rotationLerpPct);
                
            }

            public void UpdateTransform(Transform t)
            {
                t.eulerAngles = new Vector3(pitch, yaw, roll);
            }
        }
        
        [Header("Movement Settings")]
        [Tooltip("Exponential boost factor on translation, controllable by mouse wheel.")]
        public float boost = 3.5f;

        [Tooltip("Time it takes to interpolate camera position 99% of the way to the target."), Range(0.001f, 1f)]
        public float positionLerpTime = 0.2f;

        [Header("Rotation Settings")]
        [Tooltip("X = Change in mouse position.\nY = Multiplicative factor for camera rotation.")]
        public AnimationCurve mouseSensitivityCurve = new AnimationCurve(new Keyframe(0f, 0.5f, 0f, 5f), new Keyframe(1f, 2.5f, 0f, 0f));

        [Tooltip("Time it takes to interpolate camera rotation 99% of the way to the target."), Range(0.001f, 1f)]
        public float rotationLerpTime = 0.01f;

        [Tooltip("Whether or not to invert our Y axis for mouse input to rotation.")]
        public bool invertY = false;
        
        CameraState m_TargetCameraState = new CameraState();
        CameraState m_InterpolatingCameraState = new CameraState();
        
        [SerializeField] private float flySpeed;
        [SerializeField] private int resourceCost;
        private Transform character;
        private Rigidbody characterRb;
        private Animator animator;
        private bool mouseHeld = true;
        private CinemachineVirtualCamera jetpackCam;
        private static readonly int IsJetpacking = Animator.StringToHash("isJetpacking");
        
        [HideInInspector] public List<GameObject> vfxToDestroy = new List<GameObject>();

        /// <summary>
        /// Block regular camera and character control and switch to jetpack controls
        /// </summary>
        private void Awake()
        {
            ResourceManager.isJetpacking = true;
            SaveConditions.instance.AddToStack();
            
            CharacterController characterController = FindObjectOfType<CharacterController>();
            animator = characterController.GetComponent<Animator>();
            character = characterController.transform;
            characterController.grounded = false;
            characterRb = character.GetComponent<Rigidbody>();
            jetpackCam = character.GetComponentInChildren<CinemachineVirtualCamera>();
            StartCoroutine(ReduceBodyheat());
            
            m_TargetCameraState.SetFromTransform(character);
            m_InterpolatingCameraState.SetFromTransform(character);
        }

        private void Update()
        {
            if (Input.GetMouseButton(0))
                mouseHeld = true;
            else
                Destroy(gameObject);
            
            // Rotation
            if (Input.GetMouseButton(1))
            {
                Vector2 mouseMovement = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y") * (invertY ? 1 : -1));

                float mouseSensitivityFactor = mouseSensitivityCurve.Evaluate(mouseMovement.magnitude);

                m_TargetCameraState.yaw += mouseMovement.x * mouseSensitivityFactor;
                m_TargetCameraState.pitch += mouseMovement.y * mouseSensitivityFactor;
            }

            // Framerate-independent interpolation
            // Calculate the lerp amount, such that we get 99% of the way to our target in the specified time
            float rotationLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / rotationLerpTime) * Time.deltaTime);
            m_InterpolatingCameraState.LerpTowards(m_TargetCameraState, rotationLerpPct);

            m_InterpolatingCameraState.UpdateTransform(character);
        }

        private void FixedUpdate()
        {
            if (!mouseHeld)
                return;
            
            characterRb.velocity = character.up * (flySpeed * Time.fixedDeltaTime);
            mouseHeld = false;
        }

        /// <summary>
        /// Object gets destroyed when not using jetpack anymore, as such undo all changes we made to character control
        /// </summary>
        private void OnDestroy()
        {
            if (!ResourceManager.instance.CheckAlive())
                return;
            animator.SetBool(IsJetpacking, false);
            if(!ResourceManager.instance.isDead)
                CharacterController.lockMovement = false;
            CharacterController.OnOffBlob();
            SimpleCameraController.lockCamControl = false;
            LeanTween.rotateX(character.gameObject, 0f, 1f);
            LeanTween.rotateZ(character.gameObject, 0f, 1f);
            jetpackCam.Priority = 0;
            ResourceManager.isJetpacking = false;
            SaveConditions.instance.DelayedPop(1.1f);
            foreach (GameObject vfx in vfxToDestroy)
            {
                Destroy(vfx);
            }
        }

        /// <summary>
        /// Constantly reduce body heat while using the jetpack
        /// </summary>
        /// <returns></returns>
        private IEnumerator ReduceBodyheat()
        {
            ResourceManager.instance.UseResource(resourceCost);
            yield return new WaitForSeconds(1f);
            StartCoroutine(ReduceBodyheat());
        }
    }
}
