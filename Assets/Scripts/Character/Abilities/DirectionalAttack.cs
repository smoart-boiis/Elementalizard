﻿using System.Collections;
using Enemies;
using UnityEngine;
using Hellmade.Sound;

namespace Character.Abilities
{
    /// <summary>
    /// Blueprint for directional skillshot attack
    /// </summary>
    public class DirectionalAttack : Ability
    {
        [SerializeField] private string animBool;
        [SerializeField] private AnimationClip channelClip;
        [SerializeField] private AnimationClip executeClip;
        [SerializeField] private GameObject vfx_clip;
        [SerializeField] private string vfx_parent_name;
        [SerializeField] private bool vfxIsProjectile;
        [SerializeField] private AudioClip sfx;

        private Transform mainCharacter;
        private Camera playercam;
        private LayerMask layermask;

        private bool onlyOneCoroutine;

        private void Awake()
        {
            Transform thisTransform;
            (thisTransform = transform).parent = GameObject.Find("CharacterAbilitySpawn").transform;
            thisTransform.localPosition = Vector3.zero;
            thisTransform.localEulerAngles = new Vector3(0, -180, 0);
            mainCharacter = GameObject.FindGameObjectWithTag("MainCharacter").transform;
            playercam = Camera.main;
            layermask = LayerMask.GetMask("Water", "Ground", "Lava");

            if (executeClip == null || channelClip == null)
                return;
            if (!(casttime < channelClip.length + executeClip.length))
                return;
            casttime = channelClip.length + executeClip.length;
        }

        private void LateUpdate()
        {
            if(isAiming) // If currently still aiming the attack
            {
                Ray ray = playercam.ScreenPointToRay(Input.mousePosition);

                if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layermask))
                    return;
                
                Vector3 direction = (hit.point - mainCharacter.position).normalized;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z), Vector3.up);
                transform.parent.parent.rotation = lookRotation;
            }
            else if(!onlyOneCoroutine) // Otherwise initate the attack
            {
                StartCoroutine(Attack());
            }
        }

        /// <summary>
        /// Characters attack coroutine that combines logic and visuals
        /// </summary>
        /// <returns></returns>
        private IEnumerator Attack()
        {
            onlyOneCoroutine = true;
            // Rotate character so it faces target direction
            StartCoroutine(RotateCharacter(transform.parent.parent.rotation));

            //Lock movement during casttime
            CharacterController.lockMovement = true;

            transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            if (executeClip != null)
            {
                Animator animator = mainCharacter.GetComponent<Animator>();
                animator.SetBool(animBool, true);

                GameObject vfx = null;
                if (vfx_clip != null)
                {
                    Transform parent = GameObject.Find(vfx_parent_name).transform;
                    vfx = Instantiate(vfx_clip, parent.position, parent.rotation);
                    vfx.transform.parent = parent;
                }
                
                yield return new WaitForSeconds(casttime - executeClip.length);

                EazySoundManager.PlaySound(sfx, EazySoundManager.GlobalSoundsVolume, false, mainCharacter);
                
                animator.SetBool(animBool, false);
                
                if (vfxIsProjectile && vfx_clip != null)
                {
                    vfx.transform.parent = null;
                    Vector3 targetDirection = transform.GetChild(0).position;
                    targetDirection.y = vfx.transform.position.y;
                    targetDirection -= vfx.transform.position;
                    targetDirection = vfx.transform.position + 2 * targetDirection;
                    targetDirection.y -= 2.5f;
                    vfx.GetComponentInChildren<IcicleCollision>().MoveIcicle(targetDirection);
                }

                yield return new WaitForSeconds(executeClip.length);
            }
            else
            {
                yield return new WaitForSeconds(casttime);
            }
            
            if(!ResourceManager.instance.isDead)
                CharacterController.lockMovement = false;

            if(enemies.Count > 0)
            {
                foreach (GameObject enemy in enemies)
                {
                    enemy.GetComponent<EnemyStats>().ModifyHealth(damage, isFire);
                }
            }

            Destroy(gameObject);
            onlyOneCoroutine = false;
        }

        /// <summary>
        /// Rotate chracter when attacking
        /// </summary>
        /// <param name="targetRot"></param>
        /// <returns></returns>
        private IEnumerator RotateCharacter(Quaternion targetRot)
        {
            float time = 0;
            float percent = 0;
            Quaternion startRot = mainCharacter.transform.rotation;

            while(percent < 1)
            {
                time += Time.deltaTime;
                percent = time / 0.1f;
                if (percent > 1)
                    percent = 1;
                mainCharacter.transform.rotation = Quaternion.Slerp(startRot, targetRot, percent); // Quaternion.Slerp automatically searches the shortest way, fixes the problem of rotationdirection
                transform.parent.parent.rotation = targetRot; // Refresh rotation on child object so it doesnt rotate witht he character
                yield return null;
            }
        }
    }
}
