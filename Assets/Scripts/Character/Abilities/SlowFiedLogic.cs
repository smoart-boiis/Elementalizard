﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enemies;
using UnityEngine;
using UnityEngine.AI;

public class SlowFiedLogic : MonoBehaviour
{
    /// <summary>
    /// Slow amount in percent value of origional speed
    /// </summary>
    [SerializeField][Range(0, 100)]private float slowAmount;
    private List<NavMeshAgent> enemiesSlowed = new List<NavMeshAgent>();
    
    // Slow Enemy on Enter
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != 12) // If not enemy return;
            return;

        NavMeshAgent navmeshagent = other.GetComponent<NavMeshAgent>();
        if (navmeshagent == null)
            return;

        EnemyStats enemyStats = other.GetComponent<EnemyStats>();
        if (enemyStats == null)
            return;

        navmeshagent.speed = enemyStats.slowSpeed;
        enemiesSlowed.Add(navmeshagent);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer != 12) // If not enemy return;
            return;

        NavMeshAgent navmeshagent = other.GetComponent<NavMeshAgent>();
        if (navmeshagent == null)
            return;

        EnemyStats enemyStats = other.GetComponent<EnemyStats>();
        if (enemyStats == null)
            return;

        // This is done to prevent the enemy speeding up again when leaving a 2nd slow field as they are not supposed to stack
        if (Math.Abs(navmeshagent.speed - enemyStats.slowSpeed) > 0.0001f)
            navmeshagent.speed = enemyStats.slowSpeed;
    }

    // Original Speed on leave
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != 12) // If not enemy return;
            return;

        NavMeshAgent navmeshagent = other.GetComponent<NavMeshAgent>();
        if (navmeshagent == null)
            return;

        EnemyStats enemyStats = other.GetComponent<EnemyStats>();
        if (enemyStats == null)
            return;

        navmeshagent.speed = enemyStats.speed;
        enemiesSlowed.Remove(navmeshagent);
    }

    private void OnDestroy()
    {
        // If destroyed while still slowing enemies -> Reset their speed
        if (enemiesSlowed.Count <= 0)
            return;
        
        foreach (NavMeshAgent t in enemiesSlowed)
        {
            if (t == null)
                return;
            EnemyStats enemyStats = t.GetComponent<EnemyStats>();
            if (enemyStats == null)
                return;

            t.speed = enemyStats.speed;
        }
    }
}
