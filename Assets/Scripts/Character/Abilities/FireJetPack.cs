﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Character.Abilities
{
    public class FireJetPack : Ability
    {
        [SerializeField] private GameObject jetpack;
        
        private bool onlyOneCoroutine;
        
        private static readonly int IsJetpacking = Animator.StringToHash("isJetpacking");
        private static readonly int IsJumping = Animator.StringToHash("isJumping");
        
        [SerializeField] private GameObject handVfx;
        [SerializeField] private string handParent1;
        [SerializeField] private string handParent2;
        [SerializeField] private GameObject bodyVfx;
        [SerializeField] private string bodyParent;
        private List<GameObject> vfxParts = new List<GameObject>();

        private void Awake()
        {
            Transform proxyTransform;
            (proxyTransform = transform).parent = FindObjectOfType<CharacterFootCollider>().transform;
            proxyTransform.localPosition = Vector3.zero;
            proxyTransform.localScale = new Vector3(3, 3, 3);
            proxyTransform.localRotation = Quaternion.identity;
            
        }

        private void LateUpdate()
        {
            if (isAiming) // Do nothing when aiming, as is aimed below character
                return;

            StartCoroutine(CastFireJetPack());
        }

        /// <summary>
        /// Prepare Jetpack with inital jump, animation and disable old controls
        /// </summary>
        /// <returns></returns>
        private IEnumerator CastFireJetPack()
        {
            if (onlyOneCoroutine) yield break;
                
            onlyOneCoroutine = true;
            
            // Block saving
            SaveConditions.instance.AddToStack();
            
            FindObjectOfType<CharacterController>().GetComponentInChildren<CinemachineVirtualCamera>().Priority = 20;
            Transform character = FindObjectOfType<CharacterController>().transform;
            Rigidbody characterRb = character.GetComponent<Rigidbody>();
            LeanTween.rotateX(character.gameObject, 75f, 2f);
            
            Animator animator = FindObjectOfType<CharacterController>().GetComponent<Animator>();
            animator.SetBool(IsJetpacking, true);
            CharacterController.OnOffBlob();
            GetComponent<Renderer>().enabled = false;
            CharacterController.lockMovement = true;
            SimpleCameraController.lockCamControl = true;
            
            var timeGone = 0f;
            StartVfx();
            while (timeGone < 2)
            {
                yield return new WaitForFixedUpdate();
                timeGone += Time.fixedDeltaTime;
                characterRb.velocity = character.up * (750 * Time.fixedDeltaTime);
            }
            
            animator.SetBool(IsJumping, true);
            
            GameObject jetpackScript = Instantiate(jetpack);
            jetpackScript.GetComponent<JetPack>().vfxToDestroy = vfxParts;

            SaveConditions.instance.Remove();
            Destroy(gameObject);
            onlyOneCoroutine = false;
        }
        
        private void StartVfx()
        {
            GameObject vfx = Instantiate(handVfx);
            vfx.transform.parent = GameObject.Find(handParent1).transform;
            vfx.transform.localRotation = Quaternion.identity;
            vfx.transform.localPosition = Vector3.zero;
            vfxParts.Add(vfx);
            
            vfx = Instantiate(handVfx);
            vfx.transform.parent = GameObject.Find(handParent2).transform;
            vfx.transform.localRotation = Quaternion.identity;
            vfx.transform.localPosition = Vector3.zero;
            vfxParts.Add(vfx);
            
            vfx = Instantiate(bodyVfx);
            vfx.transform.parent = GameObject.Find(bodyParent).transform;
            vfx.transform.localRotation = Quaternion.identity;
            vfx.transform.localPosition = Vector3.zero;
            vfxParts.Add(vfx);
        }
    }
}
