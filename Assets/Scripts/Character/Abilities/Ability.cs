﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rootclass for abilites that defines all neccessary ability attributes
/// Also provides an enemy list that are currently within the abilites collider
/// </summary>
public class Ability : MonoBehaviour
{
    public bool isFire;
    public int resourcecost;
    public int damage;
    public float cooldown;
    public float casttime;
    /// <summary>
    /// Aiming type of the ability
    /// </summary>
    public bool isAiming = true;
    
    /// <summary>
    /// List of enemies currently in abilites collider (if it has one)
    /// </summary>
    [HideInInspector] public List<GameObject> enemies = new List<GameObject>();

    public void AddEnemy(GameObject enemy)
    {
        if(!enemies.Contains(enemy))
            enemies.Add(enemy);
    }

    public void RemoveEnemy(GameObject enemy)
    {
        if(enemies.Contains(enemy))
            enemies.Remove(enemy);
    }
    
}
