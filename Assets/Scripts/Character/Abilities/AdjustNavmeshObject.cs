﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AdjustNavmeshObject : MonoBehaviour
{
    private NavMeshObstacle navMeshObstacle;

    private void Awake()
    {
        navMeshObstacle = GetComponent<NavMeshObstacle>();
    }

    // Update is called once per frame
    private void Update()
    {
        navMeshObstacle.size = transform.localScale;
    }
}
