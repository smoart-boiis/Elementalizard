﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;
using Hellmade.Sound;

namespace  Character.Abilities
{
    /// <summary>
    /// Logic for surface Freeze Character ability that will either slow the enemy or creat platforms the player can jump on
    /// </summary>
    public class FreezeSurface : Ability
    {
        [SerializeField] private AnimationClip channelClip;
        [SerializeField] private AnimationClip executeClip;
        /// <summary>
        /// Maximum casting range of this ability
        /// </summary>
        [SerializeField] private float maxRange;

        /// <summary>
        /// Slow field, consisting of Decal, that will be spanwed if Ground is targeted
        /// </summary>
        [SerializeField] private GameObject slowField;
        /// <summary>
        /// A few platform models that can be spawned if Lava or Water is targeted, have stone Material by default
        /// </summary>
        [SerializeField] private GameObject[] jumpPlatforms;

        [SerializeField] private Material characterSpellIndicator;
        [SerializeField] private Material notCastable;
        
        [SerializeField] private Material lavaIndicatorCastable;
        [SerializeField] private Material lavaIndicatorNotCastable;

        [SerializeField] private GameObject projectileVfx;
        [SerializeField] private float projectileHeight = 5f;
        [SerializeField] private AnimationCurve projectileHeightCurve;
        [SerializeField] private AudioClip sfx;
        private GameObject vfxProj;

        private Renderer thisRenderer;
        private Vector3 targetPos;
        private LayerMask layermask;
        private Camera playerCam;
        private Transform character;
        private Transform mainCharacter;
        private bool oneCoroutine;
        private int randomPlatform;
        private Quaternion randomRotation;
        private Transform spellIndicator;
        private static readonly int Surface = Animator.StringToHash("FreezeSurface");

        private void Awake()
        {
            character = GameObject.Find("AbilitySpawnRotateParent").transform;
            mainCharacter = character.root;
            thisRenderer = GetComponent<Renderer>();
            layermask = LayerMask.GetMask("Water", "Lava", "Ground");
            playerCam = Camera.main;
            
            // Set random platform and rotation once the spell is pressed
            randomRotation = RandomYRotation();
            randomPlatform = RandomIndex();
            // Set up an indicator proxyobject
            GameObject indicator = Instantiate(jumpPlatforms[randomPlatform], Vector3.zero, randomRotation);
            indicator.GetComponent<ScaleUp>().isPreview = true;
            spellIndicator = indicator.transform;
            spellIndicator.parent = transform;
            spellIndicator.localPosition = Vector3.zero;
            Destroy(indicator.GetComponent<SlowMelt>());
            Destroy(indicator.GetComponent<MeshCollider>());
            indicator.SetActive(false);
            
            if (!(casttime < channelClip.length + executeClip.length))
                return;
            Debug.Log("Increase Cast time or reduce anim clip length");
            casttime = channelClip.length + executeClip.length;
        }

        private void LateUpdate()
        {
            // Raycast Mouse position,
            Ray ray = playerCam.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layermask))
            {
                transform.localPosition = Vector3.zero;
                AbilityManager.castable = false;
                thisRenderer.material = notCastable;
                return;
            }
            
            if (isAiming)
            {
                // Aiming logic here
                transform.position = hit.point;
                if (Vector3.Distance(hit.point, character.position) <= maxRange)
                {
                    AbilityManager.castable = true;

                    switch (hit.transform.gameObject.layer)
                    {
                        case 4: // Water
                            WaterAndLavaIndicator(true, true);
                            break;
                        case 14: // Lava
                            WaterAndLavaIndicator(false, true);
                            break;
                        default:
                            if(thisRenderer.material != characterSpellIndicator)
                                thisRenderer.material = characterSpellIndicator;
                            if(!thisRenderer.enabled)
                                thisRenderer.enabled = true;
                            spellIndicator.gameObject.SetActive(false);
                            break;
                    }
                }
                else
                {
                    AbilityManager.castable = false;
                    switch (hit.transform.gameObject.layer)
                    {
                        case 4: // Water
                            WaterAndLavaIndicator(true, false);
                            Debug.Log("water not in range");
                            break;
                        case 14: // Lava
                            WaterAndLavaIndicator(false, false);
                            break;
                        default:
                            if(thisRenderer.material != notCastable)
                                thisRenderer.material = notCastable;
                            if(!thisRenderer.enabled)
                                thisRenderer.enabled = true;
                            spellIndicator.gameObject.SetActive(false);
                            break;
                    }
                }
            }
            else
            {
                if(!oneCoroutine)
                    StartCoroutine(Attack(hit));
            }
        }

        private void WaterAndLavaIndicator(bool isWater, bool inRange)
        {
            thisRenderer.enabled = false;
            
            if(!spellIndicator.gameObject.activeInHierarchy)
                spellIndicator.gameObject.SetActive(true);

            if (isWater)
            {
                if(spellIndicator.transform.localPosition.y != 0)
                    spellIndicator.transform.localPosition = Vector3.zero;
            }
            else
            {
                if(spellIndicator.transform.localPosition.y != 0.2f) // add 0.2 height with a parent y-scale of 15 to achieve a global height of 3
                    spellIndicator.transform.localPosition = new Vector3(0, 0.2f, 0);
            }

            if (inRange)
            {
                if (spellIndicator.GetComponent<MeshRenderer>().material != lavaIndicatorCastable)
                    spellIndicator.GetComponent<MeshRenderer>().material = lavaIndicatorCastable;
            }
            else
            {
                if (spellIndicator.GetComponent<MeshRenderer>().material != lavaIndicatorNotCastable)
                    spellIndicator.GetComponent<MeshRenderer>().material = lavaIndicatorNotCastable;
            }
        }

        private IEnumerator Attack(RaycastHit hit)
        {
            oneCoroutine = true;
            if (!AbilityManager.castable)
            {
                isAiming = true;
                oneCoroutine = false;
                yield break;
            }
            
            //Lock movement during casttime
            CharacterController.lockMovement = true;
            
            // Rotate Character towards cast
            Vector3 relativePos = hit.point - mainCharacter.position;
            Quaternion targetRot = Quaternion.LookRotation(relativePos);
            relativePos = mainCharacter.eulerAngles;
            relativePos.y = targetRot.eulerAngles.y;
            targetRot = Quaternion.Euler(relativePos);
            StartCoroutine(RotateCharacter(targetRot));
            
            Animator animator = mainCharacter.GetComponent<Animator>();
            animator.SetBool(Surface, true);
            EazySoundManager.PlaySound(sfx, Mathf.Clamp01(EazySoundManager.GlobalSoundsVolume + 0.25f), false, mainCharacter);
            
            yield return new WaitForSeconds(casttime - executeClip.length);
            animator.SetBool(Surface, false);
            vfxProj = Instantiate(projectileVfx);
            vfxProj.transform.position = GameObject.Find("MouthPos").transform.position;
            LeanTween.moveX(vfxProj, hit.point.x, executeClip.length);
            LeanTween.moveZ(vfxProj, hit.point.z, executeClip.length);
            LeanTween.value(vfxProj, MoveZwithOffset, vfxProj.transform.position.y, hit.point.y, executeClip.length);
            yield return new WaitForSeconds(executeClip.length);
            if(!ResourceManager.instance.isDead)
                CharacterController.lockMovement = false;

            // Determine surface
            switch (hit.transform.gameObject.layer)
            {
                case 14: // Lava
                    // Set random object to spawn
                    Instantiate(jumpPlatforms[randomPlatform], hit.point + new Vector3(0, 3, 0), randomRotation);
                    break;
                case 4: // Water
                    // Set random object to spawn and change material
                    GameObject newIsland = Instantiate(jumpPlatforms[randomPlatform], hit.point, randomRotation);
                    newIsland.GetComponent<SlowMelt>().isIce = true;
                    break;
                case 8: // Ground
                    // Set slowfield object to spawn
                    Instantiate(slowField, hit.point, RandomYRotation());
                    break;
            }

            oneCoroutine = false;
            Destroy(gameObject);
        }

        private void MoveZwithOffset(float val, float ratio)
        {
            Vector3 oldPos = vfxProj.transform.position;
            float yOffset = Mathf.Lerp(0, projectileHeight, projectileHeightCurve.Evaluate(ratio));
            oldPos.y = val + yOffset;
            vfxProj.transform.position = oldPos;
        }
        
        /// <summary>
        /// Rotate chracter when attacking
        /// </summary>
        /// <param name="targetRot"></param>
        /// <returns></returns>
        private IEnumerator RotateCharacter(Quaternion targetRot)
        {
            float time = 0;
            float percent = 0;
            Quaternion startRot = mainCharacter.transform.rotation;

            while(percent < 1)
            {
                time += Time.deltaTime;
                percent = time / 0.1f;
                if (percent > 1)
                    percent = 1;
                mainCharacter.transform.rotation = Quaternion.Slerp(startRot, targetRot, percent); // Quaternion.Slerp automatically searches the shortest way, fixes the problem of rotationdirection
                yield return null;
            }
        }

        private static Quaternion RandomYRotation()
        {
            // Get random Y rotation
            float randomY = Random.Range(0, 360);
            Vector3 randomEuler = Vector3.zero;
            randomEuler.y = randomY;
            
            return Quaternion.Euler(randomEuler);
        }

        private int RandomIndex()
        {
            float randomF = Random.Range(0, jumpPlatforms.Length - 0.00001f);
            float rest = randomF % 1;
            return (int)(randomF - rest);
        }
        
        private void OnDestroy()
        {
            AbilityManager.castable = true;
        }
    }
}
