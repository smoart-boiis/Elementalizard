﻿using System;
using UnityEngine;
using System.Collections;
using Enemies;
using Hellmade.Sound;

namespace Character.Abilities
{
    public class FireDash : Ability
    {
        private const int DashSampleSize = 15;
        
        private Transform mainCharacter;
        private Rigidbody mainCharRb;
        private Camera playercam;
        private LayerMask layermask;
        private Transform aimpoint;

        private bool onlyOneCoroutine;
        private Vector3 dashEndPoint;

        [SerializeField] private GameObject footVfx;
        [SerializeField] private string footVfxParent;
        [SerializeField] private GameObject handVfx;
        [SerializeField] private string handParent1;
        [SerializeField] private string handParent2;
        [SerializeField] private AudioClip sfx;

        private void Awake()
        {
            Transform thisTransform;
            (thisTransform = transform).parent = GameObject.Find("CharacterAbilitySpawn").transform;
            thisTransform.localPosition = Vector3.zero;
            thisTransform.localEulerAngles = new Vector3(0, -180, 0);
            mainCharacter = GameObject.FindGameObjectWithTag("MainCharacter").transform;
            mainCharRb = mainCharacter.GetComponent<Rigidbody>();
            playercam = Camera.main;
            layermask = LayerMask.GetMask("Water", "Ground", "Lava");
            aimpoint = thisTransform.GetChild(0).GetChild(0);
        }

        private void LateUpdate()
        {
            if(isAiming) // If currently still aiming the attack
            {
                Ray ray = playercam.ScreenPointToRay(Input.mousePosition);

                if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layermask))
                    return;
                
                Vector3 direction = (hit.point - mainCharacter.position).normalized;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z), Vector3.up);
                transform.parent.parent.rotation = lookRotation;
                
                //GetDashEndPos();
            }
            else if(!onlyOneCoroutine) // Otherwise initate the attack
            {
                StartCoroutine(Attack());
            }
        }

        /// <summary>
        /// Characters attack coroutine that combines logic and visuals
        /// </summary>
        /// <returns></returns>
        private IEnumerator Attack()
        {
            onlyOneCoroutine = true;

            GetComponentInChildren<MeshRenderer>().enabled = false;

            if (!AbilityManager.castable)
            {
                isAiming = true;
                onlyOneCoroutine = false;
                yield break;
            }
            
            // Rotate character so it faces target direction
            StartCoroutine(RotateCharacter(transform.parent.parent.rotation));
            
            if(enemies.Count > 0)
            {
                foreach (GameObject enemy in enemies)
                {
                    enemy.GetComponent<EnemyStats>().ModifyHealth(damage, isFire);
                }
            }
            
            // Tween x and z coords of character to target-pos, y-pos gets handled by ground collision
            LeanTween.value(gameObject, UpdateCharacterPosition, mainCharacter.position, GetDashEndPos(), 0.33f);

            Animator animator = GameObject.FindGameObjectWithTag("MainCharacter").GetComponent<Animator>();
            animator.SetBool("isDashing", true);
            StartVfx();

            EazySoundManager.PlaySound(sfx, EazySoundManager.GlobalSoundsVolume, false, mainCharacter);
            
            //Lock movement during casttime
            CharacterController.lockMovement = true;
            yield return new WaitForSeconds(0.33f);
            animator.SetBool("isDashing", false);
            if(!ResourceManager.instance.isDead)
                CharacterController.lockMovement = false;

            Destroy(gameObject);
            onlyOneCoroutine = false;
        }

        /// <summary>
        /// Rotate chracter when attacking
        /// </summary>
        /// <param name="targetRot"></param>
        /// <returns></returns>
        private IEnumerator RotateCharacter(Quaternion targetRot)
        {
            float time = 0;
            float percent = 0;
            Quaternion startRot = mainCharRb.rotation;

            while(percent < 1)
            {
                time += Time.deltaTime;
                percent = time / 0.1f;
                if (percent > 1)
                    percent = 1;
                mainCharRb.MoveRotation(Quaternion.Slerp(startRot, targetRot, percent)); // Quaternion.Slerp automatically searches the shortest way, fixes the problem of rotationdirection
                transform.parent.parent.rotation = targetRot; // Refresh rotation on child object so it doesnt rotate witht he character
                yield return null;
            }
        }

        /// <summary>
        /// Retrieves the target and position for the dash via a child transform on the indicators opposite end
        /// </summary>
        /// <returns></returns>
        private Vector3 GetDashEndPos()
        {
            //Works fine for now; In the future might need to rethink the max slope angle and what surfaces to raycast for
            Vector3 dashEnd = mainCharRb.position;
            Vector3 targetedPosition = aimpoint.position;
            // Will only allow dashing a slope of maximum 45/-45 degrees of the current position
            float maxDistance = Vector3.Distance(mainCharRb.position, targetedPosition);
            // Create a triangle with a 45° angle upwards
            Vector3 lastSamplePoint = targetedPosition;
            lastSamplePoint.y += maxDistance;
            // Caclulate directionVector between startpoint and last sample point
            Vector3 direction = lastSamplePoint - mainCharRb.position;
            // Loop over the set sampling size, starting at a close position and going further with every sample
            for (int i = 1; i <= DashSampleSize; i++)
            {
                // Get a Point at the upper triangle line
                Vector3 samplepoint = mainCharRb.position + (direction * ((float)i/DashSampleSize));
                // From there create a raycast that samples for floor below it with a distance to keep within the max 45/-45 degree slope
                Ray ray = new Ray(samplepoint, Vector3.down);
                
                /* VISUAL DEBUGGING
                Debug.DrawLine(samplepoint, Vector3.down, Color.red, 10f);
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = samplepoint;
                */
                
                // Raycast hits something set new dashend here, in the end dashend will have the furthest possible dash end position
                if (Physics.Raycast(ray, out RaycastHit hit, maxDistance * 2 * ((float)i /DashSampleSize), layermask))
                    dashEnd = hit.point;
                else
                {
                    break;
                }

            }
            return dashEnd;
        }

        private void UpdateCharacterPosition(Vector3 currentTarget)
        {
            mainCharRb.MovePosition(currentTarget);
        }

        private void StartVfx()
        {
            GameObject vfx = Instantiate(footVfx);
            vfx.transform.parent = GameObject.Find(footVfxParent).transform;
            vfx.transform.localRotation = Quaternion.identity;
            vfx.transform.localPosition = Vector3.zero;
            
            vfx = Instantiate(handVfx);
            vfx.transform.parent = GameObject.Find(handParent1).transform;
            vfx.transform.localRotation = Quaternion.identity;
            vfx.transform.localPosition = Vector3.zero;
            
            vfx = Instantiate(handVfx);
            vfx.transform.parent = GameObject.Find(handParent2).transform;
            vfx.transform.localRotation = Quaternion.identity;
            vfx.transform.localPosition = Vector3.zero;
        }
        
    }
}
