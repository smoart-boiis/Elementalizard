﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Hellmade.Sound;
using UnityEngine.UIElements;

public class AbilityManager : MonoBehaviour
{
    public static AbilityManager instance;
    public static bool castable = true;
    public static bool noInput;
    
    public List<AbilityEntry> fireAbilites = new List<AbilityEntry>();
    public List<AbilityEntry> iceAbilites = new List<AbilityEntry>();

    private GameObject selectedAbility;
    [HideInInspector] public int selectedFire;
    [HideInInspector] public int selectedIce;
    private bool isAiming;
    private bool isCasting;
    private bool abilityRotateFireCd;
    private bool abilityRotateIceCd ;

    [SerializeField] private AudioClip switchAbility;

    [Serializable]
    public class AbilityEntry
    {
        public GameObject abilityObject;
        [HideInInspector] public Ability abilityStats;
        [FormerlySerializedAs("on_cooldown")] [HideInInspector] public bool onCooldown;

        public AbilityEntry(GameObject abilityObject, bool onCooldown)
        {
            this.abilityObject = abilityObject;
            abilityStats = this.abilityObject.GetComponent<Ability>();
            this.onCooldown = onCooldown;
        }
    }

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    private void Update()
    {
        if (noInput)
            return;
        
        // Swap Fire spell if pressing Tab
        if(Input.GetKeyDown(KeyCode.Tab) && !isAiming && !abilityRotateFireCd)
        {
            selectedFire++;
            if (selectedFire >= fireAbilites.Count)
                selectedFire = 0;
    
            if(fireAbilites.Count > 1)
                EazySoundManager.PlayUISound(switchAbility, EazySoundManager.GlobalUISoundsVolume);
            AbilityRotator.instance.RotateAbility(true);
            StartCoroutine(QuickSwapCd(true));
        }

        // Swap Ice spell if pressing R
        if (Input.GetKeyDown(KeyCode.R) && !isAiming && !abilityRotateIceCd)
        {
            selectedIce++;
            if (selectedIce >= iceAbilites.Count)
                selectedIce = 0;
            
            if(iceAbilites.Count > 1)
                EazySoundManager.PlayUISound(switchAbility, EazySoundManager.GlobalUISoundsVolume);
            AbilityRotator.instance.RotateAbility(false);
            StartCoroutine(QuickSwapCd(false));
        }

        // Dont allow new aiming / firing when still casting something
        if (isCasting)
            return;

        // If not aiming curently, a mouse button is pressed with a valid ability selected enter aiming mode
        if(!isAiming)
        {
            if (Input.GetKeyDown(KeyCode.Q) && !fireAbilites[selectedFire].onCooldown)
            {
                selectedAbility = Instantiate(fireAbilites[selectedFire].abilityObject);
                isAiming = true;
            }
            else if (Input.GetKeyDown(KeyCode.E) && !iceAbilites[selectedIce].onCooldown)
            {
                selectedAbility = Instantiate(iceAbilites[selectedIce].abilityObject);
                isAiming = true;
            }
        }
        else // if aiming activate skill on left button press, cancel on middle mouse button press
        {
            if (Input.GetKeyDown(KeyCode.Q) && !fireAbilites[selectedFire].onCooldown)
            {
                Destroy(selectedAbility);
                selectedAbility = Instantiate(fireAbilites[selectedFire].abilityObject);
                isAiming = true;
            }
            else if (Input.GetKeyDown(KeyCode.E) && !iceAbilites[selectedIce].onCooldown)
            {
                Destroy(selectedAbility);
                selectedAbility = Instantiate(iceAbilites[selectedIce].abilityObject);
                isAiming = true;
            }
            
            if (Input.GetMouseButtonDown(0))
            {
                if (!castable)
                    return;
                selectedAbility.GetComponent<Ability>().isAiming = false;
                isAiming = false;
                StartCoroutine(CastAbility(selectedAbility));
            }
            else if (Input.GetMouseButtonDown(2))
            {
                Destroy(selectedAbility);
                selectedAbility = null;
                isAiming = false;
            }
        }
    }

    /// <summary>
    /// Wait for the selected abliites cast time and take the resource cost
    /// </summary>
    /// <param name="castedAbility"></param>
    /// <returns></returns>
    private IEnumerator CastAbility(GameObject castedAbility)
    {
        Ability ability = castedAbility.GetComponent<Ability>();

        // Get relevant information about selected ability before unlocking ability swapping
        int selected = ability.isFire ? selectedFire : selectedIce;

        // release aiming lock and ability
        isAiming = false; 
        selectedAbility = null;
        
        // Take resourcecost
        ResourceManager.instance.UseResource(ability.resourcecost);


        // wait for the cast time
        isCasting = true; 
        yield return new WaitForSeconds(ability.casttime);
        isCasting = false;

        // Start ability cooldown
        StartCoroutine(AbilityCooldown(ability.isFire, selected, ability.cooldown));
    }

    /// <summary>
    /// Flag the Cooldown variable of the casted ablity
    /// </summary>
    /// <param name="isFire"></param>
    /// <param name="selected"></param>
    /// <param name="cooldown"></param>
    /// <returns></returns>
    private IEnumerator AbilityCooldown(bool isFire, int selected, float cooldown)
    {
        SaveConditions.instance.AddToStack();
        
        if (isFire)
            fireAbilites[selected].onCooldown = true;
        else
            iceAbilites[selected].onCooldown = true;

        AbilityRotator.instance.AbilityCooldownVisuals(selected, isFire, cooldown);

        yield return new WaitForSeconds(cooldown);

        if (isFire)
            fireAbilites[selected].onCooldown = false;
        else
            iceAbilites[selected].onCooldown = false;
        
        SaveConditions.instance.Remove();
    }

    /// <summary>
    /// Small internal CD between swapping abilites
    /// </summary>
    /// <param name="isFire"></param>
    /// <returns></returns>
    private IEnumerator QuickSwapCd(bool isFire)
    {
        if (isFire)
            abilityRotateFireCd = true;
        else
            abilityRotateIceCd = true;

        yield return new WaitForSeconds(AbilityRotator.instance.tweentime);

        if (isFire)
            abilityRotateFireCd = false;
        else
            abilityRotateIceCd = false;
    }
}
