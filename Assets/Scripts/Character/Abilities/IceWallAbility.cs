﻿using System.Collections;
using UnityEngine;
using Hellmade.Sound;

namespace Character.Abilities
{
    public class IceWallAbility : Ability
    {
        [SerializeField] private AnimationClip channelClip;
        [SerializeField] private AnimationClip executeClip;
        
        /// <summary>
        /// Maximum casting range of this ability
        /// </summary>
        [SerializeField] private float maxRange;

        [SerializeField] private GameObject iceWall;
        
        [SerializeField] private Material characterSpellIndicator;
        [SerializeField] private Material notCastable;
        
        private Transform character;
        private Transform mainCharacter;
        private Renderer thisRenderer;
        private Camera playerCam;
        private LayerMask layermask;
        private bool oneCoroutine;
        private static readonly int Icewall = Animator.StringToHash("Icewall");

        [SerializeField] private AudioClip sfx;

        private void Awake()
        {
            character = GameObject.Find("AbilitySpawnRotateParent").transform;
            mainCharacter = character.root;
            thisRenderer = GetComponent<Renderer>();
            layermask = LayerMask.GetMask("Water", "Lava", "Ground");
            playerCam = Camera.main;
            
            if (!(casttime < channelClip.length + executeClip.length))
                return;
            Debug.Log("Increase Cast time or reduce anim clip length");
            casttime = channelClip.length + executeClip.length;
        }
        
        private void LateUpdate()
        {
            transform.rotation = Quaternion.LookRotation((transform.position - character.position).normalized, Vector3.up);
            Vector3 eulers = transform.localEulerAngles;
            eulers.x = 0;
            eulers.z = 0;
            transform.localEulerAngles = eulers;
            
            // Raycast Mouse position,
            Ray ray = playerCam.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layermask))
            {
                transform.localPosition = Vector3.zero;
                AbilityManager.castable = false;
                thisRenderer.material = notCastable;
                return;
            }
            
            if (isAiming)
            {
                // Aiming logic here
                transform.position = hit.point;
                if (Vector3.Distance(hit.point, character.position) <= maxRange)
                {
                    AbilityManager.castable = true;
                    thisRenderer.material = characterSpellIndicator;
                }
                else
                {
                    AbilityManager.castable = false;
                    thisRenderer.material = notCastable;
                }
            }
            else
            {
                if(!oneCoroutine)
                    StartCoroutine(Attack());
            }
        }
        
        private IEnumerator Attack()
        {
            oneCoroutine = true;
            if (!AbilityManager.castable)
            {
                isAiming = true;
                oneCoroutine = false;
                yield break;
            }

            thisRenderer.enabled = false;
            
            //Lock movement during casttime
            CharacterController.lockMovement = true;
            
            // Rotate Character towards cast
            Vector3 relativePos = transform.position - mainCharacter.position;
            Quaternion targetRot = Quaternion.LookRotation(relativePos);
            relativePos = mainCharacter.eulerAngles;
            relativePos.y = targetRot.eulerAngles.y;
            targetRot = Quaternion.Euler(relativePos);
            StartCoroutine(RotateCharacter(targetRot));
            
            Animator animator = mainCharacter.GetComponent<Animator>();
            animator.SetBool(Icewall, true);
            yield return new WaitForSeconds(casttime - executeClip.length);
            animator.SetBool(Icewall, false);
            Instantiate(iceWall, transform.position, transform.rotation);
            EazySoundManager.PlaySound(sfx, EazySoundManager.GlobalSoundsVolume, false, mainCharacter);
            yield return new WaitForSeconds(executeClip.length);
            
            if(!ResourceManager.instance.isDead)
                CharacterController.lockMovement = false;

            oneCoroutine = false;
            Destroy(gameObject);
        }
        
        private void OnDestroy()
        {
            AbilityManager.castable = true;
        }
        
        /// <summary>
        /// Rotate chracter when attacking
        /// </summary>
        /// <param name="targetRot"></param>
        /// <returns></returns>
        private IEnumerator RotateCharacter(Quaternion targetRot)
        {
            float time = 0;
            float percent = 0;
            Quaternion startRot = mainCharacter.transform.rotation;

            while(percent < 1)
            {
                time += Time.deltaTime;
                percent = time / 0.1f;
                if (percent > 1)
                    percent = 1;
                mainCharacter.transform.rotation = Quaternion.Slerp(startRot, targetRot, percent); // Quaternion.Slerp automatically searches the shortest way, fixes the problem of rotationdirection
                yield return null;
            }
        }
    }
}
