﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enemies;
using UnityEngine;

namespace Character.Abilities
{
    public class IceWallLogic : MonoBehaviour
    {
        public static bool iceWallActive;
        
        [SerializeField] private float spawnTime = 0.5f;
        private bool dealsDamage = true;
        private List<GameObject> enemiesHit = new List<GameObject>();
        private int damagevalue;
        private bool isFire;
        
        private void Awake()
        {
            iceWallActive = true;
            
            //Trigger Animation
            LeanTween.moveLocalY(gameObject, 0.3f, spawnTime).setEaseInOutExpo();
            LeanTween.scale(gameObject, Vector3.one, spawnTime).setEaseInOutExpo();
            StartCoroutine(DamageOff());
            
            // Take Advantage of the Ability object spawning this wall still being active
            // And retrieve its damage values
            IceWallAbility iceWallAbility = FindObjectOfType<IceWallAbility>();
            isFire = iceWallAbility.isFire;
            damagevalue = iceWallAbility.damage;
        }

        private void OnTriggerEnter(Collider other)
        {
            // Only knocks back & deals damage as long as it "spawning" & only once per enemy
            if (!dealsDamage || enemiesHit.Contains(other.gameObject))
                return;

            enemiesHit.Add(other.gameObject);
            
            if(other.gameObject.layer != 12) // Check for enemy
                return;
            // Deal Damage
            EnemyStats enemyStats = other.gameObject.GetComponent<EnemyStats>();
            if (enemyStats == null)
                return;
            enemyStats.ModifyHealth(damagevalue, isFire);
        }

        private IEnumerator DamageOff()
        {
            yield return new WaitForSeconds(spawnTime);
            dealsDamage = false;
        }

        private void OnDestroy()
        {
            // If this is the only active icewall disable icewallactive bool
            if (FindObjectsOfType<IceWallLogic>().Length <= 0)
                iceWallActive = false;
        }
    }
}
