﻿using System.Collections;
using System.Collections.Generic;
using Character.Abilities;
using Enemies;
using UnityEngine;
using CharacterController = Character.CharacterController;

/// <summary>
/// Attatched to abilites Collision Hitbox-object and will manage the list of enemes that would get hit at that moment
/// </summary>
public class CharacterAbilityCollisionCheck : MonoBehaviour
{
    private Ability ability;
    private LayerMask layermask;
    private Transform character;

    private void Awake()
    {
        ability = transform.parent.GetComponent<Ability>();
        layermask = LayerMask.GetMask("IceWall");
        character = FindObjectOfType<CharacterController>().transform;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EnemyStats>() == null)
            return;
        
        ability.AddEnemy(other.gameObject);
        
        if(IceWallLogic.iceWallActive && CheckLoS(other.transform)) // Remove it again if LoS is blocked
            ability.RemoveEnemy(other.gameObject);
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (!IceWallLogic.iceWallActive || other.GetComponent<EnemyStats>() == null)
            return;
        
        if(CheckLoS(other.transform))
            ability.RemoveEnemy(other.gameObject);
        else
            ability.AddEnemy(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<EnemyStats>() != null)
        {
            ability.RemoveEnemy(other.gameObject);
        }
    }
    
    /// <summary>
    /// Checks if LoS to Main char is blocked by IceWall
    /// </summary>
    /// <returns></returns>
    private bool CheckLoS(Transform enemy)
    {
        float raydistance = Vector3.Distance(character.position, enemy.position);
        Ray ray = new Ray(character.position, (enemy.position - character.position).normalized);
        return Physics.Raycast(ray, out RaycastHit hit, raydistance, layermask);
    }
}
