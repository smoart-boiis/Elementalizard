﻿using System.Collections;
using UnityEngine;
using Hellmade.Sound;

public class PlaySound : MonoBehaviour
{
    [SerializeField] private AudioClip sfx;
    [SerializeField] private bool dontStopOnDestroy;
    [SerializeField] private float playDelay;
    [SerializeField] private bool shouldntLoop;
    private int soundId;

    private void Start()
    {
        StartCoroutine(SoundDelay());
    }

    private IEnumerator SoundDelay()
    {
        yield return new WaitForSeconds(playDelay);
        soundId =  EazySoundManager.PlaySound(sfx, EazySoundManager.GlobalSoundsVolume, !shouldntLoop, transform);
    }

    private void OnDestroy()
    {
        if (dontStopOnDestroy)
            return;
        
        Audio audio = EazySoundManager.GetAudio(soundId);
        audio.Stop();
    }
}
