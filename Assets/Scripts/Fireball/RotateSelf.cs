﻿using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// Script used to rotate object around itself for certain visual effects
/// </summary>
public class RotateSelf : MonoBehaviour
{
    [SerializeField] private float rotSpeed;

    // Update is called once per frame
    private void Update()
    {
        transform.RotateAround(Vector3.up, rotSpeed*Time.deltaTime);
    }
}
