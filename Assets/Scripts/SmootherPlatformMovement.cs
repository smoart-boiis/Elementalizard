﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmootherPlatformMovement : MonoBehaviour
{
    private float yPosDelta;
    private float lastYpos;

    private void Awake()
    {
        lastYpos = transform.position.y;
    }

    private void FixedUpdate()
    {
        yPosDelta = transform.position.y - lastYpos;
        lastYpos = transform.position.y;
    }

    private void OnCollisionStay(Collision other)
    {
        if (!other.gameObject.CompareTag("MainCharacter"))
            return;

        Vector3 oldPos = other.gameObject.GetComponent<Rigidbody>().position;
        oldPos.y += yPosDelta;
        other.gameObject.GetComponent<Rigidbody>().MovePosition(oldPos);
    }
}
