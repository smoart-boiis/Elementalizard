﻿using System;
using UnityEngine;

public class EnableOnDisable : MonoBehaviour
{
    [SerializeField] private GameObject nextGo;

    private void OnDestroy()
    {
        nextGo.SetActive(true);
    }
}
