﻿using System;
using System.Collections;
using UnityEngine;

public class IceBossJump : MonoBehaviour
{
    private void Awake()
    {
        transform.position = FindObjectOfType<ResourceManager>().transform.position;
        StartCoroutine(JumpCoroutine());
    }

    private IEnumerator JumpCoroutine()
    {
        yield return new WaitForSeconds(1.5f);
        GameObject iceBoss = GameObject.FindWithTag("Iceboss");
        LeanTween.move(iceBoss, transform.position, 0.5f);
    }
}
