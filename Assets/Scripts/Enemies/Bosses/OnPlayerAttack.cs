﻿using System.Collections;
using UnityEngine;

public class OnPlayerAttack : BossAttack
{
    private void Start()
    {
        // Set attack on character
        transform.position = ResourceManager.instance.transform.position;
        StartCoroutine(Attack());
    }

    /// <summary>
    /// Simple Cast-Check-Attack Coruoutine
    /// </summary>
    private IEnumerator Attack()
    {
        yield return new WaitForSeconds(castTime); // Cast the attack
        yield return new WaitForFixedUpdate(); // Wait for the latest physics update

        if(characterInside)
        {
            // if character still within the attacks hitbox, deal damage
            ResourceManager.instance.TakeDamage(damage, isFire);
        }
    }
}
