﻿using System;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] private CoreAttackBaseClass coreAttack;
    [SerializeField] private TextMeshProUGUI timerBefore;
    [SerializeField] private TextMeshProUGUI timerAfter;

    private float currentTime;
    
    // Start is called before the first frame update
    private void Awake()
    {
        currentTime = coreAttack.timer;
        ChangeTimer(currentTime);
    }

    // Update is called once per frame
    private void Update()
    {
        currentTime -= Time.unscaledDeltaTime;
        ChangeTimer(currentTime);
    }
    
    private void ChangeTimer(float newTime)
    {
        if (newTime <= 0)
        {
            timerBefore.text = "0";
            timerAfter.text = "00";
            return;
        }

        float afterComma = newTime % 1;
        float beforeComma = newTime - afterComma;
        // Round to 2 digits aftercomma
        afterComma = Mathf.Round(afterComma * 100f);

        timerBefore.text = beforeComma == 0 ? "0" : beforeComma.ToString();

        if (afterComma == 0)
            timerAfter.text = "00";
        else if(afterComma < 10)
            timerAfter.text = "0" + afterComma;
        else
            timerAfter.text = afterComma.ToString();
    }
}
