﻿using System.Collections;
using UnityEngine;

public class SpamButton : CoreAttackBaseClass, ICoreAttack
{
    [SerializeField] private int targetButtonPresses;
    [SerializeField] private RectTransform gradient;

    private float timeGoneBy;
    private int buttonCounter;
    private int result;
    private bool buttonPressed;

    private const float WinPos = 500;

    private void Start()
    {
        StartCoroutine(CheckButtonPresses());
    }

    private IEnumerator CheckButtonPresses()
    {
        while (result == 0)
        {
            yield return new WaitForSecondsRealtime(0.1f);
            
            if (buttonPressed)
            {
                // If button was pressed within the last 0.1s adjust the counter and reset the tag
                buttonCounter += 1;
                buttonPressed = false;
            }

            if (buttonCounter >= targetButtonPresses)
                result = 1;

            float percentGone = timeGoneBy / timer;
            float maxTargetAtTime = WinPos * percentGone;
            float maxTargetFromButtons = WinPos * ((float)buttonCounter / targetButtonPresses);
            float maxButtonsMashed = percentGone * targetButtonPresses;
            float percentButtonsMashed = Mathf.Clamp01(buttonCounter / maxButtonsMashed);
            float maxPositive = maxTargetAtTime > maxTargetFromButtons ? maxTargetAtTime : maxTargetFromButtons;
            float currentTarget = Mathf.Lerp(-maxTargetAtTime, maxPositive, percentButtonsMashed);
            Vector3 temp = gradient.localPosition;
            temp.x = currentTarget;
            gradient.localPosition = temp;
        }
    }

    private void Update()
    {
        timeGoneBy += Time.unscaledDeltaTime;
        if (timeGoneBy >= timer) // Failed by exceedind max time
        {
            result = -1;
            Vector3 temp = gradient.localPosition;
            temp.x = -WinPos;
            gradient.localPosition = temp;
        }
        
        if (Input.anyKeyDown)
            buttonPressed = true;
    }

    public int Check()
    {
        return result;
    }
}
