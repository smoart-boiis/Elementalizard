﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CorrectButton : CoreAttackBaseClass, ICoreAttack
{
    [System.Serializable]
    public struct KeyCombo
    {
        public KeyCode keycode;
        public string buttonText;

        private KeyCombo(KeyCode key, string text)
        {
            keycode = key;
            buttonText = text;
        }
    }

    public List<KeyCombo> keycombos = new List<KeyCombo>();

    [SerializeField] private TextMeshProUGUI keyText;
    [SerializeField] private TextMeshProUGUI counterText;
    [SerializeField] private TextMeshProUGUI counterMax;
    [SerializeField] private RawImage highlighter;
    [SerializeField] private float tweenTime = 0.05f;
    [SerializeField] private int successesNeeded = 7;
    [SerializeField] private float timeWindowToPress = 0.3f;
    [SerializeField] private float delay = 0.1f;
    
    private bool isChecking;
    private CanvasGroup canvasGroup;
    private int currentKey;
    private Coroutine checkingCoroutine;
    private int result;
    private int currentSuccessses;
    private float timeGoneBy;

    // Start is called before the first frame update
    private void Start()
    {
        canvasGroup = highlighter.GetComponent<CanvasGroup>();
        counterMax.text = "/" + successesNeeded;
        GetRandomKey();
    }

    // Update is called once per frame
    private void Update()
    {
        timeGoneBy += Time.unscaledDeltaTime;
        if (timeGoneBy >= timer)
        {
            result = -1;
            return;
        }

        if (!isChecking)
            return;
        
        if(Input.GetKeyDown(keycombos[currentKey].keycode))
            SuccessFail(true);
        else if(Input.anyKeyDown)
            SuccessFail(false);
    }

    private void GetRandomKey()
    {
        currentKey = Random.Range(0, keycombos.Count); // Random.Range with int excludes the max
        keyText.text = keycombos[currentKey].buttonText;
        highlighter.color = Color.white;
        LeanTween.alphaCanvas(canvasGroup, 1, tweenTime).setIgnoreTimeScale(true);
        checkingCoroutine = StartCoroutine(CheckingWindow());
    }

    private IEnumerator CheckingWindow()
    {
        yield return new WaitForSecondsRealtime(tweenTime);
        isChecking = true;
        yield return new WaitForSecondsRealtime(timeWindowToPress);
        isChecking = false;
        SuccessFail(false);
    }

    private void SuccessFail(bool success)
    {
        if(checkingCoroutine != null) 
            StopCoroutine(checkingCoroutine);
        checkingCoroutine = null;
        isChecking = false;
        
        if(success)
        {
            highlighter.color = Color.blue;
            currentSuccessses++;
            counterText.text = currentSuccessses.ToString();
            if (currentSuccessses >= successesNeeded)
            {
                result = 1;
                LeanTween.alphaCanvas(canvasGroup, 0, tweenTime).setIgnoreTimeScale(true);
                return;
            }
        }
        else
            highlighter.color = Color.red;

        LeanTween.alphaCanvas(canvasGroup, 0, tweenTime).setIgnoreTimeScale(true);
        StartCoroutine(WaitBeforeNewButton());
    }

    private IEnumerator WaitBeforeNewButton()
    {
        yield return new WaitForSecondsRealtime(tweenTime + delay);
        GetRandomKey();
    }

    public int Check()
    {
        return result;
    }
    
}
