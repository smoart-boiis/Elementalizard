﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StopPointer : CoreAttackBaseClass, ICoreAttack
{
    [SerializeField] private TextMeshProUGUI counterText;
    [SerializeField] private TextMeshProUGUI counterMax;
    [SerializeField] private RawImage pointer;
    [SerializeField] private RectTransform successZone;
    [SerializeField] private float tweenTime = 0.1f;
    [SerializeField] private int successesNeeded = 7;
    [SerializeField] private float pointerTweenTime = 1f;
    [SerializeField] private float maxPointerX = 399;
    [SerializeField] private float successZoneWidth = 80;

    private RectTransform pointerRt;
    private int currentKey;
    private int result;
    private int currentSuccessses;
    private float timeGoneBy;
    private int leanTweenID;
    private bool isResetting;

    // Start is called before the first frame update
    private void Start()
    {
        counterMax.text = "/" + successesNeeded;
        pointerRt = pointer.GetComponent<RectTransform>();
        leanTweenID = LeanTween.moveLocalX(pointerRt.gameObject, maxPointerX, pointerTweenTime).setLoopPingPong().setIgnoreTimeScale(true).id;
        RandomSuccessZonePos();
    }

    // Update is called once per frame
    private void Update()
    {
        timeGoneBy += Time.unscaledDeltaTime;
        
        if(!isResetting && Input.anyKey)
            CheckPointer();
        
        if (!(timeGoneBy >= timer))
            return;
        result = -1;
    }

    private void RandomSuccessZonePos()
    {
        float randomXpos = maxPointerX + 1 + 0.5f * successZoneWidth;
        randomXpos = Random.Range(-randomXpos, randomXpos);
        Vector3 tempPos = successZone.localPosition;
        tempPos.x = randomXpos;
        successZone.localPosition = tempPos;
    }

    private void CheckPointer()
    {
        LeanTween.cancel(leanTweenID);
        bool success = pointerRt.localPosition.x >= successZone.localPosition.x - 0.5f * successZoneWidth && pointerRt.localPosition.x < successZone.localPosition.x + 0.5 * successZoneWidth;
        pointer.color = success ? Color.green : Color.red;
        if (success)
        {
            currentSuccessses++;
            counterText.text = currentSuccessses.ToString();
            if (currentSuccessses >= successesNeeded)
            {
                result = 1;
                return;
            }
        }
        StartCoroutine(ResetChallenger());
    }

    private IEnumerator ResetChallenger()
    {
        isResetting = true;
        yield return new WaitForSecondsRealtime(tweenTime);
        pointer.color = Color.white;
        Vector3 tempPos = pointerRt.localPosition;
        tempPos.x = -maxPointerX;
        pointerRt.localPosition = tempPos;
        leanTweenID = LeanTween.moveLocalX(pointerRt.gameObject, maxPointerX, pointerTweenTime).setLoopPingPong().setIgnoreTimeScale(true).id;
        RandomSuccessZonePos();
        isResetting = false;
    }

    public int Check()
    {
        return result;
    }
}
