﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ClickButtons : CoreAttackBaseClass, ICoreAttack
{
    [SerializeField] private TextMeshProUGUI counterText;
    [SerializeField] private TextMeshProUGUI counterMax;
    [SerializeField] private RawImage highlighter;
    [SerializeField] private float tweenTime = 0.05f;
    [SerializeField] private int successesNeeded = 7;
    [SerializeField] private float delay = 0.1f;
    [SerializeField] private float timeToPress = 0.5f;
    [SerializeField] private float maxHeight = 150;
    [SerializeField] private float maxWidth = 350;
    
    private CanvasGroup canvasGroup;
    private RectTransform buttonToClick;
    private int currentKey;
    private Coroutine checkingCoroutine;
    private int result;
    private int currentSuccessses;
    private float timeGoneBy;

    // Start is called before the first frame update
    private void Start()
    {
        canvasGroup = highlighter.GetComponent<CanvasGroup>();
        buttonToClick = canvasGroup.GetComponent<RectTransform>();
        counterMax.text = "/" + successesNeeded;
        SpawnRandomButton();
    }

    // Update is called once per frame
    private void Update()
    {
        timeGoneBy += Time.unscaledDeltaTime;
        
        if (!(timeGoneBy >= timer))
            return;
        result = -1;
    }

    private void SpawnRandomButton()
    {
        // Put Button to random Pos
        highlighter.color = Color.white;
        Vector3 newPos = buttonToClick.localPosition;
        newPos.x = Random.Range(-maxWidth, maxWidth);
        newPos.y = Random.Range(-maxHeight, maxHeight);
        buttonToClick.localPosition = newPos;
        LeanTween.alphaCanvas(canvasGroup, 1, tweenTime).setIgnoreTimeScale(true);
        checkingCoroutine = StartCoroutine(CheckingWindow());
    }

    private IEnumerator CheckingWindow()
    {
        yield return new WaitForSecondsRealtime(tweenTime);
        canvasGroup.blocksRaycasts = true;
        yield return new WaitForSecondsRealtime(timeToPress);
        checkingCoroutine = null;
        canvasGroup.blocksRaycasts = false;
        highlighter.color = Color.red;
        LeanTween.alphaCanvas(canvasGroup, 0, tweenTime).setIgnoreTimeScale(true);
        StartCoroutine(WaitBeforeNewButton());
    }

    public void Success()
    {
        if(checkingCoroutine != null) 
            StopCoroutine(checkingCoroutine);
        checkingCoroutine = null;

        highlighter.color = Color.blue;
        canvasGroup.blocksRaycasts = false;
        currentSuccessses++;
        counterText.text = currentSuccessses.ToString();
        if (currentSuccessses >= successesNeeded)
        {
            result = 1;
            LeanTween.alphaCanvas(canvasGroup, 0, tweenTime).setIgnoreTimeScale(true);
            return;
        }

        LeanTween.alphaCanvas(canvasGroup, 0, tweenTime).setIgnoreTimeScale(true);
        StartCoroutine(WaitBeforeNewButton());
    }

    private IEnumerator WaitBeforeNewButton()
    {
        yield return new WaitForSecondsRealtime(tweenTime + delay);
        SpawnRandomButton();
    }

    public int Check()
    {
        return result;
    }
    
}
