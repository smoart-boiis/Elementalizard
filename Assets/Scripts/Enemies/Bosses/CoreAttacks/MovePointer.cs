﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MovePointer : CoreAttackBaseClass, ICoreAttack
{
    [SerializeField] private TextMeshProUGUI counterText;
    [SerializeField] private TextMeshProUGUI counterMax;
    [SerializeField] private int successesNeeded = 50;
    [SerializeField] private RectTransform pointer;
    [SerializeField] private RectTransform successZone;
    [SerializeField] private float movementZoneWidth;
    [SerializeField] private float pointerSpeed;
    [SerializeField] private float zonePerHundred;

    private int currentSuccesses;
    private float timeHovered;
    private RawImage pointerImage;
    private int result;
    private float timeGoneBy;

    private void Start()
    {
        counterMax.text = "/" + successesNeeded;
        pointerImage = pointer.GetComponent<RawImage>();
        MoveSuccessZone();
    }

    // Update is called once per frame
    private void Update()
    {
        timeGoneBy += Time.unscaledDeltaTime;

        int direction = 0;
        if (Input.GetKey(KeyCode.A))
            direction -= 1;
        if (Input.GetKey(KeyCode.D))
            direction += 1;
        Move(direction);

        CheckSucces();

        if (!(timeGoneBy >= timer))
            return;
        result = -1;
    }

    private void Move(int direction)
    {
        if (direction == 0)
            return;

        float movement = direction * pointerSpeed * Time.unscaledDeltaTime;
        Vector3 newPos = pointer.localPosition;
        newPos.x += movement;
        float edge = (movementZoneWidth - pointer.rect.width)/2;
        if(newPos.x <= -edge)
            newPos.x = -edge;
        else if (newPos.x > edge)
            newPos.x = edge;
        pointer.localPosition = newPos;
    }

    private void CheckSucces()
    {
        bool success = pointer.localPosition.x >= successZone.localPosition.x - successZone.rect.width/2
            && pointer.localPosition.x < successZone.localPosition.x + successZone.rect.width/2;

        if (success)
        {
            timeHovered += Time.unscaledDeltaTime;
            AdjustCounter();
        }

        pointerImage.color = success ? Color.green : Color.white;
    }
    
    private void AdjustCounter()
    {
        currentSuccesses = (int)(timeHovered * 10);
        counterText.text = currentSuccesses.ToString();
        if (currentSuccesses >= successesNeeded)
            result = 1;
    }

    private void MoveSuccessZone()
    {
        float targetX = movementZoneWidth/2 - successZone.rect.width/2;
        targetX = Random.Range(-targetX, targetX);
        float tweenTime = Mathf.Abs(zonePerHundred * ((targetX - successZone.localPosition.x) / 100));

        LeanTween.moveLocalX(successZone.gameObject, targetX, tweenTime).setIgnoreTimeScale(true).setOnComplete(MoveSuccessZone);
    }

    public int Check()
    {
        return result;
    }
}
