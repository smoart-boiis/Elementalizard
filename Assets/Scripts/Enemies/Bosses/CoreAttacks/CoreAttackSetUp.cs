﻿using System.Collections;
using Enemies;
using UnityEngine;
using Hellmade.Sound;

public class CoreAttackSetUp : MonoBehaviour
{
    [SerializeField] private float tweenTime = 0.25f;
    [SerializeField] private int damageOnFail;
    [SerializeField] private bool isFire;
    [SerializeField] private AudioClip successSfx;
    [SerializeField] private AudioClip failSfx;

    private bool dontCheck = true;
    private ICoreAttack coreAttackChecker;
    private GameObject boss;
    private Animator animator;
    private static readonly int Event = Animator.StringToHash("Event");
    private static readonly int Success = Animator.StringToHash("Success");
    private static readonly int Fail = Animator.StringToHash("Fail");

    // Start is called before the first frame update
    private void Start()
    {
        boss = GameObject.FindWithTag(isFire ? "Fireboss" : "Iceboss");
        animator = boss.GetComponentInChildren<Animator>();
        PauseMenu.cantPause = true;
        AbilityManager.noInput = true;
        coreAttackChecker = transform.GetChild(0).GetComponent<ICoreAttack>();
        StartCoroutine(StartUp());
    }

    private IEnumerator StartUp()
    {
        animator.SetTrigger(Event);
        // Start VFX
        
        // Wait for animationtime
        yield return new WaitForSeconds(1f);
        LeanTween.value(gameObject, SetTimescale, 1, 0, tweenTime).setIgnoreTimeScale(true).setOnComplete(OnStartComplete);
    }

    private static void SetTimescale(float val)
    {
        Time.timeScale = val;
    }

    private void OnStartComplete()
    {
        Time.timeScale = 0;
        dontCheck = false;
        transform.GetChild(0).gameObject.SetActive(true);
    }

    // Update is called once per frame
    private void Update()
    {
        if (dontCheck)
            return;

        int result = coreAttackChecker.Check();
        switch (result)
        {
            case 1:
                // success
                StartCoroutine(Finish(true));
                break;
            case -1:
                // fail
                StartCoroutine(Finish(false));
                break;
            default:
                // Keep checking
                break;
        }
    }

    private IEnumerator Finish(bool success)
    {
        dontCheck = true;
        transform.GetChild(0).gameObject.SetActive(false);
        
        // Take damage on fail
        if(!success)
        {
            EazySoundManager.PlayUISound(failSfx, EazySoundManager.GlobalUISoundsVolume);
            animator.SetTrigger(Fail);
            ResourceManager.instance.TakeDamage(damageOnFail, isFire);
        }
        else // Damage Boss
        {
            EazySoundManager.PlayUISound(successSfx, EazySoundManager.GlobalUISoundsVolume);

            animator.SetTrigger(Success);
            if(boss != null)
                boss.GetComponent<EnemyStats>().ModifyBossHealth();
        }
        
        LeanTween.value(gameObject, SetTimescale, 0, 1, tweenTime).setIgnoreTimeScale(true).setOnComplete(TimeScaleOne);
        
        // Check for success/fail and trigger and wait for that animation
        yield return new WaitForSecondsRealtime(tweenTime);
        Destroy(gameObject);
    }

    private static void TimeScaleOne()
    {
        Time.timeScale = 1;
    }

    private void OnDestroy()
    {
        PauseMenu.cantPause = false;
        AbilityManager.noInput = false;
    }
}
