﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KeepMouseOver : CoreAttackBaseClass, ICoreAttack
{
    [SerializeField] private TextMeshProUGUI counterText;
    [SerializeField] private TextMeshProUGUI counterMax;
    [SerializeField] private float tweenSpeedPerHundred = 0.1f;
    [SerializeField] private int successesNeeded = 7;
    [SerializeField] private RectTransform movingX;
    [SerializeField] private float maxMovement = 150f;
    [SerializeField] private TextMeshProUGUI xText;

    private RawImage hoverButton;
    private int result;
    private int currentSuccessses;
    private float timeGoneBy;
    private float timeHovered;
    private bool isOver;

    // Start is called before the first frame update
    private void Start()
    {
        hoverButton = movingX.GetComponentInChildren<RawImage>();
        counterMax.text = "/" + successesNeeded;
        MoveX();
    }

    // Update is called once per frame
    private void Update()
    {
        timeGoneBy += Time.unscaledDeltaTime;

        if (isOver)
        {
            timeHovered += Time.unscaledDeltaTime;
            AdjustCounter();
        }

        if (!(timeGoneBy >= timer))
            return;
        result = -1;
    }

    private void MoveX()
    {
        Vector3 randomDir = Random.insideUnitCircle;
        randomDir.z = 0;
        randomDir = randomDir.normalized;
        randomDir *= Random.Range(0, maxMovement);

        float tweenSpeed = Mathf.Abs(tweenSpeedPerHundred * (Vector3.Distance(randomDir, movingX.localPosition) / 100));
        LeanTween.moveLocal(movingX.gameObject, randomDir, tweenSpeed).setIgnoreTimeScale(true).setOnComplete(MoveX);
    }

    private void AdjustCounter()
    {
        currentSuccessses = (int)(timeHovered * 10);
        counterText.text = currentSuccessses.ToString();
        if (currentSuccessses >= successesNeeded)
            result = 1;
    }

    public void HoverEvent(bool hovering)
    {
        isOver = hovering;
        hoverButton.color = isOver ? Color.blue : Color.white;
        xText.color = isOver ? Color.blue : Color.white;
    }

    public int Check()
    {
        return result;
    }
}
