﻿using UnityEngine;

public class MouserOvercheck : MonoBehaviour
{
    [SerializeField] private KeepMouseOver keepMouseOver;

    public void EnteredRect()
    {
        keepMouseOver.HoverEvent(true);
    }

    public void LeftRect()
    {
        keepMouseOver.HoverEvent(false);
    }
}
