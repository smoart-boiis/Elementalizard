﻿using UnityEngine;

public class BossAttack : MonoBehaviour
{
    public float castTime = 1f;
    public int damage = 20;
    public bool isFire;

    [HideInInspector] public bool characterInside;
}
