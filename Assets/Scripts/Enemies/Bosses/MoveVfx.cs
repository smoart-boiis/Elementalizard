﻿using UnityEngine;

public class MoveVfx : MonoBehaviour
{
    [SerializeField] private float tweenTime = 0.5f;
    
    // Start is called before the first frame update
    private void Start()
    {
        Vector3 endPosition = transform.parent.GetChild(0).position;
        endPosition -= transform.position;
        endPosition *= 2;
        endPosition += transform.position;

        LeanTween.move(gameObject, endPosition, tweenTime);
    }
}
