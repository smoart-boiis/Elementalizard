﻿using UnityEngine;

public class ProjectileCollision : MonoBehaviour
{
    [SerializeField] private int damage = 15;
    [SerializeField] private bool isFire = true;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("IceWall"))
            Destroy(gameObject);
        
        if (other.CompareTag("MainCharacter"))
            ResourceManager.instance.TakeDamage(damage, isFire);
        
    }
}
