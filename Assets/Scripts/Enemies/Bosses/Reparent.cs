﻿using UnityEngine;

public class Reparent : MonoBehaviour
{
    [SerializeField] private bool reparent;
    [SerializeField] private string parentTarget;
    [SerializeField] private bool isFire;

    // Start is called before the first frame update
    private void Start()
    {
        GameObject parent = null;
        
        if (string.IsNullOrEmpty(parentTarget))
            parent = isFire ? GameObject.FindWithTag("Fireboss") : GameObject.FindWithTag("Iceboss");
        else
            parent = GameObject.Find(parentTarget);
        
        if (parent == null)
            return;

        if (reparent)
        {
            transform.parent = parent.transform;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
        else
        {
            transform.position = parent.transform.position;
            transform.rotation = parent.transform.rotation;
        }
    }
}
