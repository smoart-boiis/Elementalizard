﻿using Character.Abilities;
using UnityEngine;

public class BossAttackCollision : MonoBehaviour
{
    private BossAttack bossAttack;
    private Transform character;
    private LayerMask layermask;

    private void Awake()
    {
        layermask = LayerMask.GetMask("IceWall");
        character = ResourceManager.instance.transform;
        bossAttack = transform.parent.GetComponent<BossAttack>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("MainCharacter"))
            return;
            
        if(!IceWallLogic.iceWallActive)
            bossAttack.characterInside = true;
        else
        {
            bossAttack.characterInside = !CheckLoS();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("MainCharacter") && IceWallLogic.iceWallActive)
            bossAttack.characterInside = !CheckLoS();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MainCharacter"))
            bossAttack.characterInside = false;
    }
        
    /// <summary>
    /// Check if LoS is blocked by icewall
    /// </summary>
    /// <returns></returns>
    private bool CheckLoS()
    {
        float raydistance = Vector3.Distance(transform.root.position, character.position);
        Ray ray = new Ray(transform.root.position, (character.position - transform.root.position).normalized);
        return Physics.Raycast(ray, out RaycastHit hit, raydistance, layermask);
    }
}
