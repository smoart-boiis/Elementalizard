﻿using System;
using System.Collections;
using UnityEngine;

public class BossAttackAnimation : MonoBehaviour
{
    [SerializeField] private string animationbool;
    [SerializeField] private bool isFire;
    [SerializeField] private float attackDealy;

    private Animator bossAnimator;
    
    // Start is called before the first frame update
    private void Start()
    {
        if (string.IsNullOrEmpty(animationbool))
        {
            StartCoroutine(DelayedAttack());
            return;
        }

        GameObject boss = isFire ? GameObject.FindGameObjectWithTag("Fireboss") : GameObject.FindGameObjectWithTag("Iceboss");
        if (boss == null)
            return;

        bossAnimator = boss.GetComponentInChildren<Animator>();
        if (bossAnimator == null)
            return;
        bossAnimator.SetTrigger(animationbool);

        StartCoroutine(DelayedAttack());
    }

    private IEnumerator DelayedAttack()
    {
        yield return new WaitForSeconds(attackDealy);
        if(transform.childCount > 0)
            transform.GetChild(0).gameObject.SetActive(true);
    }
}
