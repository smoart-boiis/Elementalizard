﻿using System.Collections;
using UnityEngine;

public class StaticAttack : BossAttack
{ 
    private void Start()
    {
        StartCoroutine(Attack());
    }

    /// <summary>
    /// Simple Cast-Check-Attack Coruoutine
    /// </summary>
    private IEnumerator Attack()
    {
        yield return new WaitForSeconds(castTime); // Cast the attack
        yield return new WaitForFixedUpdate(); // Wait for the latest physics update

        if(characterInside)
        {
            // if character still within the attacks hitbox, deal damage
            ResourceManager.instance.TakeDamage(damage, isFire);
        }

        MeshRenderer meshRenderer = GetComponentInChildren<MeshRenderer>();
        if (meshRenderer != null)
            meshRenderer.enabled = false;
    }
}