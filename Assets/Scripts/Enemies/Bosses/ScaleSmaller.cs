﻿using UnityEngine;

public class ScaleSmaller : MonoBehaviour
{
    [SerializeField] private float tweenTime = 2f;
    
    // Start is called before the first frame update
    private void Start()
    {
        // Add random force (Ice shard explosions)
        
        LeanTween.scale(gameObject, Vector3.zero, tweenTime).setOnComplete(DestroyAfterTween);
    }

    private void DestroyAfterTween()
    {
        Destroy(gameObject);
    }
}
