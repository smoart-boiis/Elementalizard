﻿using System.Collections;
using System.Collections.Generic;
using Enemies;
using UnityEngine;
using Hellmade.Sound;

public class IcebossBehaviour : MonoBehaviour
{
    [System.Serializable]
    public struct BossAttack
    {
        public GameObject attackObject;
        public float attackDuration;

        private BossAttack(GameObject attack, float duration)
        {
            attackObject = attack;
            attackDuration = duration;
        }
    }
    
    [SerializeField] private List<BossAttack> coreAttacks = new List<BossAttack>();
    [SerializeField] private List<BossAttack> dodgeAttacks = new List<BossAttack>();
    [SerializeField] private float dodgeInterval;
    
    private EnemyDetection enemyDetection;
    private EnemyStats enemyStats;
    private float timeGoneBy;
    private bool onCooldownCore;
    private bool onCooldownDodge;
    private bool combatStartDelay;
    private bool isInDelay;
    [SerializeField] private float startDelay = 1f;
    [SerializeField] private AudioClip battleMusic;
    private bool activateOnce;

    // Start is called before the first frame update
    private void Start()
    {
        enemyDetection = GetComponent<EnemyDetection>();
        enemyStats = GetComponent<EnemyStats>();
    }

    // Update is called once per frame
    private void Update()
    { 
        if (!enemyDetection.aggro || ResourceManager.instance.isDead)
        {
            //Debug.Log("Add idle Boss animation/behaviour here.");
            activateOnce = false;
            if(!enemyStats.CheckFullHealth())
                enemyStats.ModifyHealth(1, false);
            timeGoneBy = 0;
            return;
        }
        
        if(!activateOnce)
        {
            EazySoundManager.PlayMusic(battleMusic, EazySoundManager.GlobalMusicVolume, true, false, 0, 0);
            activateOnce = true;
        }
        
        if (combatStartDelay != true)
        {
            StartCoroutine(DelayCombat());
            combatStartDelay = true;
        }
        
        if (onCooldownCore)
            return;
        
        // Is in combat
        timeGoneBy += Time.deltaTime;

        if (onCooldownDodge)
            return;
        
        if (timeGoneBy >= dodgeInterval)
        {
            // Time for core Attack that allows the player to damage the Boss if finishing the attack correctly
            // Core Attack spawned is dependant of the currents Boss Phase (boss HP)
            int coreAttack = coreAttacks.Count - enemyStats.CheckHealth();
            if (coreAttack < 0 || coreAttack >= coreAttacks.Count || !ResourceManager.instance.CheckAlive()) 
                return;
            Instantiate(coreAttacks[coreAttack].attackObject);
            StartCoroutine(Cooldown(coreAttacks[coreAttack].attackDuration, true));
            timeGoneBy = 0;
        }
        else
        {
            if (isInDelay)
                return;
            
            // Spam random Attacks while it is not time for core attack yet
            int randomAttack = Random.Range(0, dodgeAttacks.Count);
            if (randomAttack < 0 || randomAttack >= dodgeAttacks.Count || enemyStats.CheckHealth() <= 0)
                return;
            Instantiate(dodgeAttacks[randomAttack].attackObject);
            StartCoroutine(Cooldown(dodgeAttacks[randomAttack].attackDuration, false));
        }
    }

    private IEnumerator Cooldown(float cd, bool coreAttack)
    {
        if (coreAttack)
            onCooldownCore = true;
        else
            onCooldownDodge = true;
        
        yield return new WaitForSeconds(cd);
        
        if (coreAttack)
            onCooldownCore = false;
        else
            onCooldownDodge = false;
    }
    
    private IEnumerator DelayCombat()
    {
        isInDelay = true;
        yield return new WaitForSeconds(startDelay);
        isInDelay = false;
    }
}
