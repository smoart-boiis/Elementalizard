﻿using UnityEngine;

public class MoveDown : MonoBehaviour
{
    [SerializeField] private GameObject target;
    [SerializeField] private float tweenTime = 0.66f;
    [SerializeField] private Transform attackParent;
    [SerializeField] private GameObject mist;
    [SerializeField] private GameObject indicator;

    private void Start()
    {
        LeanTween.move(gameObject, target.transform.position, tweenTime).setOnComplete(Collision);
    }

    private void Collision()
    {
        mist.SetActive(true);
        indicator.SetActive(false);
        attackParent.GetChild(0).gameObject.SetActive(false);
        attackParent.GetChild(1).gameObject.SetActive(true);
    }
}
