﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class StopMovingIceBoss : MonoBehaviour
{
    private float startSpeed;
    private NavMeshAgent agent;
    [SerializeField] private float startMovingTime;
    
    // Start is called before the first frame update
    private void Start()
    {
        agent = GameObject.FindGameObjectWithTag("Iceboss").GetComponent<NavMeshAgent>();
        if (agent == null)
            return;

        startSpeed = agent.speed;
        agent.speed = 0;

        if (startMovingTime != 0)
            StartCoroutine(StartMoving());
    }

    private IEnumerator StartMoving()
    {
        yield return new WaitForSeconds(startMovingTime);
        if(agent != null)
            agent.speed = startSpeed;
    }

    private void OnDestroy()
    {
        if(startMovingTime == 0)
            agent.speed = startSpeed;
    }
}
