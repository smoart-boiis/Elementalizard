﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Hellmade.Sound;

public class BossDeathControls : MonoBehaviour
{
    [Serializable]
    public struct ZoneColors
    {
        public GameObject zone;
        [ColorUsage(true, true)] public Color startcolor;
        [ColorUsage(true, true)] public Color endcolor;

        public ZoneColors(GameObject zoneNew, Color startcolorNew, Color endcolorNew)
        {
            zone = zoneNew;
            startcolor = startcolorNew;
            endcolor = endcolorNew;
        }
    }
    
    public static BossDeathControls instance;

    [SerializeField] private GameObject coldTempZone;
    [SerializeField] private GameObject iceTempZone;
    [SerializeField] private GameObject hotTempZone;
    [SerializeField] private GameObject volcanoTempZone;
    [SerializeField] private ZoneColors coldZone;
    [SerializeField] private ZoneColors iceZone;
    [SerializeField] private ZoneColors hotZone;
    [SerializeField] private ZoneColors volcanoZone;
    [SerializeField] private Material cliffs;
    [SerializeField] [ColorUsage(true, true)]
    private Color cliffStartColor;
    
    [SerializeField] private Transform lavapole;
    [SerializeField] private Vector3 targetLava;
    private Vector3 startLava;
    [SerializeField] private Transform icepole;
    [SerializeField] private Vector3 targetIce;
    private Vector3 startIce;

    [SerializeField] private CanvasGroup fadeblack;

    private bool iceBossDead;
    private bool fireBossDead;
    private Material lerpMaterial01;
    private Material lerpMaterial02;
    private Material lerpMaterial03;
    private Material lerpMaterial04;
    private ZoneColors lerpZone01;
    private ZoneColors lerpZone02;
    private ZoneColors lerpZone03;
    private ZoneColors lerpZone04;

    private void Awake()
    {
        instance = this;
        startLava = lavapole.position;
        startIce = icepole.position;
    }

    public void BossDied(bool isFire, float tweenTime, bool died)
    {
        if (isFire)
        {
            fireBossDead = died;    
            hotTempZone.SetActive(!died);
            volcanoTempZone.SetActive(!died);
            lerpZone01 = hotZone;
            lerpZone02 = volcanoZone;
            lerpMaterial01 = lerpZone01.zone.GetComponent<MeshRenderer>().material;
            lerpMaterial02 = lerpZone02.zone.GetComponent<MeshRenderer>().material;
            LeanTween.value(gameObject, LerpCliffColor, Convert.ToInt32(!died), Convert.ToInt32(died), tweenTime);
            LeanTween.value(lerpZone01.zone, LerpMaterial01, Convert.ToInt32(!died), Convert.ToInt32(died), tweenTime);
            LeanTween.value(lerpZone02.zone, LerpMaterial02, Convert.ToInt32(!died), Convert.ToInt32(died), tweenTime);
            Vector3 target = died ? targetLava : startLava;
            LeanTween.move(lavapole.gameObject, target, tweenTime);
            LavaLerper.instance.LerpLava(died, tweenTime);
        }       
        else
        {
            iceBossDead = died;
            coldTempZone.SetActive(!died);
            iceTempZone.SetActive(!died);
            lerpZone03 = iceZone;
            lerpZone04 = coldZone;
            lerpMaterial03 = lerpZone03.zone.GetComponent<MeshRenderer>().material;
            lerpMaterial04 = lerpZone04.zone.GetComponent<MeshRenderer>().material;
            LeanTween.value(lerpZone03.zone, LerpMaterial03, Convert.ToInt32(!died), Convert.ToInt32(died), tweenTime);
            LeanTween.value(lerpZone04.zone, LerpMaterial04, Convert.ToInt32(!died), Convert.ToInt32(died), tweenTime);
            Vector3 target = died ? targetIce : startIce;
            LeanTween.move(icepole.gameObject, target, tweenTime);
        }

        if (fireBossDead && iceBossDead && tweenTime > 0)
            StartCoroutine(StartCredits(tweenTime));
    }

    private void LerpMaterial01(float val)
    {
        lerpMaterial01.SetColor("_Tint", Color.Lerp(lerpZone01.startcolor, lerpZone01.endcolor, val));
    }
    
    private void LerpMaterial02(float val)
    {
        lerpMaterial02.SetColor("_Tint", Color.Lerp(lerpZone02.startcolor, lerpZone02.endcolor, val));
    }
    
    private void LerpMaterial03(float val)
    {
        lerpMaterial03.SetColor("_Tint", Color.Lerp(lerpZone03.startcolor, lerpZone03.endcolor, val));
    }
    
    private void LerpMaterial04(float val)
    {
        lerpMaterial04.SetColor("_Tint", Color.Lerp(lerpZone04.startcolor, lerpZone04.endcolor, val));
    }

    private void LerpCliffColor(float val)
    {
        cliffs.SetColor("_Tint", Color.Lerp(cliffStartColor, volcanoZone.endcolor, val));
    }

    private IEnumerator StartCredits(float tweenTime)
    {
        yield return new WaitForSeconds(tweenTime);
        // Fadeblack
        fadeblack.gameObject.SetActive(true);
        LeanTween.alphaCanvas(fadeblack, 1, 2.5f).setOnComplete(LoadCreditScene);
    }

    private static void LoadCreditScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void OnDestroy()
    {
        cliffs.SetColor("_Tint", cliffStartColor);
    }
}
