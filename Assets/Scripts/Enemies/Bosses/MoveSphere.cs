﻿using UnityEngine;

public class MoveSphere : MonoBehaviour
{
    [SerializeField] private float moveDistance = 10;
    [SerializeField] private float tweenTime = 3f;
    
    // Start is called before the first frame update
    private void Start()
    {
        LeanTween.move(gameObject, transform.position + moveDistance * transform.forward, tweenTime).setOnComplete(DelayedDestroy);
    }

    private void DelayedDestroy()
    {
        Destroy(gameObject);
    }
}
