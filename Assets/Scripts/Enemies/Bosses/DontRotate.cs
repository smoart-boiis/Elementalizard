﻿using System.Collections;
using Enemies;
using UnityEngine;

public class DontRotate : MonoBehaviour
{
    [SerializeField] private bool isFire;
    [SerializeField] private float startRotateTime;
    private EnemyDetection enemyDetection;
    
    // Start is called before the first frame update
    private void Start()
    {
        enemyDetection = isFire
            ? GameObject.FindGameObjectWithTag("Fireboss").GetComponent<EnemyDetection>()
            : GameObject.FindGameObjectWithTag("Iceboss").GetComponent<EnemyDetection>();

        if (enemyDetection != null)
            enemyDetection.dontRotate = true;

        if (startRotateTime != 0)
            StartCoroutine(DelayedRotate());
    }

    private IEnumerator DelayedRotate()
    {
        yield return new WaitForSeconds(startRotateTime);
        
        if (enemyDetection != null)
            enemyDetection.dontRotate = false;
    }

    private void OnDestroy()
    {
        if (enemyDetection != null)
            enemyDetection.dontRotate = false;
    }
}
