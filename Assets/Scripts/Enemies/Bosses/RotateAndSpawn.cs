﻿using System.Collections;
using UnityEngine;

public class RotateAndSpawn : MonoBehaviour
{
    [SerializeField] private float rotTime;
    [SerializeField] private int sphereCount;
    [SerializeField] private GameObject spherePrefab;
    
    // Start is called before the first frame update
    private void Start()
    {
        sphereCount++;
        LeanTween.rotateAround(gameObject, Vector3.up, 360f, rotTime);
        StartCoroutine(SpawnSphere());
    }

    private IEnumerator SpawnSphere()
    {
        float spawnOffset = rotTime / sphereCount;
        Instantiate(spherePrefab, transform.position, transform.rotation);
        for(int i = 1; i < sphereCount; i++)
        {
            yield return new WaitForSeconds(spawnOffset);
            Instantiate(spherePrefab, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }
}
