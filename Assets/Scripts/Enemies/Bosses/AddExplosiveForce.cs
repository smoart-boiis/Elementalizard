﻿using UnityEngine;

public class AddExplosiveForce : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Rigidbody>().AddExplosionForce(75000, transform.position, 1);
    }
}
