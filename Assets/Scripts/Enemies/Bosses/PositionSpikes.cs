﻿using UnityEngine;

public class PositionSpikes : MonoBehaviour
{
    [SerializeField] private float maxDeviation = 25f;
    
    // Start is called before the first frame update
    private void Start()
    {
        foreach (Transform t in transform)
        {
            float newX = Random.Range(-maxDeviation, maxDeviation);
            float newZ = Random.Range(-maxDeviation, maxDeviation);
            Vector3 currentPos = t.position;
            currentPos.x += newX;
            currentPos.y = transform.position.y;
            currentPos.z += newZ;
            t.position = currentPos;
        }
    }
}
