﻿using System;
using System.Collections;
using Enemies.EnemyAttacks;
using SaveSystem;
using UnityEngine;
using UnityEngine.AI;
using Hellmade.Sound;

namespace Enemies
{
    /// <summary>
    /// Script defining the stats (mainly Health) of an enemy and managing it, together with taking damage, flinching and dying.
    /// </summary>
    public class EnemyStats : MonoBehaviour
    {
        public EnemyType enemyType;
        public bool isBoss;
        public string enemyId;
        
        /// <summary>
        /// Element of the enemy
        /// </summary>
        public bool isFire;

        public float speed;
        public float slowSpeed;
        
        [SerializeField] private Animator animator;
        [SerializeField] private AnimationClip deathanim;
        private static readonly int Death = Animator.StringToHash("Death");
        [SerializeField] private AnimationClip flinchanim;
        private static readonly int Flinch1 = Animator.StringToHash("Flinch");
        [SerializeField] private GameObject healVfx;
        [SerializeField] private AudioClip healSfx;
        [SerializeField] private AudioClip hitSfx;
        [SerializeField] private AudioClip deathSfx;


        /// <summary>
        /// Inital health and usually also max health of the enemy
        /// </summary>
        [SerializeField] private int health = 50;
        
        private NavMeshAgent agent;
        private EnemyAttackManager enemyAttackManager;
        
        private int maxhealth;
        private bool onlyOneDeathAnim;
        private bool onlyOneFlinch;

        private void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
            if(agent != null)
                agent.speed = speed;
            maxhealth = health;
 
            enemyAttackManager = GetComponent<EnemyAttackManager>();
        }

        /// <summary>
        /// Modify Health of the enemy, usually called by the projectile that his the enemy or when regenrating out of combat
        /// </summary>
        /// <param name="value"></param>
        /// <param name="attackIsFire"></param>
        public void ModifyHealth(int value, bool attackIsFire)
        {
            if (!isBoss)
            {
                if (attackIsFire == isFire) // If same element heal, else take damage
                {
                    health += value;
                    if (healVfx != null)
                    {
                        GameObject heal = Instantiate(healVfx);
                        heal.transform.parent = transform;
                        heal.transform.localPosition = Vector3.zero;
                        heal.transform.localRotation = Quaternion.identity;
                    }

                    EazySoundManager.PlaySound(healSfx, EazySoundManager.GlobalSoundsVolume, false, transform);
                }     
                else
                {
                    health -= value;
                    EazySoundManager.PlaySound(hitSfx, EazySoundManager.GlobalSoundsVolume, false, transform);
                    StartCoroutine(Flinch());
                }
            }

            if (health > maxhealth) // Dont overheal & also die below 0
                health = maxhealth;
            else if (health < 1 && !onlyOneDeathAnim)
            {
                StartCoroutine(DeathAnimation());    
            }
        }

        public void ModifyBossHealth()
        {
            health -= 1;
            if (health > maxhealth) // Dont overheal & also die below 0
                health = maxhealth;
            else if (health < 1 && !onlyOneDeathAnim)
                StartCoroutine(DeathAnimation());
        }

        /// <summary>
        /// Check if enemy is full health (Used for selfregeneration when out of combat)
        /// </summary>
        /// <returns></returns>
        public bool CheckFullHealth()
        {
            return health >= maxhealth;
        }

        public int CheckHealth()
        {
            return health;
        }

        /// <summary>
        /// Display flinch animation if enemy is hit & not animation-locked
        /// Currently only available for non-bosses
        /// </summary>
        /// <returns></returns>
        private IEnumerator Flinch()
        {
            if (onlyOneFlinch || enemyAttackManager.isAttacking) 
                yield break;
            
            onlyOneFlinch = true;
            agent.isStopped = true;
            agent.angularSpeed = 0f;

            animator.SetBool(Flinch1, true);
            yield return new WaitForSeconds(flinchanim.length);
            animator.SetBool(Flinch1, false);

            agent.isStopped = false;
            agent.angularSpeed = enemyAttackManager.angularSpeed;
            onlyOneFlinch = false;

        }

        /// <summary>
        /// Trigger death animation and VFX, when enemy is at 0 or less health
        /// </summary>
        /// <returns></returns>
        private IEnumerator DeathAnimation()
        {
            onlyOneDeathAnim = true;

            if (isBoss)
            {
                BossDeathControls.instance.BossDied(isFire, 2.5f, true);
                EazySoundManager.PlayMusic(deathSfx, EazySoundManager.GlobalMusicVolume, false, false, 0.5f, 0);
                GameObject.Find("ForestZone").GetComponent<ForestMusic>().PlayForestMusic();
            }
            else
                EazySoundManager.PlaySound(deathSfx, EazySoundManager.GlobalSoundsVolume, false, transform);
            
            if (agent != null)
            {
                agent.isStopped = true;
                agent.angularSpeed = 0f;
            }

            if (animator != null)
            {
                if(isBoss)
                    animator.SetTrigger(Death);
                else
                    animator.SetBool(Death, true);
            }
            if(deathanim != null)
                yield return new WaitForSeconds(deathanim.length);

            Debug.Log("Add Dissolve Shader animation or sth similar here :)");

            Destroy(gameObject);
        }

        private void OnDestroy()
        {    
            SaveDataAccessor.instance.MarkDeadEnemy(this);
        }
    }
}
