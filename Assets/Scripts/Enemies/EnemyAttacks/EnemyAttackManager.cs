﻿using System;
using System.Collections;
using System.Collections.Generic;
using Character.Abilities;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Analytics;
using UnityEngine.Serialization;
using CharacterController = Character.CharacterController;

namespace Enemies.EnemyAttacks
{
    /// <summary>
    /// This Script got rather big and messsy.
    /// Should definitly restructure it into different Attack Managers for each enemy type when given the time or at least refactor everything.
    /// </summary>
    public class EnemyAttackManager : MonoBehaviour
    {
        /// <summary>
        /// All attacks the Enemy has availble
        /// </summary>
        [SerializeField] private List<AttackEntry> attacks = new List<AttackEntry>();
        [SerializeField] private Transform attackSpawnpoint;
        /// <summary>
        /// Internal Cooldown between attacks to prevent instant back to back chain
        /// </summary>
        [SerializeField] private float cooldownBetweenAttacks = 0.3f; // 

        [SerializeField] private Transform iceGolemHand;
        
        [HideInInspector] public float angularSpeed;
        [FormerlySerializedAs("is_attacking")] [HideInInspector] public bool isAttacking = false;

        private EnemyDetection enemydetector;
        private NavMeshAgent agent;
        private EnemyStats enemystats;
        private Animator animator;
        private Transform character;
        private LayerMask layermask;
        private readonly GameObject activeattack;
        private GameObject currentAttack;
        
        /// <summary>
        /// Referring to internal attack cooldown, not specific ability cooldowns
        /// </summary>
        private bool onCooldown;
        private float timeBygone;

        public EnemyAttackManager(GameObject activeattack)
        {
            this.activeattack = activeattack;
        }


        public enum TargetMode
        {
            OnEnemy,
            Directional,
            Self
        };

        /// <summary>
        /// Class which defines an Attack entry, stats and availablity for an enemy, a List of this will be used for the mulitple enemy attacks.
        /// Use class instead of struct here as i want to manipulate the on_cooldown value directly, which is not allowed in a struct,
        /// </summary>
        [Serializable]
        private class AttackEntry
        {
            public GameObject attackObject;
            [HideInInspector] public EnemyAttack attackStats;
            [FormerlySerializedAs("on_cooldown")] [HideInInspector] public bool onCooldown;

            public void Populate(GameObject attackObjectList)
            {
                attackObject = attackObjectList;
                attackStats = attackObjectList.GetComponent<EnemyAttack>();
                onCooldown = false;
            }
        }

        private void Awake()
        {
            // Set up the data for each attack with the GameObject given in the Inspector-List
            foreach (AttackEntry attack in attacks)
            {
                attack.Populate(attack.attackObject);
            }
        }

        // Start is called before the first frame update
        private void Start()
        {
            enemydetector = GetComponent<EnemyDetection>();
            agent = GetComponent<NavMeshAgent>();
            animator = GetComponentInChildren<Animator>();
            angularSpeed = agent.angularSpeed;
            enemystats = GetComponent<EnemyStats>();
            character = FindObjectOfType<CharacterController>().transform;
            layermask = LayerMask.GetMask("IceWall");
        }

        // Using lateUpdate here so we can overwrite the movemnt actions from enemydetection in case we want to execute an attack
        private void LateUpdate()
        {
            if (enemystats.CheckHealth() <= 0)
            {
                if(currentAttack != null)
                    Destroy(currentAttack);
                
                return;
            }            
            if (!isAttacking && enemydetector.aggro && !onCooldown)
            {
                if (IceWallLogic.iceWallActive && CheckLoS())
                {
                    return;
                }
                GetPriorityAttack();
            }
            else if (isAttacking)
            {
                agent.isStopped = true; // Forbid movement during attack
            }

            if (isAttacking || enemydetector.aggro) 
                return;
            
            // If not in combat periodically heal
            timeBygone += Time.deltaTime; // Tracking internal attack CD
            if (!(timeBygone >= 3f))
                return;
            timeBygone = 0;
            if(!enemystats.CheckFullHealth())
            {
                enemystats.ModifyHealth(3, enemystats.isFire);
            }
        }



        /// <summary>
        /// Checks if LoS to Main char is blocked by IceWall
        /// </summary>
        /// <returns></returns>
        private bool CheckLoS()
        {
            float raydistance = Vector3.Distance(transform.position, character.position);
            Ray ray = new Ray(transform.position, (character.position - transform.position).normalized);
            return Physics.Raycast(ray, out RaycastHit hit, raydistance, layermask);
        }
        

        /// <summary>
        /// Loops through all possible attacks, checks if available and selects oen with the highest priority
        /// </summary>
        private void GetPriorityAttack()
        {
            int priority = 0;
            int attackentry = 999;
            // Iterate through attack-list and find the attack that is available with the highest priority
            for (int i = 0; i < attacks.Count; i++)
            {
                if (!attacks[i].onCooldown && attacks[i].attackStats.range >= enemydetector.distance && attacks[i].attackStats.priority > priority)
                {
                    priority = attacks[i].attackStats.priority;
                    attackentry = i;
                }
            }

            if (priority != 0)
            {
                StartCoroutine(Attack(attackentry));
            }
        }

        /// <summary>
        /// Attack coroutine that will cast and exectue the currently select attack
        /// </summary>
        /// <param name="attackentry"></param>
        /// <returns></returns>
        private IEnumerator Attack(int attackentry)
        {
            timeBygone = 0f;
            agent.isStopped = true;
            agent.angularSpeed = 0; // Forbid rotating during casting
            isAttacking = true;
            attacks[attackentry].onCooldown = true;
            currentAttack = null;

            switch (attacks[attackentry].attackStats.targetMode)
            {
                case TargetMode.OnEnemy:
                    currentAttack = Instantiate(attacks[attackentry].attackObject, enemydetector.mainCharacter.position, Quaternion.identity, transform);
                    break;
                case TargetMode.Directional:
                    currentAttack = Instantiate(attacks[attackentry].attackObject, attackSpawnpoint.position, transform.rotation, transform);
                    break;
                case TargetMode.Self:
                    currentAttack = Instantiate(attacks[attackentry].attackObject, attackSpawnpoint.position, Quaternion.identity, transform);
                    break;
            }

            if(attacks[attackentry].attackStats.attackClip != null)
            {
                // Initate channelanimation
                animator.SetBool(attacks[attackentry].attackStats.animationbool, true);
                if (attacks[attackentry].attackStats.vfx_onEnemy != null && !attacks[attackentry].attackStats.isTornado)
                    Instantiate(attacks[attackentry].attackStats.vfx_onEnemy, enemydetector.mainCharacter.position, Quaternion.identity);
                if (attacks[attackentry].attackStats.vfx_onSelf != null && !attacks[attackentry].attackStats.isSpikes)
                    Instantiate(attacks[attackentry].attackStats.vfx_onSelf, transform.position, Quaternion.identity);
                else if(attacks[attackentry].attackStats.vfx_onSelf != null && attacks[attackentry].attackStats.isSpikes)
                {
                    GameObject spiekCharge = Instantiate(attacks[attackentry].attackStats.vfx_onSelf);
                    spiekCharge.transform.parent = iceGolemHand;
                    spiekCharge.transform.localPosition = Vector3.zero;
                }
                yield return new WaitForSeconds(attacks[attackentry].attackStats.castTime - attacks[attackentry].attackStats.attackClip.length);
                // Attack animation 
                animator.SetBool(attacks[attackentry].attackStats.animationbool, false);
                // wait for attack anim to finish
                yield return new WaitForSeconds(attacks[attackentry].attackStats.attackClip.length);
                
                if (attacks[attackentry].attackStats.isTornado && currentAttack != null && attacks[attackentry].attackStats.vfx_onEnemy != null)
                {
                    GameObject tornadoVfx = Instantiate(attacks[attackentry].attackStats.vfx_onEnemy, currentAttack.transform.position, Quaternion.identity);
                    Vector3 targetPos = 2 * (currentAttack.transform.GetChild(0).position - currentAttack.transform.position) + currentAttack.transform.position;
                    tornadoVfx.GetComponent<MoveTornado>().Move(targetPos);
                }
            }
            else
            {
                Debug.Log("Attack is still missing Animation");
                yield return new WaitForSeconds(attacks[attackentry].attackStats.castTime);
            }
            

            // Cast time done allow moving and attacking again
            isAttacking = false;
            agent.angularSpeed = angularSpeed;
            agent.isStopped = false;

            StartCoroutine(AttackCooldown());

            // Wait for the cooldown to make attack available again
            yield return new WaitForSeconds(attacks[attackentry].attackStats.cooldown);
            attacks[attackentry].onCooldown = false;
        }

        /// <summary>
        /// Wait for specific attack CS
        /// </summary>
        /// <returns></returns>
        private IEnumerator AttackCooldown()
        {
            onCooldown = true;
            yield return new WaitForSeconds(cooldownBetweenAttacks);
            onCooldown = false;
        }

        // If enemy dies, also destory possible currently active attack
        private void OnDestroy()
        {
            if (activeattack != null)
                Destroy(activeattack);
        }
    }
    
}
