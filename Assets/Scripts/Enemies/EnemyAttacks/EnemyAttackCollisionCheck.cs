﻿using System;
using Character.Abilities;
using UnityEngine;
using CharacterController = Character.CharacterController;

namespace Enemies.EnemyAttacks
{
    /// <summary>
    /// Collision check of enemy attacks that flags if MainCharacter is within the attacks hitbox or not
    /// </summary>
    public class EnemyAttackCollisionCheck : MonoBehaviour
    {
        private EnemyAttack enemyattack;
        private Transform character;
        private LayerMask layermask;

        private void Awake()
        {
            layermask = LayerMask.GetMask("IceWall");
            enemyattack = transform.parent.GetComponent<EnemyAttack>();
            character = FindObjectOfType<CharacterController>().transform;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("MainCharacter"))
                return;
            
            if(!IceWallLogic.iceWallActive)
                enemyattack.characterInside = true;
            else
            {
                enemyattack.characterInside = !CheckLoS();
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if(other.CompareTag("MainCharacter") && IceWallLogic.iceWallActive)
                enemyattack.characterInside = !CheckLoS();
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("MainCharacter"))
                enemyattack.characterInside = false;
        }
        
        /// <summary>
        /// Check if LoS is blocked by icewall
        /// </summary>
        /// <returns></returns>
        private bool CheckLoS()
        {
            float raydistance = Vector3.Distance(transform.root.position, character.position);
            Ray ray = new Ray(transform.root.position, (character.position - transform.root.position).normalized);
            return Physics.Raycast(ray, out RaycastHit hit, raydistance, layermask);
        }
    }
}
