﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.Serialization;

namespace Enemies.EnemyAttacks
{
    /// <summary>
    /// Basic Attack class for enemies, defining the attacks stats and behaviour.
    /// Attack will wait for its casttime, then check if Character is hit & if so deal damage.
    /// </summary>
    public class EnemyAttack : MonoBehaviour
    {
        public int priority = 1;
        public float range = 2f;
        public float cooldown = 1.25f;
        public float castTime = 1f;
        public int damage = 20;
        [FormerlySerializedAs("is_fire")] public bool isFire = false;
        public EnemyAttackManager.TargetMode targetMode;
        public string animationbool;
        public AnimationClip attackClip;
        [FormerlySerializedAs("character_inside")] [HideInInspector] public bool characterInside = false;
        public GameObject vfx_onEnemy;
        public GameObject vfx_onSelf;
        public bool isTornado;
        public bool isSpikes;
        

        private Vector3 startPos;


        private void Awake()
        {
            startPos = transform.position;
        }

        private void Start()
        {
            StartCoroutine(Attack());
        }

        private void Update()
        {
            transform.position = startPos; // Prevent sliding with enemy
        }

        /// <summary>
        /// Simple Cast-Check-Attack Coruoutine
        /// </summary>
        /// <returns></returns>
       private IEnumerator Attack()
        {
            yield return new WaitForSeconds(castTime); // Cast the attack
            yield return new WaitForFixedUpdate(); // Wait for the latest physics update

            if(characterInside)
            {
                // if character still within the attacks hitbox, deal damage
                ResourceManager.instance.TakeDamage(damage, isFire);
            }

            Destroy(gameObject); // Destroying attack-object will also destroy the child object that checks for the charactercollision
        }
    }
}
