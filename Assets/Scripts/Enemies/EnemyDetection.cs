﻿using System;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Enemies
{
    public class EnemyDetection : MonoBehaviour
    {
        /// <summary>
        /// Updated distance from the current targetpoint
        /// </summary>
        [HideInInspector] public float distance;
        /// <summary>
        ///  Check if enemy is in combat
        /// </summary>
        [HideInInspector] public bool aggro;
        [HideInInspector] public Transform mainCharacter;
        [HideInInspector] public bool dontRotate;
        
        /// <summary>
        /// Maximum Range the enemy can detect the main chracter
        /// </summary>
        [SerializeField] private float detectionrange = 10f;

        [SerializeField] private float maxRoamRange = 15f;

        private NavMeshAgent agent;
        /// <summary>
        /// Origin Spawnpoint of the enemy - used to randomly roam around the spawnpoint when not in combat
        /// Set this manually instead of on Awake as the loading system would call the awake function again when spawning
        /// a already killed enemy.
        /// </summary>
        public Vector3 origin;
        private Vector3 targetPosition;
        private EnemyStats enemyStats;
        private Animator iceBossAnimator;
        private static readonly int WalkBlend = Animator.StringToHash("WalkBlend");

        private void Awake()
        {
            mainCharacter = GameObject.FindGameObjectWithTag("MainCharacter").transform;
            agent = GetComponent<NavMeshAgent>();
            targetPosition = origin;
            enemyStats = GetComponent<EnemyStats>();

            if (enemyStats.isBoss && !enemyStats.isFire)
                iceBossAnimator = GameObject.FindGameObjectWithTag("Iceboss").GetComponentInChildren<Animator>();
        }

        private void Update()
        {
            distance = Vector3.Distance(mainCharacter.position, transform.position);
            
            if (distance < detectionrange) // If enemy in range move to it
            {
                if (!aggro)
                {
                    aggro = true;
                    SaveConditions.instance.AddToStack();
                }
                if(agent != null)
                    agent.SetDestination(mainCharacter.position);
                FaceTarget();
            }
            else
            {
                // Roam arround randomly in a certain range of the current
                if (aggro)
                {
                    aggro = false;
                    SaveConditions.instance.Remove();
                }
                    
                if (!TargetReached())
                    return;
                GetRandomRoamPosition();
                if(agent != null)
                    agent.SetDestination(targetPosition);
            }

            if (!enemyStats.isBoss || enemyStats.isFire)
                return;
            bool isStopping = agent.remainingDistance <= agent.stoppingDistance;
            if(iceBossAnimator != null)
                iceBossAnimator.SetFloat(WalkBlend, Convert.ToInt32(!isStopping));
        }

        /// <summary>
        /// Rotates character so it faces our main character
        /// </summary>
        private void FaceTarget()
        {
            if (agent != null && agent.angularSpeed == 0) // Dont rotate when casting an attack 
                return;

            if (dontRotate)
                return;
            
            Vector3 direction = (mainCharacter.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 15f);
        }

        /// <summary>
        /// Gets a random roaming position for the enemy when not in combat and last target reached
        /// </summary>
        private void GetRandomRoamPosition()
        {
            if (agent == null)
                return;
            
            // Get random direction within a distance from Monsters Origin point
            Vector3 randomDir = Random.insideUnitSphere.normalized;
            targetPosition = origin + randomDir * Random.Range(5f, maxRoamRange);
            // next sample that random point to find the relevant point on the navmesh instead
            if(NavMesh.SamplePosition(targetPosition, out NavMeshHit hit, maxRoamRange, NavMesh.AllAreas))
                targetPosition = hit.position;
            else
                GetRandomRoamPosition(); // Try a different random position
        }

        /// <summary>
        /// Checks if the target point is reached
        /// </summary>
        /// <returns></returns>
        private bool TargetReached()
        {
            if (agent == null)
                return false;

            if (agent.pathPending || !(agent.remainingDistance <= agent.stoppingDistance))
                return false;

            return !agent.hasPath || agent.velocity.sqrMagnitude == 0f;
        }
        
        /// <summary>
        /// Draws a gizmo in scene view when the object is selected
        /// </summary>
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, detectionrange);
        }

        private void OnDestroy()
        {
            if(aggro)
                SaveConditions.instance.Remove();
        }
    }
}
