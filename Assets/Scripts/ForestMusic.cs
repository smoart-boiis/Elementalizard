﻿using System;
using System.Collections;
using UnityEngine;
using Hellmade.Sound;

public class ForestMusic : MonoBehaviour
{
    public AudioClip audioClip;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("MainCharacter"))
            EazySoundManager.PlayMusic(audioClip, EazySoundManager.GlobalMusicVolume, true, false, 3, 3);
    }

    public void PlayForestMusic()
    {
        StartCoroutine(DelayForestMusic());
    }
    
    private IEnumerator DelayForestMusic()
    {
        yield return new WaitForSeconds(5);
        EazySoundManager.PlayMusic(audioClip, EazySoundManager.GlobalMusicVolume, true, false, 0, 3);
        Debug.Log("Test should be playing now");
    }
}
