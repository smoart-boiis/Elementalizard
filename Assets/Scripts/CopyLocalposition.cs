﻿using UnityEngine;

public class CopyLocalposition : MonoBehaviour
{
    [SerializeField] private RectTransform copyFrom;

    private RectTransform copyTo;

    private void Awake()
    {
        copyTo = GetComponent<RectTransform>();
        copyTo.localPosition = copyFrom.localPosition;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        copyTo.localPosition = copyFrom.localPosition;
    }
}
