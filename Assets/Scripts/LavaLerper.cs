﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaLerper : MonoBehaviour
{
    [Serializable]
    public struct LavaInfo
    {
        public float speedMainX;
        public float speedMainY;
        public Color color;
        public Color color2;
        public Color color3;
        public float speedDistortX;
        public float speedDistortY;
        public Color edgeC;
        public float speed;

        public LavaInfo(float speedMainXnew, float speedMainYnew, Color colorNew, Color color2New, Color color3New, float speedDistortXnew,
            float speedDistortYnew, Color edgeCnew, float speedNew)
        {
            speedMainX = speedMainXnew;
            speedMainY = speedMainYnew;
            color = colorNew;
            color2 = color2New;
            color3 = color3New;
            speedDistortX = speedDistortXnew;
            speedDistortY = speedDistortYnew;
            edgeC = edgeCnew;
            speed = speedNew;
        }
    }
    
    public static LavaLerper instance;
    
    [SerializeField] private GameObject lavaCollider;
    /// <summary>
    /// 1 for start, 0 for target 
    /// </summary>
    [SerializeField] private List<LavaInfo> lavaInfos = new List<LavaInfo>();

    private float tweentime;

    private Material lavaMaterial;
    private static readonly int Speed = Shader.PropertyToID("_Speed");
    private static readonly int EdgeC = Shader.PropertyToID("_EdgeC");
    private static readonly int SpeedDistortY = Shader.PropertyToID("_SpeedDistortY");
    private static readonly int SpeedDistortX = Shader.PropertyToID("_SpeedDistortX");
    private static readonly int Color3 = Shader.PropertyToID("_Color3");
    private static readonly int Color2 = Shader.PropertyToID("_Color2");
    private static readonly int Color1 = Shader.PropertyToID("_Color");
    private static readonly int SpeedMainY = Shader.PropertyToID("_SpeedMainY");
    private static readonly int SpeedMainX = Shader.PropertyToID("_SpeedMainX");

    private void Awake()
    {
        instance = this;
        lavaMaterial = GetComponent<MeshRenderer>().material;
        lavaInfos.Add(new LavaInfo(
            lavaMaterial.GetFloat(SpeedMainX),
            lavaMaterial.GetFloat(SpeedMainY),
            lavaMaterial.GetColor(Color1),
            lavaMaterial.GetColor(Color2),
            lavaMaterial.GetColor(Color3),
            lavaMaterial.GetFloat(SpeedDistortX),
            lavaMaterial.GetFloat(SpeedDistortY),
            lavaMaterial.GetColor(EdgeC),
            lavaMaterial.GetFloat(Speed)
            ));
    }

    public void LerpLava(bool fireDead, float tweenTime)
    {
        lavaCollider.SetActive(!fireDead);
        GetComponent<MeshCollider>().enabled = fireDead;
        GetComponent<WaterBob>().enabled = !fireDead;
        if (fireDead)
            LeanTween.value(gameObject, LerpValue, 1, 0, tweenTime);
        else
            LeanTween.value(gameObject, LerpValue, 0, 1, tweenTime);

        int index = fireDead ? 0 : 1;
        tweentime = tweenTime;
        StartCoroutine(StopMoving(index));
    }

    private IEnumerator StopMoving(int index)
    {
        yield return new WaitForSeconds(tweentime/1.5f);
        lavaMaterial.SetFloat(SpeedMainX, lavaInfos[index].speedMainX);
        lavaMaterial.SetFloat(SpeedMainY, lavaInfos[index].speedMainY);
        lavaMaterial.SetFloat(SpeedDistortX, lavaInfos[index].speedDistortX);
        lavaMaterial.SetFloat(SpeedDistortY, lavaInfos[index].speedDistortY);
        lavaMaterial.SetFloat(Speed, lavaInfos[index].speed);
    }

    private void LerpValue(float val)
    {
        //lavaMaterial.SetFloat(SpeedMainX, Mathf.Lerp(lavaInfos[0].speedMainX, lavaInfos[1].speedMainX, val));
        //lavaMaterial.SetFloat(SpeedMainY, Mathf.Lerp(lavaInfos[0].speedMainY, lavaInfos[1].speedMainY, val));
        lavaMaterial.SetColor(Color1, Color.Lerp(lavaInfos[0].color, lavaInfos[1].color, val));
        lavaMaterial.SetColor(Color2, Color.Lerp(lavaInfos[0].color2, lavaInfos[1].color2, val));
        lavaMaterial.SetColor(Color3, Color.Lerp(lavaInfos[0].color3, lavaInfos[1].color3, val));
        //lavaMaterial.SetFloat(SpeedDistortX, Mathf.Lerp(lavaInfos[0].speedDistortX, lavaInfos[1].speedDistortX, val));
        //lavaMaterial.SetFloat(SpeedDistortY, Mathf.Lerp(lavaInfos[0].speedDistortY, lavaInfos[1].speedDistortY, val));
        lavaMaterial.SetColor(EdgeC, Color.Lerp(lavaInfos[0].edgeC, lavaInfos[1].edgeC, val));
        //lavaMaterial.SetFloat(Speed, Mathf.Lerp(lavaInfos[0].speed, lavaInfos[1].speed, val));
    }
}
