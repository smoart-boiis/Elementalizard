﻿using Hellmade.Sound;
using UnityEngine;

public class GlobalSoundOptions : MonoBehaviour
{
    private void Awake()
    {
        EazySoundManager.GlobalVolume = 0.4f;
        EazySoundManager.GlobalSoundsVolume = 1f;
        EazySoundManager.GlobalMusicVolume = 0.15f;
        EazySoundManager.GlobalUISoundsVolume = 0.75f;
    }
}
