﻿using UnityEngine;

public class DisableEnable : MonoBehaviour
{
    [SerializeField] private GameObject menu;
    
    public void MenuOnOff()
    {
        menu.SetActive(!menu.activeInHierarchy);
    }
}
