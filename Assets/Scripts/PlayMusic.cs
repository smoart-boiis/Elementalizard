﻿using Hellmade.Sound;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    [SerializeField] private AudioClip audioClip;

    private void Awake()
    {
        EazySoundManager.GlobalVolume = 0.05f;
        EazySoundManager.PlayMusic(audioClip, EazySoundManager.GlobalMusicVolume, true, false, 1, 1);
    }
}
