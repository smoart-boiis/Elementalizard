﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMover : MonoBehaviour
{
    [HideInInspector] public bool moveToStart;
    [HideInInspector] public float currentTime;
    
    /// <summary>
    /// Animation Curve that manipulates the lerp for a non linear lerp :)
    /// </summary>
    [SerializeField] private AnimationCurve animationCurve;

    [SerializeField] private GameObject platformToMove;
    [SerializeField] private Transform startPos;
    [SerializeField] private Transform endPos;
    [SerializeField] private float lerptime;
    
    private Coroutine coroutine;

    // Start is called before the first frame update
    private void Start()
    {
       coroutine = StartCoroutine(MoveEnd());
    }

    /// <summary>
    /// Used to set Platform to proper position when load save
    /// </summary>
    /// <param name="movingToStart"></param>
    /// <param name="currentPercent"></param>
    public void LoadIn(bool movingToStart, float currentLerpTime)
    {
        StopCoroutine(coroutine);
        currentTime = currentLerpTime;
        coroutine = StartCoroutine(movingToStart ? MoveStart() : MoveEnd());
    }

    /// <summary>
    /// Lerp platform to end position
    /// </summary>
    /// <returns></returns>
    private IEnumerator MoveEnd()
    {
        moveToStart = false;
        
        float percent = 0;

        while (percent < 1)
        {
            currentTime += Time.deltaTime;
            percent = Mathf.Clamp01(currentTime / lerptime);

            platformToMove.transform.position = Vector3.Lerp(startPos.position, endPos.position, animationCurve.Evaluate(percent));
            
            yield return new WaitForFixedUpdate();
        }
        
        currentTime = 0;
        coroutine = StartCoroutine(MoveStart());
    }

    /// <summary>
    /// Lerp platform to startPosition
    /// </summary>
    /// <returns></returns>
    private IEnumerator MoveStart()
    {
        moveToStart = true;
        
        float percent = 0;

        while (percent < 1)
        {
            currentTime += Time.deltaTime;
            percent = Mathf.Clamp01(currentTime / lerptime);

            platformToMove.transform.position = Vector3.Lerp(endPos.position, startPos.position, animationCurve.Evaluate(percent));
            
            yield return new WaitForFixedUpdate();
        }

        currentTime = 0;
        coroutine = StartCoroutine(MoveEnd());
    }
}
