﻿using System.Collections;
using Hellmade.Sound;
using UnityEngine;

public class TemperatureZone : MonoBehaviour
{
    [SerializeField] private int temperatureEffect;
    [SerializeField] private float tickTime = 1;
    [SerializeField] private AudioClip audioClip;
    private Coroutine modifyTemp;
    private float enterSoundCd = 3f;
    private bool onCd;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("MainCharacter"))
            return;

        if(audioClip != null)
            EazySoundManager.PlayMusic(audioClip, EazySoundManager.GlobalMusicVolume, true, false, 3, 3);
        modifyTemp = StartCoroutine(AffectTemperature());
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.CompareTag("MainCharacter"))
            return;

        StopCoroutine(modifyTemp);
    }

    private IEnumerator AffectTemperature()
    {
        yield return new WaitForSeconds(tickTime);
        ResourceManager.instance.UseResource(temperatureEffect);
        modifyTemp = StartCoroutine(AffectTemperature());
    }

}
