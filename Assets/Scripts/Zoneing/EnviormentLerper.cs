﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/// <summary>
/// This script adjusts the atomsphere of the enviornment depending on the distance to given Poles
/// In order to modify the worlds atmosphere it will change:
/// - Directional Light Rotation and Strength
/// - Skyshader
/// - PostProcessing Stack
/// </summary>
public class EnviormentLerper : MonoBehaviour
{
    public enum Enviornment{Forest, Volcano, Icepole}

    [Header("Default")]
    [SerializeField] private Light mainLight;

    [SerializeField] private Material skyboxMaterial;
    [HideInInspector] public Enviornment currentEnviornment = Enviornment.Forest;
    private Transform character;
    
    [Space(10)]
    [Header("Lava")]
    [SerializeField] private Transform lavaPole;
    [SerializeField] private float lavaMaxDistance;
    
    [Space(10)]
    [Header("Ice")]
    [SerializeField] private Transform icePole;
    [SerializeField] private float iceMaxDistance;

    [Space(10)] [Header("Values - 0Default; 1Fire; 2Ice")]
    [SerializeField] private int[] colorAdjustContrast = new int[3];
    [SerializeField] private int[] colorAdjustHue = new int[3];
    [SerializeField] private int[] colorAdjustSat = new int[3];
    [SerializeField] private float[] bloom = new float[3];
    [SerializeField] private float[] chromAbberation = new float[3];
    [SerializeField] private int[] channelmixer = new int[3];
    [SerializeField] private int[] whiteBalanceTemp = new int[3];
    [SerializeField] private int[] whiteBalanceTint = new int[3];
    [SerializeField] private float[] lensDistortion = new float[3];
    
    [SerializeField] private Color[] lightColor = new Color[3];
    [SerializeField] private float[] lightIntensity = new float[3];
    [SerializeField] private Vector3[] lightRotation = new Vector3[3];
    
    [SerializeField] private Color[] moonColor = new Color[3];
    [SerializeField] private Color[] starSkyColor = new Color[3];
    [SerializeField] private Color[] sunsetColor = new Color[3];
    [SerializeField] private Color[] daySkyColorTop = new Color[3];
    [SerializeField] private Color[] daySkyColorBot = new Color[3];
    [SerializeField] private Color[] nightSkyColorTop = new Color[3];
    [SerializeField] private Color[] nightSkyColorBot = new Color[3];
    [SerializeField] private Color[] cloudEdgeNight = new Color[3];
    [SerializeField] private float[] sunRadius = new float[3];
    [SerializeField] private float[] horizonOffset = new float[3];
    [SerializeField] private float[] horizonIntensity = new float[3];
    [SerializeField] private float[] distortNoiseScale = new float[3];
    [SerializeField] private float[] extraDistortion = new float[3];
    [SerializeField] private float[] moveSpeed = new float[3];
    [SerializeField] private float[] cloudFuzziness = new float[3];
    [SerializeField] private float[] cloudBrightness = new float[3];

    private ColorAdjustments colorAdjustments;
    private Bloom bloomPpv;
    private ChromaticAberration chromaticAberration;
    private ChannelMixer channelMixer;
    private WhiteBalance whiteBalance;
    private LensDistortion lensDistortionPpv;
    
    private static readonly int OffsetHorizon = Shader.PropertyToID("_OffsetHorizon");
    private static readonly int SunRadius = Shader.PropertyToID("_SunRadius");
    private static readonly int HorizonIntensity = Shader.PropertyToID("_HorizonIntensity");
    private static readonly int DistortScale = Shader.PropertyToID("_DistortScale");
    private static readonly int Distortion = Shader.PropertyToID("_Distortion");
    private static readonly int Speed = Shader.PropertyToID("_Speed");
    private static readonly int Brightness = Shader.PropertyToID("_Brightness");
    private static readonly int Fuzziness = Shader.PropertyToID("_Fuzziness");
    private static readonly int MoonColor = Shader.PropertyToID("_MoonColor");
    private static readonly int StarsSkyColor = Shader.PropertyToID("_StarsSkyColor");
    private static readonly int SunSet = Shader.PropertyToID("_SunSet");
    private static readonly int DayTopColor = Shader.PropertyToID("_DayTopColor");
    private static readonly int DayBottomColor = Shader.PropertyToID("_DayBottomColor");
    private static readonly int NightTopColor = Shader.PropertyToID("_NightTopColor");
    private static readonly int NightBottomColor = Shader.PropertyToID("_NightBottomColor");
    private static readonly int CloudColorNightEdge = Shader.PropertyToID("_CloudColorNightEdge");

    private void Awake()
    {
        character = GameObject.FindGameObjectWithTag("MainCharacter").transform;
        
        // Get access to Post Processing Settings
        VolumeProfile volumeProfile = FindObjectOfType<Volume>().profile;
        if (!volumeProfile.TryGet(out colorAdjustments)) throw new System.NullReferenceException(nameof(colorAdjustments));
        if (!volumeProfile.TryGet(out bloomPpv)) throw new System.NullReferenceException(nameof(bloomPpv));
        if (!volumeProfile.TryGet(out chromaticAberration)) throw new System.NullReferenceException(nameof(chromaticAberration));
        if (!volumeProfile.TryGet(out channelMixer)) throw new System.NullReferenceException(nameof(channelMixer));
        if (!volumeProfile.TryGet(out whiteBalance)) throw new System.NullReferenceException(nameof(whiteBalance));
        if (!volumeProfile.TryGet(out lensDistortionPpv)) throw new System.NullReferenceException(nameof(lensDistortionPpv));
        
        // Readjust skybox shader on awake, as unity editor does not reset changes to the skyboxmaterial made in play mode...
        skyboxMaterial.SetFloat(SunRadius, sunRadius[0]);
        skyboxMaterial.SetFloat(OffsetHorizon, horizonOffset[0]);
        skyboxMaterial.SetFloat(HorizonIntensity, horizonIntensity[0]);
        skyboxMaterial.SetFloat(DistortScale, distortNoiseScale[0]);
        skyboxMaterial.SetFloat(Distortion, extraDistortion[0]);
        skyboxMaterial.SetFloat(Speed, moveSpeed[0]);
        skyboxMaterial.SetFloat(Fuzziness, cloudFuzziness[0]);
        skyboxMaterial.SetFloat(Brightness, cloudBrightness[0]);
        
        skyboxMaterial.SetColor(MoonColor, moonColor[0]);
        skyboxMaterial.SetColor(StarsSkyColor, starSkyColor[0]);
        skyboxMaterial.SetColor(SunSet, sunsetColor[0]);
        skyboxMaterial.SetColor(DayTopColor, daySkyColorTop[0]);
        skyboxMaterial.SetColor(DayBottomColor, daySkyColorBot[0]);
        skyboxMaterial.SetColor(NightTopColor, nightSkyColorTop[0]);
        skyboxMaterial.SetColor(NightBottomColor, nightSkyColorBot[0]);
        skyboxMaterial.SetColor(CloudColorNightEdge, cloudEdgeNight[0]);
    }

    // Update is called once per frame
    private void Update()
    {
        float lerpvalue = 99f;
        
        switch (currentEnviornment)
        {
            case Enviornment.Icepole:
                lerpvalue = CalculateLerpPercent(false);
                break;
            case Enviornment.Volcano:
                lerpvalue = CalculateLerpPercent(true);
                break;
            case Enviornment.Forest:
                break;
            default:
                break;
        }
        
        if (lerpvalue <= 1)
            LerpEnviornment(lerpvalue);
    }

    private float CalculateLerpPercent(bool isFire)
    {
        float lerppercent;
        if (isFire)
        {
            lerppercent = Mathf.Abs(Vector3.Distance(lavaPole.position, character.position));
            lerppercent /= lavaMaxDistance;
        }
        else
        {
            lerppercent = Mathf.Abs(Vector3.Distance(icePole.position, character.position));
            lerppercent /= iceMaxDistance;
        }
            
        if (lerppercent > 1) // Percentage bigger than 100 is irrelevant 
            lerppercent = 1;
        lerppercent = 1 - lerppercent; // Invert percentage, so the close we are to the pole the bigger the percentage and therefore influence
        
        return lerppercent;
    }

    private void LerpEnviornment(float lerp)
    {
        int enviornment = (int)currentEnviornment;

        // Adjust Lighting
        mainLight.intensity = Mathf.Lerp(lightIntensity[0], lightIntensity[enviornment], lerp);
        mainLight.transform.eulerAngles = Vector3.Lerp(lightRotation[0], lightRotation[enviornment], lerp);
        mainLight.color = Color.Lerp(lightColor[0], lightColor[enviornment], lerp);
        
        // Adjust PostProcessing
        colorAdjustments.contrast.Override(Mathf.Lerp(colorAdjustContrast[0], colorAdjustContrast[enviornment], lerp));
        colorAdjustments.hueShift.Override(Mathf.Lerp(colorAdjustHue[0], colorAdjustHue[enviornment], lerp));
        colorAdjustments.saturation.Override(Mathf.Lerp(colorAdjustSat[0], colorAdjustSat[enviornment], lerp));
        
        whiteBalance.temperature.Override(Mathf.Lerp(whiteBalanceTemp[0], whiteBalanceTemp[enviornment], lerp));
        whiteBalance.tint.Override(Mathf.Lerp(whiteBalanceTint[0], whiteBalanceTint[enviornment], lerp));
        
        chromaticAberration.intensity.Override(Mathf.Lerp(chromAbberation[0], chromAbberation[enviornment], lerp));
        
        bloomPpv.intensity.Override((Mathf.Lerp(bloom[0], bloom[enviornment], lerp)));
        
        lensDistortionPpv.intensity.Override((Mathf.Lerp(lensDistortion[0], lensDistortion[enviornment], lerp)));
        
        if(currentEnviornment == Enviornment.Volcano)
            channelMixer.redOutRedIn.Override(Mathf.Lerp(channelmixer[0], channelmixer[enviornment], lerp));
        else

            channelMixer.blueOutBlueIn.Override(Mathf.Lerp(channelmixer[0], channelmixer[enviornment], lerp));
        
        // Adjust Sky Shader
        skyboxMaterial.SetFloat(SunRadius, Mathf.Lerp(sunRadius[0], sunRadius[enviornment], lerp));
        skyboxMaterial.SetFloat(OffsetHorizon, Mathf.Lerp(horizonOffset[0], horizonOffset[enviornment], lerp));
        skyboxMaterial.SetFloat(HorizonIntensity, Mathf.Lerp(horizonIntensity[0], horizonIntensity[enviornment], lerp));
        skyboxMaterial.SetFloat(DistortScale, Mathf.Lerp(distortNoiseScale[0], distortNoiseScale[enviornment], lerp));
        skyboxMaterial.SetFloat(Distortion, Mathf.Lerp(extraDistortion[0], extraDistortion[enviornment], lerp));
        skyboxMaterial.SetFloat(Speed, Mathf.Lerp(moveSpeed[0], moveSpeed[enviornment], lerp));
        skyboxMaterial.SetFloat(Fuzziness, Mathf.Lerp(cloudFuzziness[0], cloudFuzziness[enviornment], lerp));
        skyboxMaterial.SetFloat(Brightness, Mathf.Lerp(cloudBrightness[0], cloudBrightness[enviornment], lerp));
        
        skyboxMaterial.SetColor(MoonColor, Color.Lerp(moonColor[0], moonColor[enviornment], lerp));
        skyboxMaterial.SetColor(StarsSkyColor, Color.Lerp(starSkyColor[0], starSkyColor[enviornment], lerp));
        skyboxMaterial.SetColor(SunSet, Color.Lerp(sunsetColor[0], sunsetColor[enviornment], lerp));
        skyboxMaterial.SetColor(DayTopColor, Color.Lerp(daySkyColorTop[0], daySkyColorTop[enviornment], lerp));
        skyboxMaterial.SetColor(DayBottomColor, Color.Lerp(daySkyColorBot[0], daySkyColorBot[enviornment], lerp));
        skyboxMaterial.SetColor(NightTopColor, Color.Lerp(nightSkyColorTop[0], nightSkyColorTop[enviornment], lerp));
        skyboxMaterial.SetColor(NightBottomColor, Color.Lerp(nightSkyColorBot[0], nightSkyColorBot[enviornment], lerp));
        skyboxMaterial.SetColor(CloudColorNightEdge, Color.Lerp(cloudEdgeNight[0], cloudEdgeNight[enviornment], lerp));
    }
}
