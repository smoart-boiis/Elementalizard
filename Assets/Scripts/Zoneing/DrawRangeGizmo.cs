﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawRangeGizmo : MonoBehaviour
{
    [SerializeField] private float range;
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
