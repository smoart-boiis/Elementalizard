﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaEnter : MonoBehaviour
{
    [SerializeField] private EnviormentLerper.Enviornment area;
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("MainCharacter"))
            return;

        transform.parent.GetComponent<EnviormentLerper>().currentEnviornment = area;
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.CompareTag("MainCharacter"))
            return;

        transform.parent.GetComponent<EnviormentLerper>().currentEnviornment = EnviormentLerper.Enviornment.Forest;
    }
}
