﻿using UnityEngine;
using Hellmade.Sound;

public class MouseClickSound : MonoBehaviour
{
    [SerializeField] private AudioClip mouseClick;

    public void OnClick()
    {
        EazySoundManager.PlayUISound(mouseClick, EazySoundManager.GlobalUISoundsVolume);
    }
}
