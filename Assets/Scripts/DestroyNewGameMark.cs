﻿using UnityEngine;

public class DestroyNewGameMark : MonoBehaviour
{
    public void DestroyMark()
    {
        GameObject mark = GameObject.Find("NewGame");
        if (mark != null)
            Destroy(mark);
    }
}
